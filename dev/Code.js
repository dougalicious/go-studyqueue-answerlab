/** CONSTANT ENV VAR FOR MUltivitiam */  
const devEmails = 'douglascox@google.com, skiggens@google.com, uxi-mv-uat@google.com'
/*
function removeProtections(){
  var sheet = SpreadsheetApp.getActive().getSheetByName("New unallocated studies");
  var protections = sheet.getProtections(SpreadsheetApp.ProtectionType.SHEET);
  for (var p=0; p < protections.length; p++){
    protections[p].remove();
  }
  
  var activeSheet = SpreadsheetApp.getActive().getSheetByName("Active Studies");
  activeSheet.setFrozenRows(1);
}
*/
function dailyBackups() {
  var s = SpreadsheetApp.getActive();
  var copy = s.copy("Study Queue backup: " + new Date());
  copy.addEditor("uxi-data@google.com");
}

function getStudiesAsHtmlList(arr) {
  const asList = (arr) => arr.map(({ question, answer }) => `<li>${question}: ${answer}`)
  return `<ul> ${asList(arr)}  </ul>`
}
function getStudyPropertiesAsArr({ namedValues }) {
  return Object.entries(namedValues).reduce((acc, arr) => {
    let answer = arr[1][0] === "" ? "N/A" : arr[1][0]
    let question = arr[0]
    let formObj = { question, answer: answer }
    return acc.concat(formObj)
  }, [])

}


// Get person info using AdminDirectory.
// All users are assumed to be registered in ldap as userName@google.com
function getGooglerInfo(userName) {
  userName = userName.indexOf("@") === -1 ? userName : userName.substring(0, userName.indexOf("@google.com"));
  var person = null;
  try {
    var resp = AdminDirectory.Users.get(userName + "@google.com", { 'viewType': 'domain_public' });
    if (resp != null) {
      var p = resp;
      person = {};
      // Safe mapping to bring data if it can be found under the deep nested hierarchy.
      person.id = p.id;
      person.email = p.primaryEmail;
      person.userName = getUserNameFromEmail(person.email);
      try {
        person.location = p.addresses[0].locality;
      }
      catch (e) { }
      // person.userName = getUserNameFromEmail(p.primaryEmail);
      //  person.employeeId = getExternalId(p.externalIds, "Employee ID");
      try {
        person.firstName = p.name.givenName;
        person.lastName = p.name.familyName;
        person.fullName = p.name.fullName;
      } catch (e) { Logger.log("Name not found for %s", userName); };
      try {
        // Try to find a primary organization.  If not found, use the first one.
        var primaryOrg = getPrimaryOrganization(p.organizations);
        person.physicalDesk = primaryOrg.location;
        person.costCenter = primaryOrg.costCenter;
        person.title = primaryOrg.title;
        //person.costCenterNumber = getCostCenterNumberByName(person.costCenter);
        //   person.description = primaryOrg.description;

      } catch (e) { Logger.log("Organization not found for %s", userName); };
      try {
        var managerIds = p.relations
          .filter(function (r) { return r.type == "manager" })
          .map(function (r) { return getUserNameFromEmail(r.value); });
        person.managers = managerIds.join(", ");  // Comma separated list of managers
        person.manager = managerIds[0];           // First found manager
      } catch (e) { Logger.log("Admin assistants not found for %s", userName); };

    }
  }
  catch (e) {
    var exp = e;
    // Logger.log(e);
    Logger.log("%s not found on` teams", userName);
  }
  return person;
}

function getExternalId(externalIds, type) {
  try {
    return externalIds.filter(function (id) { return id.customType == type; })
      .map(function (id) { return id.value; })[0];
  } catch (e) {
    //Logger.log(e);
    Logger.log("External Id with type %s not found", type);
  }
}

function getUserNameFromEmail(email) {
  // Regex used to extract username from the email
  return email.match(/(("?(.*)"?)\s)?(<?((.*)@.*)>?)/i).slice(-1)[0];
}

function getDirectReports(userName) {
  return getReports(userName, true);
}

function getAllReports(userName) {
  return getReports(userName, false);
}

function updateLocationsRolling() {
  var sheet = SpreadsheetApp.getActiveSheet();
  for (var i = 2; i <= sheet.getLastRow(); i++) {
    if (sheet.getRange(i, 21, 1, 1).getValue() == "") {
      var userName = sheet.getRange(i, 9, 1, 1).getValue();
      userName = userName.substring(0, userName.indexOf("@google.com"));
      try {
        sheet.getRange(i, 21, 1, 1).setValue(getGooglerInfo(userName).location);
      }
      catch (e) { }
    }
  }
}

function updateCCsRolling() {
  var sheet = SpreadsheetApp.getActiveSheet();
  for (var i = 2; i <= sheet.getLastRow(); i++) {
    if (sheet.getRange(i, 22, 1, 1).getValue() == "undefined") {
      var userName = sheet.getRange(i, 9, 1, 1).getValue();
      userName = userName.substring(0, userName.indexOf("@google.com"));
      try {
        var researcherID = getGooglerInfo(userName);
        var ccString = researcherID.costCenter;
        var ccCode = ccString.substring(0, ccString.indexOf(":")); //Person.costCenterNumber;
        var ccName = ccString.substring(ccString.indexOf(" ") + 1); //Person.costCenter;
        sheet.getRange(i, 22, 1, 1).setValue(ccCode);
        sheet.getRange(i, 23, 1, 1).setValue(ccName);
      }
      catch (e) { }
    }
  }
}

function getReports(userName, directReports) {
  var reports = [];
  try {
    var resp = AdminDirectory.Users.list({
      customer: 'my_customer',
      query: (directReports ? 'directManager=' : 'manager=') + userName + "@google.com",
      fields: 'users(name,primaryEmail,organizations,externalIds)',    // only name, email and organizations
      viewType: 'domain_public',
      maxResults: 100                        // Set this to increase max number of direct reports
    });
    if (resp != null) {
      reports = resp.users.map(function (u) {
        return {
          fullName: u.name.fullName, email: u.primaryEmail,
          userName: getUserNameFromEmail(u.primaryEmail),
          description: getPrimaryOrganization(u.organizations).description,
          employeeId: getExternalId(u.externalIds)
        };
      });
    }
  }
  catch (e) {
    var exp = e;
    // Logger.log(e);
    Logger.log("Direct reports not found for %s", userName);
  }
  return reports;
}

function getPrimaryOrganization(organizations) {
  var primaryOrg = organizations.filter(function (o) { return o.primary });
  if (primaryOrg.length > 0) {
    return primaryOrg[0];
  } else {
    return organizations[0];
  }
}

function getCostCenterNumberByName(costCenterName) {
  return runQuery("SELECT cost_center_num FROM PeopleView.CostCenters where cost_center_name='" + costCenterName + "'");
}

function runQuery(query) {
  try {
    var request = {
      queryRequest: {
        query: {
          text: query,
          engine: "DREMEL"
        }
      }
    };

    var projection = Plx.Projections.create(request);
    while (projection.state !== 'done') {
      Utilities.sleep(1000);
      projection = getProjection_(projection);
    }
    var data = Utilities.parseCsv(projection.data);
    if (data.length > 1)
      return data[1][0];
  } catch (e) {
    //Logger.log(e);
    Logger.log("Error running query \"%s\"", query);
    return null;
  }
}


function getProjection_(projection) {
  return Plx.Projections.get(projection.id, { token: projection.token });
}


// whoSupports returns (...[ldap, email, supportedByString, notIPaTS, iPaTS, myURL, ldapFound, podLeadsString]);
function whoSupports(headers, requestRow) {

  //list of ldaps we cover
  var sheet1 = SpreadsheetApp.getActiveSpreadsheet();
  var supportsheet = sheet1.getSheetByName("Who we cover");
  var supportrows = supportsheet.getDataRange();
  var supportnumRows = supportrows.getNumRows();
  var supportvalues = supportrows.getValues();
  var supportlastRow = supportsheet.getLastRow();
  var supportlastColumn = supportsheet.getLastColumn();

  //list of PAs we cover
  var PAsheet = sheet1.getSheetByName("PAs we cover");
  var PArows = PAsheet.getDataRange();
  var PAnumRows = PArows.getNumRows();
  var PAvalues = PArows.getValues();
  var PAlastRow = PAsheet.getLastRow();
  var PAlastColumn = PAsheet.getLastColumn();

  var emails = [];
  var managerEmails = [];
  var manager = "";
  var ldapFound = false;
  var supportedby = [];
  var supportedbyUS = [];
  var notIPaTS = true;
  //var notEDU = true;
  var iPaTS = false;
  var podLeads = [];
  var supportLevel = "None";

  debugger
  for (var p = 0; p < PAvalues.length; p++) {

    if (PAvalues[p][0] == "Generalists") {
      var generalists = PAvalues[p][3].split(", ");
    }
    else if (PAvalues[p][0] == "Not supported") {
      var notsupported = PAvalues[p][3].split(", ");
    }
    else if (PAvalues[p][0] == "iPaTS") {
      var iPaTSsupport = PAvalues[p][3].split(", ");
    }
    else if (PAvalues[p][0] == "a11y") {
      var a11ysupport = PAvalues[p][3].split(", ");
      var a11yPod = PAvalues[p][2];

      // 6.23.20 @douglascox moved to reference id
      // var whomWeSupportSheet = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/1uRYj1W6rz8PaGooW1P-iwguIjY9fPQi6CAhQ1gx8U7A/edit#gid=624907318");
      /** Multivitiam update dev reference */
      var whomWeSupportSheet = SpreadsheetApp.openById("1s7hcB2ipDz2bTfbl4v61rjU9N9pmPXgm1BsLKEnYf_Q");
      // var whomWeSupportSheet = SpreadsheetApp.openById("1uRYj1W6rz8PaGooW1P-iwguIjY9fPQi6CAhQ1gx8U7A");var podSheet = whomWeSupportSheet.getSheetByName("Pods");
      var podValues = podSheet.getDataRange().getValues();
      var podColumn = podValues[0].indexOf("Pod Name");
      var leadColumn = podValues[0].indexOf("Pod Leads");
      for (var l = 1; l < podValues.length; l++) {
        if (podValues[l][podColumn] == a11yPod) {
          var a11yPodLead = podValues[l][leadColumn];
        }
      }


    }
    // else if(PAvalues[p][0] == "Education"){
    //  var eduSupport  = PAvalues[p][3].split(", ");
    //}
  }



  //determine which columns have the info we need in the study queue by scanning through the first row in the sheet and looking for specific text
  for (var i = 1; i < headers.length; i++) {
    if (headers[i] == "Username") {
      var ldapColumn = i;
    }
    else if (headers[i] == "Email Address") {
      var ldapColumn = i;
    }
    else if (headers[i] == "Where in the world would the participants in your study be located?") {
      var regionColumn = i;
    }
    else if (headers[i] == "What type of support do you need?") {
      var supportTypeColumn = i;
    }
    else if (headers[i] == "What type of participants are you planning to use?") {
      var pptTypeColumn = i;
    }
    else if (headers[i] == "Paste the link to your Study Spreadsheet here. Ensure that your Screener Form has been linked to it and that the sharing settings for both are: private, with uxr-operations-share@google.com added as an editor/collaborator.") {
      var URLColumn = i;
    }
    else if (headers[i] == "Product Area (recruiters do not edit this column)") {
      var BUColumn = i;
    }
    else if (headers[i] == "Do your participants belong to any of the following categories?") {
      var categoryColumn = i;
    }
    else if (headers[i] == "Pod") {
      var podColumn = i;
    }

  }

  //find the requester's ldap by looking for the value in the active row in the column we determined contains the ldap
  var ldap = requestRow[ldapColumn];
  Logger.log("ldap: " + ldap);


  //find the requester type by looking for the value in the active row in the column we determined contains the request type
  var supportType = "";
  supportType = requestRow[supportTypeColumn];
  Logger.log("supportType: " + supportType);

  //find the region by looking for the value in the active row in the column we determined contains the region
  var region = requestRow[regionColumn];
  var regions = region.split(", ");
  Logger.log("regions: " + regions);

  //find the ppt type by looking for the value in the active row in the column we determined contains the ppt type
  var pptType = requestRow[pptTypeColumn];
  Logger.log("pptsType: " + pptType);

  //find the BU by looking for the value in the active row in the column we determined contains the BU
  var requestPA = requestRow[BUColumn];
  Logger.log("Request PA: " + requestPA);

  //find the ldap in the "who we cover" sheet to determine the recruiter support

  for (var j = 1; j < PAlastRow; j++) {
    //    var regexRequestPA = RegExp(requestPA.trim(), 'gi')
    //    var isMatchRequestPA = regexRequestPA.test(PAvalues[j][0])
    var buPAs = typeof PAvalues[j][0] == 'string' ? (PAvalues[j][0]).trim() : PAvalues[j][0]
    if (buPAs == requestPA) {
      ldapFound = true;
      // if(PAvalues[j][8]!=null){
      supportLevel = PAvalues[j][8];
      // }
      Logger.log("first support level: " + supportLevel + " " + j);
      //  Logger.log("PAvalues" + PAvalues);
      supportedbyUS = PAvalues[j][3].split(", ");
      var USPodLead = PAvalues[j][14];

    }

  }

  Logger.log("supportedbyUS: " + supportedbyUS);
  Logger.log("USPodLead: " + USPodLead);

  //if the ldap was found, but no recruiter is assigned yet, it is supported by the generalists
  if ((ldapFound == true) && (supportedbyUS == "TBD" || supportedbyUS == "") && (supportLevel.indexOf("Full") >= 0)) {
    supportedbyUS = generalists;
    //Logger.log("SupportedbyUS: " + supportedbyUS);
  }

  //if request is a11y related, add in a11y recruiters
  if (requestRow[categoryColumn].indexOf("A11y") >= 0) {
    supportedby = supportedby.concat(a11ysupport);
    podLeads.push(a11yPodLead);
  }
  //if request is edu related, switch to edu recruiters
  // else if(requestRow[categoryColumn].indexOf("Education") >= 0){

  //  supportedby = eduSupport;
  // notEDU = false;
  //}

  //if the request is internal, it is supported by the iPaTS recruiter
  if ((ldapFound == true) && (String(supportType).indexOf("Self-recruitment support") > -1) && (String(pptType).indexOf("Internal - Googlers") > -1)) {
    iPaTS = true;
    supportedby = supportedby.concat(iPaTSsupport);

    if (String(pptType).indexOf("External") > -1) {

      notIPaTS = true;
    }
  }
  else {
    notIPaTS = true;
    Logger.log("Not IPATS");
  }
  var intSupport = "";
  for (var r = 0; r < regions.length; r++) {
    if ((regions[r] != "United States and Canada") && (notIPaTS)) {
      for (var p = 0; p < PAlastRow; p++) {
        if (PAvalues[p][0] == regions[r]) {
          intSupport = PAvalues[p][3].split(", ")
          Logger.log("regions: " + regions[r]);

          Logger.log("intSupport: " + intSupport);
          if (supportLevel.indexOf("Full") >= 0) {
            supportedby = supportedby.concat(intSupport);
            podLeads.push(PAvalues[p][14]);
          }

        }
      }
    }
    else if (notIPaTS) {// && notEDU){
      Logger.log("Support Level: " + supportLevel);
      Logger.log("Not iPaTS");
      if ((supportLevel.indexOf("Full") >= 0) || (String(supportType).indexOf("Self-recruitment support") > -1)) {
        supportedby = supportedby.concat(supportedbyUS);
        Logger.log("supportedBy: " + supportedby);
        podLeads.push(USPodLead);

      }
      else if (supportLevel.indexOf("Full") < 0) {
        ldapFound = false;
        Logger.log("Support level: " + supportLevel);
      }
    }
  }

  if (ldapFound == false) {
    supportedby = notsupported;
  }

  supportedby = ArrayLib.unique(supportedby);
  Logger.log("ArrayLib.unique: " + supportedby);
  podLeads = ArrayLib.unique(podLeads)
  debugger
  // scan through the list of people supporting this ldap
  for (var m = 0; m < supportedby.length; m++) {
    //try to find each person's manager
    //add each recruiter to the email list
    emails.push(supportedby[m]);

    try {
      manager = getGooglerInfo(supportedby[m]).manager;
      // check to see if the manager is already on the list
      var found = false;
      for (var k = 0; k < managerEmails.length; k++) {
        if (managerEmails[k] == manager) {
          found = true;
        }
      }

      // if the manager is not already on the list, add to the managerEmails list
      if ((found == false) && (manager != "dawnherman") && (manager != "mgreenhalgh")) {
        managerEmails.push(manager);
      }
    }
    catch (e) {
    }
  }

  /* EMAIL DEDICATED RECRUITER(S)*/
  var supportedByNames = [];
  var supportedByString = "";

  // find the Alias (first name) of each recruiter
  for (var e = 0; e < emails.length; e++) {
    Logger.log("emails " + e + ": " + emails[e]);
    try {
      supportedByNames.push(getGooglerInfo(emails[e]).firstName);
      Logger.log("supportedByNames: " + supportedByNames);
    }
    catch (e) {
    }
    if (emails[e] == "uxr-operations") {
      supportedByNames.push("UXR Responders");
    }
  }

  // silly extra code to add the appropriate "ands" and commas based on how many recruiters are on the supported by list.  
  if (supportedByNames.length == 1) {
    supportedByString = supportedByNames[0];
  }
  else if (supportedByNames.length == 2) {
    supportedByString = supportedByNames[0] + " and " + supportedByNames[1];
  }
  else if (supportedByNames.length > 2) {
    for (var s = 0; s < supportedByNames.length; s++) {
      if (s == supportedByNames.length - 2) {
        supportedByString = supportedByString + supportedByNames[s] + " and ";
      }
      else if (s == supportedByNames.length - 1) {
        supportedByString = supportedByString + supportedByNames[s];
      }
      else {
        supportedByString = supportedByString + supportedByNames[s] + ", ";
      }
    }
  }

  // combine a string together of all the emails this email is going to - recruiter(s) and their manager(s)
  var emailString = "";
  for (var v = 0; v < emails.length; v++) {
    // scan through all the recruiters, combine into one list and add @google.com to each of them (and a comma)
    emailString = emailString + emails[v] + "@google.com, ";
  }

  var podLeadsString = "";
  Logger.log("podLeads")
  Logger.log(podLeads);
  if (podLeads[0]) {
    for (var v = 0; v < podLeads.length; v++) {
      // scan through all the managers, combine into one list and add @google.com to each of them (and a comma, unless it's the last one in the list)
      var tempLeads = podLeads[v] === undefined || /^null/gi.test(podLeads[v]) ? [] : podLeads[v].split(",");
      for (var t = 0; t < tempLeads.length; t++) {
        // if(v==podLeads.length-1){
        //  podLeadsString = podLeadsString + podLeads[v] + "@google.com, ";
        // }
        // else{
        podLeadsString = podLeadsString + tempLeads[t] + "@google.com, ";
        // }
      }
    }
  }
  //if no one was found to email - email the email responder list
  Logger.log("Supported by: " + supportedby);

  var email = emailString;
  // }
  var myURL = requestRow[URLColumn];
  if (myURL.indexOf("http") >= 0) {
    myURL = "<a href=\"" + requestRow[URLColumn] + "\">study request</a>";
  }
  else {
    myURL = "study request";
  }

  //  var contextParam = {
  //  ldap, email, supportedByString, notIPaTS, iPaTS, myURL, ldapFound, podLeadsString
  //  }
  debugger
  return ([ldap, email, supportedByString, notIPaTS, iPaTS, myURL, ldapFound, podLeadsString]);
}
/** allocation function remove for multivitiam */
// function allocation(headers, requestRow, e) {
//   //study queue
//   var sheet = e.range.getSheet();//SpreadsheetApp.getActiveSpreadsheet();
//   //var sheet = sheet1.getSheetByName("New unallocated studies");
//   var rows = sheet.getDataRange();
//   var numRows = rows.getNumRows();
//   var values = rows.getValues();
//   var lastRow = sheet.getLastRow();
//   var lastColumn = sheet.getLastColumn();
//   var ActiveRow = e.range.getRow(); //sheet1.getActiveRange().getRow();

//   //create a string of the entire row that was just submitted to later add to the body of the email.
//   var studydetails = "<ul>";
//   for (var m = 0; m < lastColumn; m++) {
//     if (headers[m] == "Researcher Location") {
//       studydetails = studydetails + "<li>Researcher location: " + requestRow[m] + "<br>";
//     }
//     else if (requestRow[m] === "") {
//       continue
//     }
//     else {
//       studydetails = studydetails + "<li>" + requestRow[m] + "<br>";
//     }
//   }
//   studydetails = studydetails + "</ul>"


//   // whoSupports @returns (...[ldap, email, supportedByString, notIPaTS, iPaTS, myURL, ldapFound, podLeadsString]);
//   var supportInfo = whoSupports(headers, requestRow);
//   Logger.log("Support Info: " + supportInfo);
//   // jgolenbock@google.com,,,true,false,study request,true,

//   var ldap = supportInfo[0];
//   var email = supportInfo[1];
//   var supportedByString = supportInfo[2];
//   var notIPaTS = supportInfo[3];
//   var iPaTS = supportInfo[4];
//   var myURL = supportInfo[5];
//   var ldapFound = supportInfo[6];
//   var podLeads = supportInfo[7];


//   var body = "";

//   //get the request's timestamp and format it to be shorter in the subject of the email
//   var timestamp = String(sheet.getRange(ActiveRow, 1, 1, 1).getValue());
//   var timestamp2 = timestamp;
//   timestamp = timestamp.substr(0, timestamp.length - 15) + timestamp2.substr(33, 6);

//   //define the subject of the email
//   var subject = "[Multivitamin TEST] New study request from: " + ldap + " (" + timestamp + ")";

//   var claimURL = "https://docs.google.com/spreadsheets/d/1b1gf_VaeSEfxOK6q6GSthICcPglN5A78kVQUFIeSUQg/edit#gid=404773570"
//   /** DEV ENV  */
//   claimUrl = SpreadsheetApp.getActiveSpreadsheet().getUrl()
//   claimURL = "<a href=\"" + claimURL + "\">moved to the active tab</a>";





//   //body of the email that goes out for iPaTS requests
//   if (iPaTS) {
//     if (notIPaTS) {
//       body = //"Note: This email was sent from Janice’s account but is triggered automatically by a script. No need to reply to Janice. <br><br>" +
//         "Hello " + supportedByString + ",<br><br>" +
//         "A new " + myURL + " has come in from " + ldap + ".  This submission may include an iPaTS request as well.<br>" +
//         "Please pick up this study or escalate to your manager if needed. " +
//         "You and your manager will both be emailed again if the study is not " + claimURL + " within 2 business days.<br>" +
//         "Here are the full request details:<br><br>" + studydetails +
//         "<br><br>" +
//         "Thank you!<br>" +
//         "Super duper allocator script Ninja <br><br>";// +
//       //  "<small>Note: This email was sent from Janice’s account but is triggered automatically by a script.<br>Janice does not need to be included in future correspondence on this thread </small>. <br><br>";


//     }
//     else {

//       body = //"Note: This email was sent from Janice’s account but is triggered automatically by a script. No need to reply to Janice. <br><br>" +
//         "Hello " + supportedByString + ",<br><br>" +
//         "A new " + myURL + " for iPaTS access has come in from " + ldap + ".<br>" +
//         "Please grant iPaTS access or escalate to your manager if needed. " +
//         "You and your manager will both be emailed again if the study is not " + claimURL + " within 2 business days.<br>" +
//         "Here are the full request details:<br><br>" + studydetails +
//         "<br><br>" +
//         "Thank you!<br>" +
//         "Super duper allocator script Ninja <br><br>";// +
//       // "<small>Note: This email was sent from Janice’s account but is triggered automatically by a script.<br>Janice does not need to be included in future correspondence on this thread </small>. <br><br>";
//     }
//   }

//   //body of the email that goes out in most cases
//   else if (ldapFound == true) {
//     body = //"Note: This email was sent from Janice’s account but is triggered automatically by a script. No need to reply to Janice. <br><br>" +
//       "Hello " + supportedByString + ",<br><br>" +
//       "A new " + myURL + " has come in from " + ldap + ".<br>" +
//       "Please pick up this study or escalate to your manager if needed. " +
//       "You and your manager will both be emailed again if the study is not " + claimURL + " within 2 business days.<br>" +
//       "Here are the full request details:<br><br>" + studydetails +
//       "<br><br>" +
//       "Thank you!<br>" +
//       "Super duper allocator script Ninja <br><br>";// +
//     // "<small>Note: This email was sent from Janice’s account but is triggered automatically by a script.<br>Janice does not need to be included in future correspondence on this thread </small>. <br><br>";
//   }
//   //email when a study is not supported
//   else {
//     body = //"Note: This email was sent from Janice’s account but is triggered automatically by a script. No need to reply to Janice. <br><br>" +
//       "Hello " + supportedByString + ",<br><br>" +
//       "A new study request has come in from " + ldap + ".<br>" +
//       "This is not a person we support. Please reply with information about our support model.<br>" +
//       "Here are the full request details:<br><br>" + studydetails +
//       "<br><br>" + fetchRequestDetails(ldap) +
//       "<br><br>" +
//       "Thank you!<br>" +
//       "Super duper allocator script Ninja <br><br>";// +
//     // "<small>Note: This email was sent from Janice’s account but is triggered automatically by a script.<br>Janice does not need to be included in future correspondence on this thread </small>. <br><br>";
//   }

//   //send email
//   var mailFromIndex = GmailApp.getAliases().indexOf("uxi-alerts@google.com");
//   var mailFrom = GmailApp.getAliases()[mailFromIndex];
//   /** production  */
//   // GmailApp.sendEmail(email, subject, body, { htmlBody: body, from: mailFrom, cc: podLeads, bcc: "nocella@google.com", replyTo: podLeads });
//   /** development env */
//   GmailApp.sendEmail('douglascox@google.com, skiggens@google.com,uxi-mv-uat@google.com', subject, body, { htmlBody: body, });


//   //add a note to the sheet indicating email was sent (when and to whom)

//   sheet.getRange(ActiveRow, 1, 1, 1).setNote(supportedByString + " emailed via script " + Utilities.formatDate(new Date(), "GMT", "yyyy-MM-dd' 'HH:mm' GMT'"));

//   function fetchRequestDetails(user_name) {
//     const compose = (...fns) => x => fns.reduceRight((y, f) => f(y), x);
//     const getLdap = (user_name) => user_name.slice(0, user_name.indexOf('@') === -1 ? user_name.length : user_name.indexOf('@'));
//     const fetchRequestDetailsAsHtml = compose(requesterDetailsAsHtml, fetchPlxDatabyUser, getLdap);
//     const url = SpreadsheetApp.getActiveSpreadsheet().getUrl();
//     try {
//       return fetchRequestDetailsAsHtml(user_name)
//     } catch (e) {
//       Logger.log(`fetchRequestDetails failed ${e}`)
//       GmailApp.sendEmail('douglascox@google.com', `Script Failure ${e}`, `${url} \n ts:${new Date()}`)
//       return ""
//     }

//     function fetchPlxDatabyUser(user_name) {
//       let sql = `SELECT user_name, preferred_name, person_type, business_title, unified_rollup_level_1, cost_center_name, recent_support_date, business_unit, lead_uxr FROM uxr_ops_data.whom_we_support.unsupported_all WHERE user_name = '${user_name}';`;
//       let request = {
//         queryRequest: {
//           query: { text: sql, engine: "DREMEL" }, cacheControl: { minCacheTimestampMs: -1 }
//         }
//       };
//       let projection = Plx.Projections.create(request);

//       while (projection.state !== 'done') {
//         Utilities.sleep(1000);
//         projection = Plx.Projections.get(projection.id, { token: projection.token });
//       };

//       let [keys, values] = Utilities.parseCsv(projection.data);
//       Logger.log(`keys: ${keys}`)
//       Logger.log(`values: ${values}`)
//       let emailParams = keys.reduce((acc, next, idx) => { acc[next] = values[idx] === "null" ? "N/A" : values[idx]; return acc }, {});
//       Logger.log(`emailParams: ${emailParams}`)
//       return emailParams
//     }

//     function requesterDetailsAsHtml({ user_name, preferred_name, person_type, unified_rollup_level_1, cost_center_name, recent_support_date, business_unit, lead_uxr }) {
//       return `
//     <table style="border: 1px solid black; border-collapse: collapse ">
//       <tr >
//         <td style="border: 1px solid #333; padding: 10px; text-align:center;"><strong>User Name</strong></td>
//         <td style="border: 1px solid #333; padding: 10px; text-align:center;"><strong>Name</strong></td>
//         <td style="border: 1px solid #333; padding: 10px; text-align:center;"><strong>Person Type</strong></td>
//         <td style="border: 1px solid #333; padding: 10px; text-align:center;"><strong>PA</strong></td>
//         <td style="border: 1px solid #333; padding: 10px; text-align:center;"><strong>Cost Center</strong></td>
//         <td style="border: 1px solid #333; padding: 10px; text-align:center;"><strong>Most Recently Supported</strong></td>
//         <td style="border: 1px solid #333; padding: 10px; text-align:center;"><strong>Business Unit</strong></td>
//         <td style="border: 1px solid #333; padding: 10px; text-align:center;"><strong>Lead UXR</strong></td>
//       </tr>
//       <tr>
//        <td style="border: 1px solid #333; padding: 10px; text-align:center;">${user_name}</td>
//        <td style="border: 1px solid #333; padding: 10px; text-align:center;">${preferred_name}</td>
//        <td style="border: 1px solid #333; padding: 10px; text-align:center;">${person_type}</td>
//        <td style="border: 1px solid #333; padding: 10px; text-align:center;">${unified_rollup_level_1}</td>
//        <td style="border: 1px solid #333; padding: 10px; text-align:center;">${cost_center_name}</td>
//        <td style="border: 1px solid #333; padding: 10px; text-align:center;">${recent_support_date}</td>
//        <td style="border: 1px solid #333; padding: 10px; text-align:center;">${business_unit}</td>
//        <td style="border: 1px solid #333; padding: 10px; text-align:center;">${lead_uxr}</td>
//       </tr>
//     </table>`
//     }
//   }
// }


/** multivitiam remove allocation */
// function forceAllocate() {

//   // Get a public lock on this script, because we're about to modify a shared resource.
//   var lock = LockService.getScriptLock();

//   // Wait for up to 90 seconds for other processes to finish.
//   lock.waitLock(90000);

//   //study queue
//   var sheet1 = SpreadsheetApp.getActiveSpreadsheet();

//   var sheet = sheet1.getActiveSheet();
//   if (sheet.getName() != "New unallocated studies") {
//     return;
//   }

//   var rows = sheet.getDataRange();
//   var numRows = rows.getNumRows();
//   var values = rows.getValues();
//   var lastRow = sheet.getLastRow();
//   var lastColumn = sheet.getLastColumn();

//   var ActiveRow = sheet.getActiveRange().getRow();

//   //create a string of the entire row that was just submitted to later add to the body of the email.
//   var studydetails = "<ul>";
//   for (var m = 0; m < lastColumn; m++) {
//     if (values[0][m] == "Researcher Location") {
//       studydetails = studydetails + "<li>Researcher location: " + values[ActiveRow - 1][m] + "<br>";
//     }
//     else {
//       studydetails = studydetails + "<li>" + values[ActiveRow - 1][m] + "<br>";
//     }
//   }
//   studydetails = studydetails + "</ul>"

//   // whoSupports @returns (...[ldap, email, supportedByString, notIPaTS, iPaTS, myURL, ldapFound, podLeadsString]);
//   var supportInfo = whoSupports(values[0], values[ActiveRow - 1]);

//   var ldap = supportInfo[0];
//   var email = supportInfo[1];
//   var supportedByString = supportInfo[2];
//   var notIPaTS = supportInfo[3];
//   var iPaTS = supportInfo[4];
//   var myURL = supportInfo[5];
//   var ldapFound = supportInfo[6];
//   var podLeads = supportInfo[7];


//   var body = "";

//   //get the request's timestamp and format it to be shorter in the subject of the email
//   var timestamp = String(sheet.getRange(ActiveRow, 1, 1, 1).getValue());
//   var timestamp2 = timestamp;
//   timestamp = timestamp.substr(0, timestamp.length - 15) + timestamp2.substr(33, 6);

//   //define the subject of the email
//   var subject = "[Multivitamin TEST] New study request from: " + ldap + " (" + timestamp + ")";

//   var claimURL = "https://docs.google.com/spreadsheets/d/1b1gf_VaeSEfxOK6q6GSthICcPglN5A78kVQUFIeSUQg/edit#gid=404773570"
//   /** DEV ENV */
//   claimURL = SpreadsheetApp.getActiveSpreadsheet().getUrl()
//   claimURL = "<a href=\"" + claimURL + "\">moved to the active tab</a>";





//   //body of the email that goes out for iPaTS requests
//   if (iPaTS) {
//     if (notIPaTS) {
//       body = //"Note: This email was sent from Janice’s account but is triggered automatically by a script. No need to reply to Janice. <br><br>" +
//         "Hello " + supportedByString + ",<br><br>" +
//         "A new " + myURL + " has come in from " + ldap + ".  This submission may include an iPaTS request as well.<br>" +
//         "Please pick up this study or escalate to your manager if needed. " +
//         "You and your manager will both be emailed again if the study is not " + claimURL + " within 2 business days.<br>" +
//         "Here are the full request details:<br><br>" + studydetails +
//         "<br><br>" +
//         "Thank you!<br>" +
//         "Super duper allocator script Ninja <br><br>";// +
//       // "<small>Note: This email was sent from Janice’s account but is triggered automatically by a script.<br>Janice does not need to be included in future correspondence on this thread </small>. <br><br>";


//     }
//     else {

//       body = //"Note: This email was sent from Janice’s account but is triggered automatically by a script. No need to reply to Janice. <br><br>" +
//         "Hello " + supportedByString + ",<br><br>" +
//         "A new " + myURL + " for iPaTS access has come in from " + ldap + ".<br>" +
//         "Please grant iPaTS access or escalate to your manager if needed. " +
//         "You and your manager will both be emailed again if the study is not " + claimURL + " within 2 business days.<br>" +
//         "Here are the full request details:<br><br>" + studydetails +
//         "<br><br>" +
//         "Thank you!<br>" +
//         "Super duper allocator script Ninja <br><br>";// +
//       // "<small>Note: This email was sent from Janice’s account but is triggered automatically by a script.<br>Janice does not need to be included in future correspondence on this thread </small>. <br><br>";
//     }
//   }

//   //body of the email that goes out in most cases
//   else if (ldapFound == true) {
//     body = //"Note: This email was sent from Janice’s account but is triggered automatically by a script. No need to reply to Janice. <br><br>" +
//       "Hello " + supportedByString + ",<br><br>" +
//       "A new " + myURL + " has come in from " + ldap + ".<br>" +
//       "Please pick up this study or escalate to your manager if needed. " +
//       "You and your manager will both be emailed again if the study is not " + claimURL + " within 2 business days.<br>" +
//       "Here are the full request details:<br><br>" + studydetails +
//       "<br><br>" +
//       "Thank you!<br>" +
//       "Super duper allocator script Ninja <br><br>";// +
//     // "<small>Note: This email was sent from Janice’s account but is triggered automatically by a script.<br>Janice does not need to be included in future correspondence on this thread </small>. <br><br>";
//   }
//   //email that goes to the Leads when a study is not supported
//   else {
//     body = //"Note: This email was sent from Janice’s account but is triggered automatically by a script. No need to reply to Janice. <br><br>" +
//       "Hello " + supportedByString + ",<br><br>" +
//       "A new study request has come in from " + ldap + ".<br>" +
//       "This is not a person we support. Please reply with information about our support model.<br>" +
//       "Here are the full request details:<br><br>" + studydetails +
//       // "<br><br>" + fetchRequestDetails(ldap) +
//       "<br><br>" +
//       "Thank you!<br>" +
//       "Super duper allocator script Ninja <br><br>";// +
//     //"<small>Note: This email was sent from Janice’s account but is triggered automatically by a script.<br>Janice does not need to be included in future correspondence on this thread </small>. <br><br>";
//   }

//   //send email
//   var mailFromIndex = GmailApp.getAliases().indexOf("uxi-alerts@google.com");
//   var mailFrom = GmailApp.getAliases()[mailFromIndex];
//   /** PROD forceallocate */
//   // GmailApp.sendEmail(email, subject, body, { htmlBody: body, from: mailFrom, cc: podLeads, bcc: "nocella@google.com", replyTo: podLeads });
//   /** DEV forceallocate */
//   GmailApp.sendEmail('uxi-mv-uat@google.com, douglascox@google.com, skiggens@google.com', 'forceallocation' + subject, body, { htmlBody: body, });



//   //add a note to the sheet indicating email was sent (when and to whom)

//   sheet.getRange(ActiveRow, 1, 1, 1).setNote(supportedByString + " emailed via script " + Utilities.formatDate(new Date(), "GMT", "yyyy-MM-dd' 'HH:mm'GMT'"));

//   // Release the lock so that other processes can continue.

//   lock.releaseLock();

function fetchRequestDetails(user_name) {
  const compose = (...fns) => x => fns.reduceRight((y, f) => f(y), x)
  const fetchRequestDetailsAsHtml = compose(requesterDetailsAsHtml, fetchPlxDatabyUser)
  try {
    return fetchRequestDetailsAsHtml(user_name)
  } catch (e) {
    Logger.log(`fetchRequestDetails failed ${e}`)
    GmailApp.sendEmail('douglascox@google.com', `Script Failure ${e}`, ``)
  }

  function fetchPlxDatabyUser(user_name) {
    let sql = `SELECT user_name, preferred_name, person_type, business_title, unified_rollup_level_1, cost_center_name, recent_support_date, business_unit, lead_uxr FROM uxr_ops_data.whom_we_support.unsupported_all WHERE user_name = '${user_name}';`;
    let request = {
      queryRequest: {
        query: { text: sql, engine: "DREMEL" }, cacheControl: { minCacheTimestampMs: -1 }
      }
    };
    let projection = Plx.Projections.create(request);

    while (projection.state !== 'done') {
      Utilities.sleep(1000);
      projection = Plx.Projections.get(projection.id, { token: projection.token });
    };

    let [keys, values] = Utilities.parseCsv(projection.data);
    let emailParams = keys.reduce((acc, next, idx) => { acc[next] = values[idx] === "null" ? "N/A" : values[idx]; return acc }, {});
    return emailParams
  }

  function requesterDetailsAsHtml({ user_name, preferred_name, person_type, unified_rollup_level_1, cost_center_name, recent_support_date, business_unit, lead_uxr }) {
    return `
    <table style="border: 1px solid black; border-collapse: collapse ">
      <tr >
        <td style="border: 1px solid #333; padding: 10px; text-align:center;"><strong>User Name</strong></td>
        <td style="border: 1px solid #333; padding: 10px; text-align:center;"><strong>Name</strong></td>
        <td style="border: 1px solid #333; padding: 10px; text-align:center;"><strong>Person Type</strong></td>
        <td style="border: 1px solid #333; padding: 10px; text-align:center;"><strong>PA</strong></td>
        <td style="border: 1px solid #333; padding: 10px; text-align:center;"><strong>Cost Center</strong></td>
        <td style="border: 1px solid #333; padding: 10px; text-align:center;"><strong>Most Recently Supported</strong></td>
        <td style="border: 1px solid #333; padding: 10px; text-align:center;"><strong>Business Unit</strong></td>
        <td style="border: 1px solid #333; padding: 10px; text-align:center;"><strong>Lead UXR</strong></td>
      </tr>
      <tr>
       <td style="border: 1px solid #333; padding: 10px; text-align:center;">${user_name}</td>
       <td style="border: 1px solid #333; padding: 10px; text-align:center;">${preferred_name}</td>
       <td style="border: 1px solid #333; padding: 10px; text-align:center;">${person_type}</td>
       <td style="border: 1px solid #333; padding: 10px; text-align:center;">${unified_rollup_level_1}</td>
       <td style="border: 1px solid #333; padding: 10px; text-align:center;">${cost_center_name}</td>
       <td style="border: 1px solid #333; padding: 10px; text-align:center;">${recent_support_date}</td>
       <td style="border: 1px solid #333; padding: 10px; text-align:center;">${business_unit}</td>
       <td style="border: 1px solid #333; padding: 10px; text-align:center;">${lead_uxr}</td>
      </tr>
    </table>`
  }
}
// }



/** multivitiam remove allocation */
// function allocationReminders() {

//   // Get a public lock on this script, because we're about to modify a shared resource.
//   var lock = LockService.getScriptLock();

//   // Wait for up to 90 seconds for other processes to finish.
//   lock.waitLock(90000);

//   //study queue
//   var sheet1 = SpreadsheetApp.getActiveSpreadsheet();
//   var sheet = sheet1.getSheetByName("New unallocated studies");
//   var rows = sheet.getDataRange();
//   var numRows = rows.getNumRows();
//   var values = rows.getValues();
//   var lastRow = sheet.getLastRow();
//   var lastColumn = sheet.getLastColumn();
//   var todaysDate = new Date();//Utilities.formatDate(new Date(),"GMT", "MM/dd/yyyy' 'HH:mm");

//   //list of ldaps we cover
//   var supportsheet = sheet1.getSheetByName("Who we cover");
//   var supportrows = supportsheet.getDataRange();
//   var supportnumRows = supportrows.getNumRows();
//   var supportvalues = supportrows.getValues();
//   var supportlastRow = supportsheet.getLastRow();
//   var supportlastColumn = supportsheet.getLastColumn();

//   //list of PAs we cover
//   var PAsheet = sheet1.getSheetByName("PAs we cover");
//   var PArows = PAsheet.getDataRange();
//   var PAnumRows = PArows.getNumRows();
//   var PAvalues = PArows.getValues();
//   var PAlastRow = PAsheet.getLastRow();
//   var PAlastColumn = PAsheet.getLastColumn();



//   for (var a = 1; a < values.length; a++) {

//     var requestdate = new Date(values[a][0]);
//     //var requestdate = Utilities.formatDate(new Date(values[a][0]), "GMT", "MM/dd/yyyy' 'HH:mm");
//     var yesterday = new Date(); //Utilities.formatDate(new Date(), "GMT", "MM/dd/yyyy' 'HH:mm");
//     yesterday.setDate(todaysDate.getDate() - 1);
//     // yesterday = Utilities.formatDate(yesterday, "GMT", "MM/dd/yyyy' 'HH:mm");
//     var today = new Date(); //Utilities.formatDate(new Date(), "GMT", "MM/dd/yyyy' 'HH:mm");


//     // Logger.log("yesterday " + yesterday);
//     // Logger.log("todaysDate " + todaysDate);
//     // Logger.log("requestdate " + requestdate);

//     // whoSupports @returns (...[ldap, email, supportedByString, notIPaTS, iPaTS, myURL, ldapFound, podLeadsString]);
//     var supportInfo = whoSupports(values[0], values[a]);

//     var ldap = supportInfo[0];
//     var email = supportInfo[1];
//     var supportedByString = supportInfo[2];
//     var notIPaTS = supportInfo[3];
//     var iPaTS = supportInfo[4];
//     var myURL = supportInfo[5];
//     var ldapFound = supportInfo[6];
//     var podLeads = supportInfo[7];

//     var isEmptyRow = ([ldap, email, supportedByString, notIPaTS, iPaTS, myURL, ldapFound, podLeads].join("") === "")

//     if ((requestdate <= yesterday) && (today.getDay() != 0) && (today.getDay() != 6) && (!isEmptyRow)) {

//       //create a string of the entire row to later add to the body of the email.
//       var studydetails = "<ul>";
//       for (var m = 0; m < lastColumn; m++) {
//         studydetails = studydetails + "<li>" + values[a][m] + "<br>";
//       }
//       studydetails = studydetails + "</ul>"


//       var body = "";

//       //get the request's timestamp and format it to be shorter in the subject of the email
//       var timestamp = String(sheet.getRange(a + 1, 1, 1, 1).getValue());
//       var timestamp2 = timestamp;
//       timestamp = timestamp.substr(0, timestamp.length - 15) + timestamp2.substr(33, 6);



//       //define the subject of the email
//       var subject = "REMINDER! study request from: " + ldap + " (" + timestamp + ")";

/** Multivitiam env.dev adjust claimUrl to refrence this.trix url */
// var claimURL = SpreadsheetApp.getActiveSpreadsheet().getUrl()
// var claimURL = "https://docs.google.com/spreadsheets/d/1b1gf_VaeSEfxOK6q6GSthICcPglN5A78kVQUFIeSUQg/edit#gid=404773570"
//       claimURL = "<a href=\"" + claimURL + "\">moved to</a>";

//       //body of the email that goes out for iPaTS requests
//       if (iPaTS) {
//         if (notIPaTS) {
//           body = //"Note: This email was sent from Janice’s account but is triggered automatically by a script. No need to reply to Janice. <br><br>" +
//             "Hello " + supportedByString + ",<br><br>" +
//             "A new " + myURL + " has come in from " + ldap + ".  This submission may include an iPaTS request as well.<br>" +
//             "Please pick up this study or escalate to your manager if needed. " +
//             "You and your manager will both be emailed again if the study is not " + claimURL + " within 2 business days.<br>" +
//             "Here are the full request details:<br><br>" + studydetails +
//             "<br><br>" +
//             "Thank you!<br>" +
//             "Super duper allocator script Ninja <br><br>";// +
//           //  "<small>Note: This email was sent from Janice’s account but is triggered automatically by a script.<br>Janice does not need to be included in future correspondence on this thread </small>. <br><br>";


//         }
//         else {

//           body = //"Note: This email was sent from Janice’s account but is triggered automatically by a script. No need to reply to Janice. <br><br>" +
//             "Hello " + supportedByString + ",<br><br>" +
//             "A new " + myURL + " for iPaTS access has come in from " + ldap + ".<br>" +
//             "Please grant iPaTS access or escalate to your manager if needed. " +
//             "You and your manager will both be emailed again if the study is not " + claimURL + " within 2 business days.<br>" +
//             "Here are the full request details:<br><br>" + studydetails +
//             "<br><br>" +
//             "Thank you!<br>" +
//             "Super duper allocator script Ninja <br><br>";// +
//           //"<small>Note: This email was sent from Janice’s account but is triggered automatically by a script.<br>Janice does not need to be included in future correspondence on this thread </small>. <br><br>";
//         }
//       }

//       //body of the email that goes out in most cases
//       else if (ldapFound == true) {
//         body = //"Note: This email was sent from Janice’s account but is triggered automatically by a script. No need to reply to Janice. <br><br>" +
//           "Hello " + supportedByString + ",<br><br>" +
//           "A new " + myURL + " has come in from " + ldap + ".<br>" +
//           "Please pick up this study or escalate to your manager if needed. " +
//           "You and your manager will both be emailed again if the study is not " + claimURL + " within 2 business days.<br>" +
//           "Here are the full request details:<br><br>" + studydetails +
//           "<br><br>" +
//           "Thank you!<br>" +
//           "Super duper allocator script Ninja <br><br>";// +
//         //"<small>Note: This email was sent from Janice’s account but is triggered automatically by a script.<br>Janice does not need to be included in future correspondence on this thread </small>. <br><br>";
//       }
//       //email when a study is not supported
//       else {
//         body = //"Note: This email was sent from Janice’s account but is triggered automatically by a script. No need to reply to Janice. <br><br>" +
//           "Hello " + supportedByString + ",<br><br>" +
//           "A new study request has come in from " + ldap + ".<br>" +
//           "This is not a person we support. Please reply with information about our support model" +
//           "Here are the full request details:<br><br>" + studydetails +
//           "<br><br>" +
//           "Thank you!<br>" +
//           "Super duper allocator script Ninja <br><br>";// +
//         //"<small>Note: This email was sent from Janice’s account but is triggered automatically by a script.<br>Janice does not need to be included in future correspondence on this thread </small>. <br><br>";
//       }

//       //send email
//       var mailFromIndex = GmailApp.getAliases().indexOf("uxi-alerts@google.com");
//       var mailFrom = GmailApp.getAliases()[mailFromIndex];
//       // GmailApp.sendEmail(email, subject, body, { htmlBody: body, from: mailFrom, cc: podLeads, bcc: "nocella@google.com", replyTo: podLeads });
//       GmailApp.sendEmail("uxi-mv-uat@google.com, douglascox@google.com", subject, body, { htmlBody: body, from: mailFrom, replyTo: podLeads });


//       //add a note to the sheet indicating email was sent (when and to whom)

//       var myNote = sheet.getRange(a + 1, 1, 1, 1).getNote();

//       sheet.getRange(a + 1, 1, 1, 1).setNote(myNote + "\n\n" + supportedByString + " emailed reminder via script " + Utilities.formatDate(new Date(), "GMT", "yyyy-MM-dd' 'HH:mm' GMT'"));

//     }

//   }
//   // Release the lock so that other processes can continue.

//   lock.releaseLock();
// }



function emailStudyReceipt(sheet, researcher, myURL, myStudyName, activeRow) {
  debugger
  Logger.log('emailStudyReceipt - researcher: ' + researcher);
  Logger.log('emailStudyReceipt - researcher.firstName: ' + researcher.firstName);
  // if (myURL.indexOf("http") >= 0) {
  //   myURL = `<a href="${myURL}">${myStudyName}</a>`
  //   // myURL = "<a href=\"" + myURL + "\">" + myStudyName + "</a>";
  // }
  // else {
  //   myURL = myStudyName;
  // }



  //var googler = getGooglerInfo(researcher);
  var subject = "[Multivitamin TEST] UX Research Operations Study Support Request Received";

  var body = //"Note: This email was sent from Janice’s account but is triggered automatically by a script. No need to reply to Janice. <br><br>" +
    '<i><small style="color: grey">FYI, managers are automatically cc’d on requests; no action is needed</i></small><br><br>' +
    "Hello " + researcher.firstName + ",<br><br>" +
    "Thank you for your study support request for " + myURL + ".<br><br>" +
    "A recruiter will be in touch within 1-2 business days with next steps.<br><br>" +
    "Thank you,<br>" +
    "UX Research Operations (uxr-operations@google.com)<br><br>"; // +
  //'<i><small style="color: grey">FYI, managers are automatically cc’d on requests; no action is needed</i></small>';
  // "<small>Note: This email was sent from Janice’s account but is triggered automatically by a script.<br>Any responses should go directly to uxr-operations@google.com</small>";
  var mailFromIndex = GmailApp.getAliases().indexOf("uxi-alerts@google.com");
  var mailFrom = GmailApp.getAliases()[mailFromIndex];
  // var htmlBody = getStudyReceiptAsHtml({firstName: researcher.firstName, myUrl, myStudyName}})
  var ccAddress = researcher.manager + '@google.com';
  Logger.log('ccAddress: ' + ccAddress);
  var htmlBody = getStudyReceiptAsHtml({ firstName: researcher.firstName, myUrl: myURL, myStudyName: myStudyName })
  /** PROD forceallocate */
  // GmailApp.sendEmail(researcher.email, subject, body, { htmlBody: htmlBody, from: mailFrom, cc: ccAddress, replyTo: "uxr-operations@google.com" });
  /** DEV forceallocate */
  const sendEmailParams = {
    to: researcher.email,
    subject,
    options: { from: mailFrom, cc: ccAddress, replyTo: "uxr-operations@google.com" }
  }
  const prodOutboundParam = JSON.stringify(sendEmailParams)+'<br />'
  body = prodOutboundParam + body
  GmailApp.sendEmail(devEmails, subject, "Study Receipt", { htmlBody: htmlBody, from: mailFrom });
  sheet.getRange(activeRow, 2, 1, 1).setNote(researcher.userName + ' and ' + researcher.manager + " emailed request receipt via script - " + Utilities.formatDate(new Date(), "GMT", "yyyy-MM-dd' 'HH:mm'GMT'"));
}

function emailStudyClaimed(researcher, myURL, myStudyName) {

  var recruiterEmail = Session.getActiveUser().getEmail();
  var email = researcher.email + ", " + recruiterEmail;
  var recruiter = getGooglerInfo(recruiterEmail.substring(0, recruiterEmail.indexOf("@google.com")));
  var recruiterName = recruiter.firstName;
  var recruiterFullName = recruiter.firstName + " " + recruiter.lastName;
  // Logger.log("myURL: " + myURL);
  if (myURL.indexOf("http") >= 0) {
    myURL = "<a href=\"" + myURL + "\">" + myStudyName + "</a>";
  }
  else {
    myURL = myStudyName;
  }



  //var googler = getGooglerInfo(researcher);
  var subject = "Your study has been assigned to " + recruiterFullName;

  var body = "Hello " + researcher.firstName + ",<br><br>" +
    "Thank you for your study support request for " + myURL + ".<br><br>" +
    "I will follow up with you shortly with more information about your request.  " +
    "I'm excited to be working with you." +
    "<br><br>" +
    "Thank you!<br>" +
    recruiterName + "<br><br>" +
    "<small>Disclaimer: this email was sent from " + recruiterName + "'s account but is triggered automatically by a script.</small>";
  var mailFromIndex = GmailApp.getAliases().indexOf("uxi-alerts@google.com");
  var mailFrom = GmailApp.getAliases()[mailFromIndex];

  /** DEV ENV SETUP */
  const sendEmailParams = {
    to: email,
    subject,
    // body,
    options: { from: mailFrom }
  }
  body = JSON.stringify(sendEmailParams) + '<br />' + body
  GmailApp.sendEmail(devEmails, subject, body, { htmlBody: body, from: mailFrom });
  /** PROD ENV SETUP */
  // GmailApp.sendEmail(email, subject, body, { htmlBody: body, from: mailFrom });
  //Logger.log(researcher.email);
  //Logger.log(body);
}



function costCentersonSubmit(e) {
  Logger.log("DEV MODE")
  e = e || artificalEvent();
  e = Object.assign(e, {devEmails})

  if (e.range.columnStart == e.range.columnEnd) return;

  // Get a public lock on this script, because we're about to modify a shared resource.
  var lock = LockService.getScriptLock();

  // Wait for up to 900 seconds for other processes to finish.
  lock.waitLock(900000);

  var sheet = e.range.getSheet();//SpreadsheetApp.getActiveSheet();
  //var ss = SpreadsheetApp.getActiveSpreadsheet();
  //var sheet = ss.getSheetByName('New unallocated studies');
  var rows = sheet.getDataRange();
  var numRows = rows.getNumRows();
  var values = rows.getValues();
  var lastRow = sheet.getLastRow();
  var lastColumn = sheet.getLastColumn();
  // var activeRow = sheet.getActiveRange().getRow();
  var activeRow = e.range.getRow();
  //var activeRow = 4;
  //  Logger.log(activeRow)

  for (var m = 0; m < lastColumn; m++) {
    if (values[0][m] == "Username") {
      var userNameColumn = m;
    }
    else if (values[0][m] == "Email Address") {
      var userNameColumn = m;
    }
    else if (values[0][m].indexOf("Cost Center Code (recruiters do not edit this column)") >= 0) {
      var codeColumn = m;
    }
    else if (values[0][m].indexOf("Cost Center Name (recruiters do not edit this column)") >= 0) {
      var nameColumn = m;
    }
    else if (values[0][m].indexOf("supported") >= 0) {
      var supportColumn = m;
    }
    else if (values[0][m].indexOf("Product Area (recruiters do not edit this column)") >= 0) {
      var PAColumn = m;
    }
    else if (values[0][m].indexOf("Pod") >= 0) {
      var PodColumn = m;
    }
    else if (values[0][m].indexOf("BU Code") >= 0) {
      var BUColumn = m;
    }
    else if (values[0][m].indexOf("Where in the world would the participants in your study be located?") >= 0) {
      var RegionColumn = m;
    }
    else if (values[0][m].indexOf("Paste the link to your Study Spreadsheet here. Ensure that your Screener Form has been linked to it and that the sharing settings for both are: private, with uxr-operations-share@google.com added as an editor/collaborator.") >= 0) {
      var urlColumn = m;
    }
    else if (values[0][m].indexOf("What's the name of the study?") >= 0) {
      var studyNameColumn = m;
    }
    else if (values[0][m].indexOf("Researcher Location") >= 0) {
      var researcherLocationColumn = m;
    }
    else if (values[0][m].indexOf("BU code for Cases") >= 0) {
      var BUCasesColumn = m;
    }
    else if (values[0][m].indexOf("Do your participants belong to any of the following categories?") >= 0) {
      var categoryColumn = m;
    }
  }



  var researcherID = 0;

  var tempResearcher = e.values[userNameColumn].substring(0, e.values[userNameColumn].indexOf("@google.com"));
  Logger.log('tempResearcher: ' + tempResearcher);
  var myUrl = e.values[urlColumn];

  var myStudyName = e.values[studyNameColumn];


  try {
    researcherID = getGooglerInfo(tempResearcher);
  }
  catch (e) {

  }

  /** Moved email study receipt to Single Intake form  */
 // emailStudyReceipt(sheet, researcherID, myUrl, myStudyName, activeRow);
  /** nothing below needs to execute as its not relative to alStudyqueue */

  sendAllocationNotification(e)


  if (researcherID == 0) {
    return;
  }


  //var costCenter = researcherID.costCenter;
  var ccString = researcherID.costCenter;
  var ccCode = ccString.substring(0, ccString.indexOf(":")); //Person.costCenterNumber;
  var ccName = ccString.substring(ccString.indexOf(" ") + 1); //Person.costCenter;

  //var ccCode = researcherID.costCenterNumber;
  /** remove for Multivitiam */
  // if (ccCode == "71T") {
  //   var manager = getGooglerInfo(researcherID.manager);
  //   var name = manager.fullName;

  //   var ccString = manager.costCenter;
  //   var ccCode = ccString.substring(0, ccString.indexOf(":")); //Person.costCenterNumber;
  //   var ccName = ccString.substring(ccString.indexOf(" ") + 1); //Person.costCenter;

  //   //costCenter = manager.costCenter;
  //   //ccCode = manager.costCenterCode;
  // }


  //var ccName = costCenter;
  /** removed from answerlab studyqueue */
  // var supported = checkSupport(tempResearcher);
  /*
  let isIndiaApacPod = podAllocationIndiaApac(e.namedValues);
  let podValue = !!isIndiaApacPod ? isIndiaApacPod : supported[3]
  
  */
  /** update multivitams this data come through single intake now */
  // sheet.getRange(activeRow, codeColumn + 1, 1, 1).setValue(ccCode);
  // sheet.getRange(activeRow, nameColumn + 1, 1, 1).setValue(ccName);
  // sheet.getRange(activeRow, supportColumn + 1, 1, 1).setValue(supported[0]);
  // sheet.getRange(activeRow, PAColumn + 1, 1, 1).setValue(supported[1]);
  // sheet.getRange(activeRow, researcherLocationColumn + 1, 1, 1).setValue(supported[2]);
  // sheet.getRange(activeRow, PodColumn + 1, 1, 1).setValue(supported[3]);
  // sheet.getRange(activeRow, BUColumn + 1, 1, 1).setValue(supported[4]);
  // removed 10.15
  // sheet.getRange(activeRow, BUCasesColumn + 1, 1, 1).setValue(supported[5]);
  /** removed from answerlab studyqueue */
  // if (supported[0] != "Not on list") {
  //   if (sheet.getRange(activeRow, RegionColumn + 1, 1, 1).getValue() == "Europe; Middle East; Africa") {
  //     sheet.getRange(activeRow, PodColumn + 1, 1, 1).setValue("Pod EMEA");
  //   }
  //   else if (sheet.getRange(activeRow, RegionColumn + 1, 1, 1).getValue() == "Asia (excluding India); Pacific Islands") {
  //     sheet.getRange(activeRow, PodColumn + 1, 1, 1).setValue("Pod APAC");
  //   }
  //   else if (sheet.getRange(activeRow, RegionColumn + 1, 1, 1).getValue() == "Australia; New Zealand") {
  //     sheet.getRange(activeRow, PodColumn + 1, 1, 1).setValue("Pod APAC");
  //   }
  //   else if (sheet.getRange(activeRow, RegionColumn + 1, 1, 1).getValue() == "India") {
  //     sheet.getRange(activeRow, PodColumn + 1, 1, 1).setValue("Pod APAC");
  //   }
  //   // else if((sheet.getRange(activeRow, categoryColumn+1, 1, 1).getValue()).indexOf("Education")>=0){
  //   //   sheet.getRange(activeRow, PodColumn+1, 1, 1).setValue("Pod 2");
  //   // }
  // }
  // try {
  //   Logger.log("Manager List", "unified_rollup_level_1")
  //   const getDataByTypes = fetchTwoDataPoints("Manager List", "unified_rollup_level_1")
  //   updateDataPointsByUsers(getDataByTypes)
  // } catch (e) {
  //   Logger.log(`update implementation fail: ${e}`)
  // }

  /** multivitiam remove allocation */
  var activeValues = sheet.getRange(activeRow, 1, 1, sheet.getLastColumn()).getValues();
  // allocation(values[0], activeValues[0], e);
  // Release the lock so that other processes can continue.
  debugger
  lock.releaseLock();
  function fetchSupportedByAndPodLeads(e) {

    const whomWeSupport = SpreadsheetApp.openById("1s7hcB2ipDz2bTfbl4v61rjU9N9pmPXgm1BsLKEnYf_Q");
    const emailAddress = e.namedValues?.["Email Address"][0] || null
    const ldap = emailAddress.slice(0, emailAddress.indexOf('@'))
    const bu = e.namedValues?.["Product Area (recruiters do not edit this column)"][0] || null
    const pod = e.namedValues?.["Pod"][0] || null
    /** match criters */
    const hasFullSupport = (props) => props?.["Support level"].includes("Full")
    const podMatch = (props) => props?.["pod"] === pod || null
    const buMatch = (props) => props?.["Business Unit"] === bu || null
    const ldapMatch = (props) => props?.["ldap"] === ldap

    const AllLdapsSheet = whomWeSupport.getSheetByName("All ldaps");
    const { body: allLdapBody } = getDataBySheet(AllLdapsSheet);
    const matchFull = allLdapBody.filter(hasFullSupport)
    const matchPod = allLdapBody.slice(-1).filter(podMatch)
    const matchBU = allLdapBody.slice(-1).filter(buMatch);

    const hasAllLdapsMatch = (props) => [hasFullSupport, ldapMatch].every(fn => fn(props)) // removed podMatch, buMatch,
    // const hasAllLdapsMatch2 = (props) => [hasFullSupport, podMatch, buMatch, ldapMatch].map(fn => fn(props))
    // const match2 = allLdapBody.map(hasAllLdapsMatch2) || null

    const match = allLdapBody.filter(hasAllLdapsMatch) || null
    /** extract emails */
    const splitAndAddDomain = (str) => typeof str === 'string' ? str.split(', ').map(str => `${str}@google.com`) : []
    const getSupportedBy = (acc, { "Supported by": recruiters }) => {
      if (recruiters === null || recruiters === "TBD") { return acc }
      return [...acc, ...splitAndAddDomain(recruiters)];
    }
    const getPodLeads = ({ "pod_lead": podLead }) => {
      return splitAndAddDomain(podLead)
    }
    const filterOutNulls = str => str !== null
    const podLeads = match.flatMap(getPodLeads).join(', ')
    const supportedBy = match.reduce(getSupportedBy, [])
    // .filter(filterOutNulls)]

    const getNames = (acc, str) => {
      try {
        return acc.concat(getGooglerInfo(str).firstName)
      } catch (e) {
        return acc.concat(str)
      }
    }
    const supportedByNames = supportedBy.reduce(getNames, []).join(', ')
    const supportedByString = supportedBy.join(', ')
    
    return match.length !== 0 ? {
      supportedByString,
      supportedByNames,
      podLeads
    } : getNotsupported()
  
  function getNotsupported() {
    //pull from PAs sheet
    Logger.log("running not supported")
      const PAsSheet = whomWeSupport.getSheetByName("PAs");
      const { body: pasBody } = getDataBySheet(PAsSheet);
      const hasBUNotSupported = (props) => props?.["Business Unit"] === "Not supported"
      const notSupportedMatch = pasBody.find(hasBUNotSupported);
      const supportedBy = notSupportedMatch?.["Recruiter(s)  (do not edit, updates automatically)"] || ""
      const podLeadMatch = notSupportedMatch?.["Pod lead"] || ""

      const strToArr = ( str) => (typeof str === 'string' || str !== "") ? str.split(',') : []
      const noTBD = (arr) => arr.filter(str => str !== "TBD");
      const addAtGoogle = (str) => `${str}@google.com`
      const getName = str => {
        try{
          return getGooglerInfo(str).firstName
        } catch(e){
          return str
        }
      }

      const supportedByStringArr = compose(noTBD, strToArr)(supportedBy);
      const supportedByString = supportedByStringArr.map(addAtGoogle).join(', ')
      // const supportedByNames = supportedByStringArr.map(getName);
      const podLeadArr = podLeadMatch === "" ? [] : compose(noTBD, strToArr)(podLeadMatch);
      const podLeads = podLeadArr.length === 0 ? "" : podLeadArr.map(addAtGoogle).join(', ')
      debugger
      return {
        supportedByString,
        supportedByNames: supportedByString,
        podLeads: podLeads
      }
    }
  }
  function fetchStudyDetails({ namedValues }) {
    const studyDetails = Object.keys(namedValues).reduce((acc, next) => {
      let question = next
      let answer = namedValues[next][0]
      if (question === "Researcher Location") {
        return acc.concat(`<li>${question}: ${answer} </li>`)
      }
      
      if(answer === ""){
        return acc
      }
      return acc.concat(`<li>${answer} </li>`)
    }, "<ul> ").concat("</ul>")
    return studyDetails
  }
  function sendAllocationNotification(e) {

    const { supportedByNames, supportedByString, podLeads } = fetchSupportedByAndPodLeads(e)
    const [myURL] = e?.namedValues["Paste the link to your Study Spreadsheet here. Ensure that your Screener Form has been linked to it and that the sharing settings for both are: private, with uxr-operations-share@google.com added as an editor/collaborator."] || null
    const [businessUnit] = e?.namedValues["Product Area (recruiters do not edit this column)"] || [null]
    const studydetails = fetchStudyDetails(e)
    const getValueFromNamedValues = (str) => e?.namedValues.hasOwnProperty(str) ? e?.namedValues[str][0] : `Unable to find ${str}`
    const region = getValueFromNamedValues("Where in the world would the participants in your study be located?").split(', ')
    let ldap = getValueFromNamedValues("Email Address")
    ldap = ldap.slice(0, ldap.indexOf('@'))
    const timestamp = new Date()
    debugger
    // var subject = "[Multivitamin Test] New study request from: " + ldap + " (" + timestamp + ")";

    var claimURL = "https://docs.google.com/spreadsheets/d/194r-7nE90vmLSmq87ylU4DETJNg9wbYeKKa6OS8mr0Y/edit"
    /** DEV ENV  */
    claimUrl = SpreadsheetApp.getActiveSpreadsheet().getUrl()
    claimURL = "<a href=\"" + claimURL + "\">moved to the active tab</a>";

    debugger
    const htmlBody = "Hello " + supportedByNames + ",<br><br>" +
      "A new " + myURL + " has come in from " + ldap + ".<br>" +
      "Please pick up this study or escalate to your manager if needed. " +
      "You and your manager will both be emailed again if the study is not " + claimURL + " within 2 business days.<br>" +
      "Here are the full request details:<br><br>" + studydetails +
      "<br><br>" +
      "Thank you!<br>" +
      "Super duper allocator script Ninja <br><br>";

    
    var mailFromIndex = GmailApp.getAliases().indexOf("uxi-alerts@google.com");
    var mailFrom = GmailApp.getAliases()[mailFromIndex];
    let emailOptions = {from: mailFrom, cc: podLeads, replyTo: podLeads };
    var subject = `[Multivitamin Test] New study request from:  ${ldap}  (${new Date()})`;
    debugger
    Logger.log('before send outbout')
    _sendOutbondNotification({ to: supportedByString, subject, htmlBody, options: emailOptions })


  }
  function _sendOutbondNotification({ to, subject, htmlBody, options }) {
    try {
      // GmailApp.sendEmail('douglascox@google.com', 'test' + subject, body, { htmlBody: body, from: mailFrom, cc: podLeads, replyTo: podLeads });
      const sendEmailParams = {
        to: to,
        subject,
        options,
      }
      // Logger.log(body)
       htmlBody = JSON.stringify(sendEmailParams)+'<br /><br />' + htmlBody
      Logger.log("before study allocation email sent")
       GmailApp.sendEmail(devEmails, subject, "Study Allocation Notification", { htmlBody});
      debugger
      // GmailApp.sendEmail(to, subject, body, { htmlBody: body, from: mailFrom, cc: podLeads,  replyTo: podLeads });
      return
    } catch (e) {
      throw(e)
    }
  }

  function updateDataPointsByUsers(getDataByTypes) {
    const activeRowIdx = activeRow - 1; // minus 1 is done above, 
    const currentRow = values[activeRowIdx]
    const requestor = currentRow[userNameColumn]
    const dataPointsByUser = getDataByTypes(requestor)
    const headers = Object.keys(dataPointsByUser)
    const headerRow = values[0]
    const getHeaderIdx = (header) => headerRow.indexOf(header)
    headers.forEach(updateByHeader)

    function updateByHeader(str) {
      const headerIdx = getHeaderIdx(str)
      if (headerIdx === -1) { return }
      const dataPoint = dataPointsByUser[str]
      debugger
      sheet.getRange(activeRow, headerIdx + 1, 1, 1).setValue(dataPoint);
    }
  }
  function fetchTwoDataPoints(type1, type2) {
    const sheet = fetchSSSheet("1uRYj1W6rz8PaGooW1P-iwguIjY9fPQi6CAhQ1gx8U7A")("All ldaps")
    const { body } = getDataBySheet(sheet)
    return function byEmailAddress(emailAddress) {
      const match = body.find(({ ldap }) => (new RegExp(ldap, 'gi')).test(getLdapfromEmail(emailAddress)))

      return match === 0 ? null : { [type1]: getMatchType(match, type1), [type2]: getMatchType(match, type2) }

      function getMatchType(match, type) {
        try {
          const matchType = match[type]
          return matchType
        } catch (e) {
          return `${type} Not Found`
        }
      }
      function getLdapfromEmail(emailAddress) {
        const atIdx = emailAddress.indexOf('@');
        return emailAddress.slice(0, atIdx)
      }
    }
    function fetchSSSheet(id) {
      const ss = SpreadsheetApp.openById(id)
      return function fetchSheetByName(name) {
        return ss.getSheetByName(name)
      }
    }
  }
}

// update Columns to reference idx before it was pulling the value
function checkSupport(ldap) {
  var supportSpreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  var supportSheet = supportSpreadsheet.getSheetByName("Who we cover");
  var supportSheetRows = supportSheet.getDataRange();
  var values = supportSheetRows.getValues();
  var paSheet = supportSpreadsheet.getSheetByName("PAs we cover");
  var paSheetRows = paSheet.getDataRange();
  var paValues = paSheetRows.getValues();
  //var found = 0;
  var { headerObj } = getDataBySheet(supportSheet);
  var headers = Object.keys(headerObj)
  var supportTypeColumn = headers.indexOf("Support level");
  //var ldapColumn = headerObj["BU Code"] || headerObj["level_of_support"];
  var buColumn = headers.indexOf("Business Unit");
  var podColumn = headers.indexOf("pod");
  var locationColumn = headers.indexOf("Location");
  var buCodeColumn = headers.indexOf("BU Code");
  //  var buCodeCasesColumn = headers.indexOf("BU code for Cases");
  var userNameColumn = headers.indexOf("ldap");

  debugger
  for (var i = 1; i < values.length; i++) {
    if (values[i][userNameColumn] == ldap) {
      // accounts for the removal of "BU code for Cases"
      //      return (["On the list! (" + values[i][supportTypeColumn] + ")", values[i][buColumn], values[i][locationColumn], values[i][podColumn], values[i][buCodeColumn], values[i][buCodeCasesColumn]]);
      return (["On the list! (" + values[i][supportTypeColumn] + ")", values[i][buColumn], values[i][locationColumn], values[i][podColumn], values[i][buCodeColumn],]);
    }
  }

  var managerChain = getManagerChain(ldap);
  var googler = getGooglerInfo(ldap);
  var job = googler.title;
  // 6.23.20 @douglascox moved to reference id
  // var exceptionSheet = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/1uRYj1W6rz8PaGooW1P-iwguIjY9fPQi6CAhQ1gx8U7A/edit");
  var exceptionSheet = SpreadsheetApp.openById("1uRYj1W6rz8PaGooW1P-iwguIjY9fPQi6CAhQ1gx8U7A");
  var supportedTitleTab = exceptionSheet.getSheetByName("Supported Exceptions - Titles");
  var unsupportedTitleTab = exceptionSheet.getSheetByName("Unsupported Exceptions - Titles");
  var supportedTitleArray = [];
  var unsupportedTitleArray = [];
  var supportedTitleValues = supportedTitleTab.getDataRange().getValues();
  var unsupportedTitleValues = unsupportedTitleTab.getDataRange().getValues();
  var supportedLdapsTab = exceptionSheet.getSheetByName("Supported Exceptions - LDAPs");
  var unsupportedLdapsTab = exceptionSheet.getSheetByName("Unsupported Exceptions - LDAPs");
  var supportedLdapsArray = [];
  var unsupportedLdapsArray = [];
  var supportedLdapsValues = supportedLdapsTab.getDataRange().getValues();
  var unsupportedLdapsValues = unsupportedLdapsTab.getDataRange().getValues();

  for (var s = 1; s < supportedTitleValues.length; s++) {
    supportedTitleArray.push(supportedTitleValues[s][0]);
  }

  for (var s = 1; s < unsupportedTitleValues.length; s++) {
    unsupportedTitleArray.push(unsupportedTitleValues[s][0]);
  }

  for (var s = 1; s < supportedLdapsValues.length; s++) {
    supportedLdapsArray.push(supportedLdapsValues[s][0]);
  }

  for (var s = 1; s < unsupportedLdapsValues.length; s++) {
    unsupportedLdapsArray.push(unsupportedLdapsValues[s][0]);
  }

  if (((job.toLowerCase().indexOf("research") < 0) && (supportedTitleArray.indexOf(job) < 0)) && (supportedLdapsArray.indexOf(ldap) < 0) || ((unsupportedTitleArray.indexOf(job) >= 0) || (unsupportedLdapsArray.indexOf(ldap) >= 0))) {
    return (["Not on list", "", "", "", ""]);
  }



  for (var m = 0; m < managerChain.length; m++) {

    if (managerChain[m] != "") {
      for (var i = 1; i < paValues.length; i++) {
        var tempCheck = paValues[i][1] + ", " + paValues[i][10] + ",";
        var tempCheckArray = tempCheck.split(",");
        for (var c = 0; c < tempCheckArray.length; c++) {
          if (tempCheckArray[c] == managerChain[m]) {

            //send email to Pod Leads to let them know about the exception

            var email = "uxr-ops-pod-leads@google.com, jgolenbock@google.com";
            var subject = "Support exception (manager supported)";
            var body = //"Note: This email was sent from Janice’s account but is triggered automatically by a script. No need to reply to Janice. <br><br>" +
              "Hello Pod Leads,<br><br>" +
              "This is a notification that a support exception was made.<br>" +
              "A new study request has come in from <b>" + ldap + "@</b>.<br>" +
              "This was originally flagged as not supported but since the manager, <b>" + managerChain[m] + "@</b>, is on the list, we will support.<br>" +
              "<br><br>" +
              "Thank you!<br>" +
              "Super duper allocator script Ninja <br><br>";// +
            // "Note: This email was sent from Janice’s account but is triggered automatically by a script.<br>";

            //send email
            var mailFromIndex = GmailApp.getAliases().indexOf("uxi-alerts@google.com");
            var mailFrom = GmailApp.getAliases()[mailFromIndex];
            /** production */
            // GmailApp.sendEmail(email, subject, body, { htmlBody: body, from: mailFrom }); 
            /** development */
            const sendEmailParams = {
              to: email,
              subject,
              body,
              options: { from: mailFrom }
            }
            body = JSON.stringify(sendEmailParams) + body
            GmailApp.sendEmail(devEmails, subject, body, { htmlBody: body, from: mailFrom });



            return (["On the list! (" + values[i][9] + ")", paValues[i][0], "", paValues[i][2], paValues[i][9]]);
          }
        }
      }
    }
  }

  //1 and 9
  return (["Not on list", "", "", "", ""]);

}

function getManagerChain(ldap) {
  var managerChain = [];
  var myldap = ldap;
  var found = true;

  do {
    found = false;
    try {
      myldap = getGooglerInfo(myldap).managers;
      managerChain.push(myldap);

      found = true;
    }
    catch (e) {
      found = false;
    }
  } while (found == true);
  return managerChain;
}


function calculateComplexity(row) {
  var sheet1 = SpreadsheetApp.getActiveSpreadsheet();

  var complexityTab = sheet1.getSheetByName("Complexity Score");
  var complexityValues = complexityTab.getDataRange().getValues();
  var activeSheet = sheet1.getSheetByName("Active Studies");
  var activeValues = activeSheet.getDataRange().getValues();
  var lastColumn = activeSheet.getLastColumn();

  for (var m = 0; m < lastColumn; m++) {
    if (activeValues[0][m] == "What type of support do you need?") {
      var supportColumn = m;
    }
    else if (activeValues[0][m] == "How many participants do you need for this study?") {
      var pptColumn = m;
    }
    else if (activeValues[0][m] == "What is the targeted first date of the study?") {
      var startDateColumn = m;
    }
    else if (activeValues[0][m] == "Timestamp") {
      var requestDateColumn = m;
    }
    else if (activeValues[0][m] == "Which type of study are you planning to do?") {
      var inPersonColumn = m;
    }
    else if (activeValues[0][m] == "Where in the world would the participants in your study be located?") {
      var nonNApptColumn = m;
    }
    else if (activeValues[0][m] == "How difficult is this study to recruit for based on the locations requested?") {
      var locsColumn = m;
    }
    else if (activeValues[0][m] == "How difficult is it to recruit for the requested ppts?") {
      var profilesColumn = m;
    }
    else if (activeValues[0][m] == "Phone screens required?") {
      var phoneScreenColumn = m;
    }
    else if (activeValues[0][m] == "Does your study require you to get approval(s) from Legal/E&C/PWG?") {
      var approvalColumn = m;
    }
    else if (activeValues[0][m] == "In which city/region/country will study sessions take place?") {
      var cityColumn = m;
    }
    else if (activeValues[0][m] == "Complexity Score") {
      var complexityScoreColumn = m;
    }
  }

  var studyStatus = "";


  var daysMin = complexityValues[6][2];
  var daysMax = complexityValues[6][3];
  var daysWeight = complexityValues[6][4];

  var locsMin = complexityValues[7][2];
  var locsMax = complexityValues[7][3];
  var locsWeight = complexityValues[7][4];

  var profilesMin = complexityValues[8][2];
  var profilesMax = complexityValues[8][3];
  var profilesWeight = complexityValues[8][4];

  var inPersonWeight = complexityValues[13][2];
  var nonNAWeight = complexityValues[14][2];
  var phonescreenWeight = complexityValues[15][2];
  var approvalWeight = complexityValues[16][2];

  var maxNumPts = complexityValues[0][1];
  var criteriaContrib = complexityValues[1][1];
  var multContrib = complexityValues[2][1];

  var pptWeight = complexityValues[19][2];
  var bayAreaWeight = complexityValues[20][2];


  if ((sheet1.getActiveSheet().getName() == "New unallocated studies") || (sheet1.getActiveSheet().getName() == "Rolling Studies")) {
    studyStatus = "new";
    var activeRow = activeSheet.getLastRow();
    var activeValues = activeSheet.getRange(activeRow, 1, 1, activeSheet.getLastColumn()).getValues();
  }
  else if ((sheet1.getActiveSheet().getName() == "Active Studies") || (sheet1.getActiveSheet().getName().indexOf("Closed Studies") >= 0)) {
    studyStatus = "change";
    var activeRow = activeSheet.getActiveCell().getRow();

    var activeValues = activeSheet.getRange(activeRow, 1, 1, activeSheet.getLastColumn()).getValues();
  }
  else {
    return;
  }

  var supportType = activeValues[0][supportColumn];
  var pptsInput = activeValues[0][pptColumn];

  var startDate = new Date(activeValues[0][startDateColumn]);
  var requestDate = new Date(activeValues[0][requestDateColumn]);


  //Write function to calculate # of business days between request to study date 
  networkdays = function (requestDate, startDate) {
    var requestDate = typeof requestDate == 'object' ? requestDate : new Date(requestDate);
    var startDate = typeof startDate == 'object' ? startDate : new Date(startDate);
    if (startDate > requestDate) {
      var days = Math.ceil((startDate.setHours(23, 59, 59, 999) - requestDate.setHours(0, 0, 0, 1)) / (86400 * 1000));
      var weeks = Math.floor(Math.ceil((startDate.setHours(23, 59, 59, 999) - requestDate.setHours(0, 0, 0, 1)) / (86400 * 1000)) / 7);

      days = days - (weeks * 2);
      days = requestDate.getDay() == 0 && startDate.getDay() != 6 ? days - 1 : days;
      days = startDate.getDay() == 6 && requestDate.getDay() != 0 ? days - 1 : days;

      return days;
    }
    return null;
  };


  var daysInput = (networkdays(requestDate, startDate) - 1);
  var inPersonInput = activeValues[0][inPersonColumn];
  var nonNApptInput = activeValues[0][nonNApptColumn];

  //Logger.log("inPersonInput: " + inPersonInput);
  if (inPersonInput.indexOf("In person") >= 0) {
    inPersonInput = "1";
  }
  else {
    inPersonInput = "0";
  }

  var nonNApptScore = 0;
  var regionsArray = nonNApptInput.split(", ");
  if (regionsArray.length > 1) {
    nonNApptScore = 1;
  }
  else if (regionsArray[0] != "United States and Canada") {
    nonNApptScore = 1;
  }


  var locsInput = activeValues[0][locsColumn];
  var profilesInput = activeValues[0][profilesColumn];
  var phoneScreenInput = activeValues[0][phoneScreenColumn];
  var approvalInput = activeValues[0][approvalColumn];

  var daysScore = (daysInput - daysMin) / (daysMax - daysMin) * daysWeight;
  if (daysScore >= daysWeight) {
    daysScore = daysWeight;
  }
  else if (daysScore < 0) {
    daysScore = 0;
  }

  var locsScore = (locsInput - locsMin) / (locsMax - locsMin) * locsWeight;
  if (locsScore >= locsWeight) {
    locsScore = locsWeight;
  }
  var profilesScore = (profilesInput - profilesMin) / (profilesMax - profilesMin) * profilesWeight;
  if (profilesScore >= profilesWeight) {
    profilesScore = profilesWeight;
  }
  var sumCriteria = daysWeight + locsWeight + profilesWeight;
  var phoneScore = 0;


  if (phoneScreenInput.toLowerCase() == "yes") {
    phoneScore = phonescreenWeight;
  }
  else if (phoneScreenInput.toLowerCase() == "no") {
    phoneScore = 0;
  }

  if (approvalInput.toLowerCase() == "yes") {
    approvalInput = 1;
  }
  else if (approvalInput.toLowerCase() == "no") {
    approvalInput = 0;
  }

  var approvalScore = approvalInput * approvalWeight;

  var inPersonScore = inPersonInput * inPersonWeight;

  var nonNApptScore = nonNApptScore * nonNAWeight;

  var sumMultipliers = phonescreenWeight + approvalWeight + inPersonWeight + nonNAWeight;

  var criteria = (daysScore + locsScore + profilesScore) / sumCriteria * criteriaContrib;
  var multipliers = (phoneScore + approvalScore + inPersonScore + nonNApptScore) / sumMultipliers * (100 - criteriaContrib);
  var preComplexityScore = Math.round(criteria + multipliers);

  // Need to add # of ppts multiplier
  var pptsScore = pptsInput * pptWeight;

  // Need to add Bay Area tax multiplier
  var bayAreaInput = activeValues[0][cityColumn];

  if ((bayAreaInput.indexOf("San Francisco") >= 0) || (bayAreaInput.indexOf("San Bruno") >= 0) || (bayAreaInput.indexOf("Sunnyvale") >= 0) || (bayAreaInput.indexOf("Mountain View") >= 0)) {
    bayAreaInput = 1;
  }
  else {
    bayAreaInput = 0;
  }

  var bayAreaScore = bayAreaInput * (bayAreaWeight * preComplexityScore);

  var complexityScore = Math.round(preComplexityScore + pptsScore + bayAreaScore);

  // If this is a self-recruitment support study, assign complexity score of 20.
  if (String(supportType) == "Self-recruitment support - only need an invitation list shared OR access granted to a resource") {
    complexityScore = 20;
    activeSheet.getRange(activeRow, complexityScoreColumn + 1, 1, 1).setValue(complexityScore);
    return;
  }


  //Log all scores that are over 100 in the "Complexity Scores Over 100" tab and cap scores to 100
  // 2021.03.24 removed csOvercapTab @douglascox
  // var csOvercapTab = sheet1.getSheetByName("Complexity Scores Over 100");
  var scoreArray = activeValues[0];

  // if (complexityScore > 100) {
  //   scoreArray[8] = complexityScore;
  //   csOvercapTab.appendRow(scoreArray);
  //   complexityScore = 100;
  // }

  // Logger.log(complexityScore);
  // Logger.log(complexityScoreColumn);
  // Logger.log(activeSheet.getName());
  // Logger.log(row);
  if (activeSheet.getName() == "New unallocated studies") {
    activeSheet.getRange(activeSheet, complexityScoreColumn + 1, 1, 1).setValue(complexityScore);
  }
  else if (activeSheet.getName() == "Active Studies") {
    activeSheet.getRange(activeRow, complexityScoreColumn + 1, 1, 1).setValue(complexityScore);
  }

  //  activeSheet.getRange(activeRow, complexityScoreColumn+1, 1, 1).setValue(complexityScore);
}

function claimStudy() {

  // Get a public lock on this script, because we're about to modify a shared resource.
  var lock = LockService.getScriptLock();

  // Wait for up to 90 seconds for other processes to finish.
  lock.waitLock(90000);

  var studyType = "";
  var sheet1 = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = sheet1.getActiveSheet();

  if (sheet.getName() == "New unallocated studies") {
    studyType = "regular";
  }
  else if (sheet.getName() == "Rolling Studies") {
    studyType = "rolling";
  }
  else if (sheet.getName() == "Canceled / Unsupported / Other") {
    studyType = "postponed";
  }
  else {
    return;
  }

  var rows = sheet.getDataRange();
  var numRows = rows.getNumRows();
  var values = rows.getValues();
  var lastRow = sheet.getLastRow();
  var lastColumn = sheet.getLastColumn();
  var ActiveRow = sheet1.getActiveRange().getRow();
  var ActiveColumn = sheet1.getActiveRange().getColumn();
  var ActiveCell = sheet1.getActiveCell();
  var numActiveRows = sheet1.getActiveRange().getNumRows();
  var numActiveCols = sheet1.getActiveRange().getNumColumns();
  var rollingURL = "";
  var mypod = "";

  var activeSheet = sheet1.getSheetByName("Active Studies");

  var me = Session.getActiveUser().getEmail();
  me = me.substring(0, me.indexOf("@google.com"));
  var requestArray = ["", me, Utilities.formatDate(new Date(), "GMT", "MM/dd/yyyy' 'HH:mm"), "", "", "", "", "", "", ""];
  //var requestArray = ["", me, (new Date()).toUTCString(),"", "", "", "", "", "", ""];

  // var podSheet1 = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/1uRYj1W6rz8PaGooW1P-iwguIjY9fPQi6CAhQ1gx8U7A/edit#gid=624907318");
  // var podSheet1 = SpreadsheetApp.openById("1uRYj1W6rz8PaGooW1P-iwguIjY9fPQi6CAhQ1gx8U7A");
  /** Multivitiam testing pods */
  var podSheet1 = SpreadsheetApp.openById("1s7hcB2ipDz2bTfbl4v61rjU9N9pmPXgm1BsLKEnYf_Q");
  var podSheet = podSheet1.getSheetByName("Pods");
  var podValues = podSheet.getDataRange().getValues();
  var pod = "";
  var podMembers = [];

  for (var p = 1; p < podValues.length; p++) {
    pod = podValues[p][0];
    podMembers = podValues[p][1].split(", ");
    if (podMembers.indexOf(me) >= 0) {
      Logger.log("Pod: " + pod);
      mypod = pod;
    }
  }



  for (var i = 1; i < lastColumn; i++) {
    if (values[0][i] == "Username") {
      var ldapColumn = i;
    }
    else if (values[0][i] == "Email Address") {
      var ldapColumn = i;
    }
    else if (values[0][i] == "What's the name of the study?") {
      var nameColumn = i;
    }
    else if (values[0][i] == "Paste the link to your Study Spreadsheet here. Ensure that your Screener Form has been linked to it and that the sharing settings for both are: private, with uxr-operations-share@google.com added as an editor/collaborator.") {
      var URLColumn = i;
    }
    else if (values[0][i] == "Timestamp") {
      var timestampColumn = i;
    }
    else if (values[0][i] == "Pod") {
      var podColumn = i;
    }
    else if (values[0][i] == "In which city/region/country will study sessions take place?") {
      var locationColumn = i;
    }
    else if (values[0][i] == "What type of support do you need?") {
      var supportColumn = i;
    }
    else if (values[0][i] == "How many participants do you need for this study?") {
      var pptColumn = i;
    }
  }

  var supportType = values[ActiveRow - 1][supportColumn];

  // 02.10.21 adding prompt to validate single row selection, and correct selection
  const getCellValueByHeader = (header) => values[0].indexOf(header) !== -1 ? values[ActiveRow - 1][values[0].indexOf(header)] : "N/A"
  const isSingleRowSelected = ({ getNumRows }) => 1 === getNumRows()
  if (!isSingleRowSelected(sheet1.getActiveRange())) {
    Browser.msgBox(`"Please ensure that only one row is highlighted. Claim Study can only process a single study at a time. Highlighting more than one row will cause other studies to get lost."`)
    return
  }
  const validationCheck = { "Study Name": getCellValueByHeader("What's the name of the study?"), "Email Address": getCellValueByHeader("Email Address"), "Targeted first date of the study": getCellValueByHeader("What is the targeted first date of the study?") }
  const isValidSelection = promptConfirmationClaimStudy("To Claim this Study, please confirm the follwing.", validationCheck);
  debugger
  if (!isValidSelection) {
    return
  }

  // Display a dialog box
  var ui = SpreadsheetApp.getUi();
  if ((studyType == "regular") || (studyType == "postponed")) {
    var response = ui.alert('Click Yes to confirm you are claiming ' + values[ActiveRow - 1][ldapColumn] + '\'s study: ' + values[ActiveRow - 1][nameColumn], ui.ButtonSet.YES_NO);
  }
  else if (studyType == "rolling") {
    var response = ui.alert('Click Yes to confirm you are claiming the rolling study: ' + values[ActiveRow - 1][nameColumn], ui.ButtonSet.YES_NO);
  }
  if (response == "YES") {

    if (mypod != values[ActiveRow - 1][podColumn]) {
      var response = ui.prompt('This study is currently assigned to ' + values[ActiveRow - 1][podColumn] + '. Would you like to change the pod assignment? Leave blank to make no change.', ui.ButtonSet.OK);
      if (response.getResponseText() != "") {
        values[ActiveRow - 1][podColumn] = response.getResponseText();
      }
    }

    for (var i = 0; i < values[ActiveRow - 1].length; i++) {
      if (studyType == "rolling") {

        if ((values[0][i] == "Username") || (values[0][i] == "Email Address")) {
          var response = ui.prompt('Is this the correct researcher for this round: ' + values[ActiveRow - 1][i] + '?  Click Ok if this is correct, or enter the correct email address and then click Ok (you can always edit manually later)', ui.ButtonSet.OK_CANCEL);
          if (response.getSelectedButton() != "OK") {
            return;
          }

          else if (response.getResponseText() == "") {
            requestArray.push(values[ActiveRow - 1][i]);
          }
          else {
            requestArray.push(response.getResponseText());
          }

        }

        else if (i == URLColumn) {
          var response = ui.prompt('Provide a link to this round\'s study spreadsheet', ui.ButtonSet.OK_CANCEL);
          if (response.getSelectedButton() != "OK") {
            return;
          }
          else if (response.getResponseText() == "") {
            requestArray.push("");
          }
          else {

            rollingURL = response.getResponseText();
            requestArray.push(rollingURL);

          }

        }

        else if (values[0][i] == "Current Round") {
          var newRound = values[ActiveRow - 1][i] + 1;
          var response = ui.prompt('Is this round ' + newRound + ' of this study?  Click Ok if this is correct, or enter the correct value and then click Ok (you can always edit manually later)', ui.ButtonSet.OK_CANCEL);
          if (response.getSelectedButton() != "OK") {
            return;
          }
          else if (response.getResponseText() == "") {
            requestArray.push(Utilities.formatDate(new Date(), "GMT", "MM/dd/yyyy' 'HH:mm")).toString();
            sheet.getRange(ActiveRow, i + 1, 1, 1).setValue(newRound);
          }
          else {
            requestArray.push(Utilities.formatDate(new Date(), "GMT", "MM/dd/yyyy' 'HH:mm")).toString();
            sheet.getRange(ActiveRow, i + 1, 1, 1).setValue(response.getResponseText());
            newRound = response.getResponseText();
          }
        }
        else if (values[0][i] == "What's the name of the study?") {
          requestArray.push(values[ActiveRow - 1][i] + " - Round: " + newRound);
        }
        else if (values[0][i] == "Assigned recruiter") {
          //do nothing
        }
        // add pop-up to ask first targetted study date for this rolling study round
        else if (values[0][i] == "What is the targeted first date of the study?") {
          var response = ui.prompt('Please indicate the first targeted study date for this study. Enter the correct value in MM/DD/YYYY format.', ui.ButtonSet.OK_CANCEL);
          requestArray.push(new Date(response.getResponseText()));
        }
        else if (values[0][i] == "Study ID") {
          requestArray[0] = values[ActiveRow - 1][i];
        }
        else {
          requestArray.push(values[ActiveRow - 1][i]);
        }
      }
      else {
        if ((values[0][i] != "Indicate Status") && (values[0][i] != "Notes") && (values[0][i] != "Recruiter")) {
          requestArray.push(values[ActiveRow - 1][i]);
        }
      }
    }




    if (supportType != "Self-recruitment support - only need an invitation list shared OR access granted to a resource") {
      if ((studyType == "regular") || (studyType == "postponed") && (supportType != "Self-recruitment support - only need an invitation list shared OR access granted to a resource")) {
        //requestArray[3] = values[ActiveRow-1][pptColumn];
      }
      else if (studyType == "rolling") {
        //requestArray[3] = values[ActiveRow-1][pptColumn];
      }
      if (requestArray[3] == "canceled") {
        return;
      }



      //  var requestForm = FormApp.openByUrl(SpreadsheetApp.getActive().getFormUrl());
      /** Multivitiam Dev link  */
      var devUrl = "https://docs.google.com/forms/d/1mGdF2PTZLgF_q57MpZNdsGwnYl7WrvkUjcAVVhidu6k/edit";
      var requestForm = FormApp.openByUrl(devUrl);
      // var requestForm = FormApp.openByUrl("https://docs.google.com/forms/d/1KvwLeU5UW2ep731m2RidjA-nTddXWAy6iUnjUth0Ql8/edit");

      var questionArray = requestForm.getItems();
      var otherFound = 0;
      for (var q = 0; q < questionArray.length; q++) {
        if (questionArray[q].getTitle() == "In which city/region/country will study sessions take place?") {
          var locationOptionsArray = questionArray[q].asCheckboxItem().getChoices();
        }
      }


      var response = ui.prompt('How difficult is this study to recruit for based on the locations requested? Please go to go/locscriteria for more info on the 0-3 scale.', ui.ButtonSet.OK_CANCEL);
      if (response.getSelectedButton() != "OK") {
        return;
      }
      else {
        requestArray[4] = response.getResponseText();
      }


      // Display a dialog box

      if (isNaN(requestArray[4])) {
        var response = ui.prompt('How difficult is this study to recruit for based on the locations requested = ' + requestArray[4], 'This is not an integer. Please enter a number (0-3), and then click Ok (you can always edit manually later)', ui.ButtonSet.OK_CANCEL);
      }


      var response = ui.prompt('How difficult is it to recruit for the requested ppts? Please go to go/pptscriteria for more info on the 0-5 scale.', ui.ButtonSet.OK_CANCEL);
      if (response.getSelectedButton() != "OK") {
        return;
      } else {
        requestArray[5] = response.getResponseText();
      }


      // Display a dialog box
      if (isNaN(requestArray[5])) {
        var response = ui.prompt('How difficult is it to recruit for the requested ppts = ' + requestArray[5], 'This is not an integer. Please enter a number (0-5), and then click Ok (you can always edit manually later)', ui.ButtonSet.OK_CANCEL);
      }

      if (requestArray[16] == "") {
        var response = ui.prompt('Please indicate the targeted study start date', ui.ButtonSet.OK_CANCEL);
        requestArray[16] = new Date(response.getResponseText());
      }

      var response = ui.alert('Will phone screens be required?', ui.ButtonSet.YES_NO);
      requestArray[6] = response;

      var response = ui.alert('Does your study require you to get approval(s) from Legal/E&C/PWG?', ui.ButtonSet.YES_NO);
      requestArray[7] = response;
    }

    activeSheet.appendRow(populateOrangeColumnByRow(requestArray));
    var researcherID = 0;

    var tempResearcher = values[ActiveRow - 1][ldapColumn].substring(0, values[ActiveRow - 1][ldapColumn].indexOf("@google.com"));

    var myUrl = values[ActiveRow - 1][URLColumn];
    if (studyType == "rolling") {
      myUrl = rollingURL;
    }
    if (studyType == "rolling") {

      var myStudyName = values[ActiveRow - 1][nameColumn] + " - Round: " + newRound;
    }
    else { var myStudyName = values[ActiveRow - 1][nameColumn]; }

    try {
      researcherID = getGooglerInfo(tempResearcher);
    }
    catch (e) {
    }

    calculateComplexity();
    activeSheet.sort(2);

    emailStudyClaimed(researcherID, myUrl, myStudyName);


    if (studyType != "rolling") {
      //JGOLENBOCK UPDATE 3/22
      //sheet.deleteRow(ActiveRow);
      sheet.getRange(ActiveRow, 1, 1, sheet.getLastColumn()).clear();
      sheet.getRange(ActiveRow, 1, 1, sheet.getLastColumn()).clearNote();
    }
  }
  // Release the lock so that other processes can continue.

  lock.releaseLock();
}



function cancelStudy() {

  // Get a public lock on this script, because we're about to modify a shared resource.
  var lock = LockService.getScriptLock();

  // Wait for up to 90 seconds for other processes to finish.
  lock.waitLock(90000);

  var studyType = "";
  var sheet1 = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = sheet1.getActiveSheet();

  if (sheet.getName() == "New unallocated studies") {
    studyType = "new";
  }
  else if (sheet.getName() == "Active Studies") {
    studyType = "active";
  }
  else {
    return;
  }


  var cancelSheet = sheet1.getSheetByName("Canceled / Unsupported / Other");

  var rows = sheet.getDataRange();
  var numRows = rows.getNumRows();
  var values = rows.getValues();
  var lastRow = sheet.getLastRow();
  var lastColumn = sheet.getLastColumn();
  var ActiveRow = sheet1.getActiveRange().getRow();
  var ActiveColumn = sheet1.getActiveRange().getColumn();
  var ActiveCell = sheet1.getActiveCell();
  var numActiveRows = sheet1.getActiveRange().getNumRows();
  var numActiveCols = sheet1.getActiveRange().getNumColumns();
  var rollingURL = "";
  var me = Session.getActiveUser().getEmail();
  me = me.substring(0, me.indexOf("@google.com"));


  for (var i = 0; i < lastColumn; i++) {
    if (values[0][i] == "Username") {
      var ldapColumn = i;
    }
    else if (values[0][i] == "Email Address") {
      var ldapColumn = i;
    }
    else if (values[0][i] == "What's the name of the study?") {
      var nameColumn = i;
    }
    else if (values[0][i] == "Timestamp") {
      var timestampColumn = i;
    }
    else if (values[0][i] == "Study ID") {
      var studyIDColumn = i;
    }
  }

  var studyID = values[ActiveRow - 1][studyIDColumn];
  var requestArray = [studyID, "", "", me];

  // Display a dialog box
  var ui = SpreadsheetApp.getUi();
  var response = ui.alert('Click Yes to confirm you are canceling ' + values[ActiveRow - 1][ldapColumn] + '\'s study: ' + values[ActiveRow - 1][nameColumn], ui.ButtonSet.YES_NO)

  if (response == "YES") {

    response = ui.prompt('What is the status of this study? Postponed, Canceled, Unsupported, Self-Recruit, Self-Outsource, Self-Recruit Voluntary, Self-Outsource Voluntary', ui.ButtonSet.OK_CANCEL);
    if (response.getSelectedButton() != "OK") {
      return;
    }
    else {
      requestArray[1] = response.getResponseText();
    }
    response = ui.prompt('Any notes to add?', ui.ButtonSet.OK_CANCEL);
    if (response.getSelectedButton() != "OK") {
      return;
    }
    else {
      requestArray[2] = response.getResponseText();
    }
    for (var i = timestampColumn; i < lastColumn; i++) {
      // values has 0-base index, row has 1-base index ActiveRow-1 => rowIdx 
      // looping through cols in row to copy values aka requestArray =[...requestArray, ...values[ActiveRow-1].slice(timestampColumn)]
      requestArray.push(values[ActiveRow - 1][i]);
    }

    requestArray = addCancellationTimestampToRow(cancelSheet, requestArray)
    cancelSheet.appendRow(requestArray);

    //JGOLENBOCK UPDATE 3/22
    //sheet.deleteRow(ActiveRow);
    sheet.getRange(ActiveRow, 1, 1, sheet.getLastColumn()).clear();
    sheet.getRange(ActiveRow, 1, 1, sheet.getLastColumn()).clearNote();



  }
  // Release the lock so that other processes can continue.

  lock.releaseLock();
}

function moveToRolling() {

  // Get a public lock on this script, because we're about to modify a shared resource.
  var lock = LockService.getScriptLock();

  // Wait for up to 90 seconds for other processes to finish.
  lock.waitLock(90000);


  var studyType = "";
  var sheet1 = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = sheet1.getActiveSheet();

  if (sheet.getName() == "New unallocated studies") {
    studyType = "new";
  }
  else if (sheet.getName() == "Active Studies") {
    studyType = "active";
  }
  else {
    return;
  }


  var rollingSheet = sheet1.getSheetByName("Rolling Studies");

  var rows = sheet.getDataRange();
  var numRows = rows.getNumRows();
  var values = rows.getValues();
  var lastRow = sheet.getLastRow();
  var lastColumn = sheet.getLastColumn();
  var ActiveRow = sheet1.getActiveRange().getRow();
  var ActiveColumn = sheet1.getActiveRange().getColumn();
  var ActiveCell = sheet1.getActiveCell();
  var numActiveRows = sheet1.getActiveRange().getNumRows();
  var numActiveCols = sheet1.getActiveRange().getNumColumns();
  var rollingURL = "";
  var me = Session.getActiveUser().getEmail();
  me = me.substring(0, me.indexOf("@google.com"));


  for (var i = 0; i < lastColumn; i++) {
    if (values[0][i] == "Username") {
      var ldapColumn = i;
    }
    else if (values[0][i] == "Email Address") {
      var ldapColumn = i;
    }
    else if (values[0][i] == "What's the name of the study?") {
      var nameColumn = i;
    }
    else if (values[0][i] == "Timestamp") {
      var timestampColumn = i;
    }
    else if (values[0][i] == "Study ID") {
      var studyIDColumn = i;
    }
  }

  var studyID = "";//values[ActiveRow-1][studyIDColumn];
  var requestArray = [studyID, me, 1];

  // Display a dialog box
  var ui = SpreadsheetApp.getUi();
  var response = ui.alert('Click Yes to confirm you are moving ' + values[ActiveRow - 1][ldapColumn] + '\'s study: ' + values[ActiveRow - 1][nameColumn] + " to the Rolling Studies Tab", ui.ButtonSet.YES_NO)

  if (response == "YES") {

    for (var i = ldapColumn; i < lastColumn; i++) {
      requestArray.push(values[ActiveRow - 1][i]);

    }
    rollingSheet.appendRow(requestArray);

    //JGOLENBOCK UPDATE 3/22
    //sheet.deleteRow(ActiveRow);
    sheet.getRange(ActiveRow, 1, 1, sheet.getLastColumn()).clear();
    sheet.getRange(ActiveRow, 1, 1, sheet.getLastColumn()).clearNote();



  }
  // Release the lock so that other processes can continue.

  lock.releaseLock();
}

function moveToMonitor() {

  // Get a public lock on this script, because we're about to modify a shared resource.
  var lock = LockService.getScriptLock();

  // Wait for up to 90 seconds for other processes to finish.
  lock.waitLock(90000);

  var studyType = "";
  var sheet1 = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = sheet1.getActiveSheet();

  if (sheet.getName() != "Active Studies") {
    return;
  }


  var activeSheet = sheet1.getSheetByName("Active Studies");
  var rows = sheet.getDataRange();
  var numRows = rows.getNumRows();
  var values = rows.getValues();
  var lastRow = sheet.getLastRow();
  var lastColumn = sheet.getLastColumn();
  var ActiveRow = sheet1.getActiveRange().getRow();
  var ActiveColumn = sheet1.getActiveRange().getColumn();
  var ActiveCell = sheet1.getActiveCell();
  var numActiveRows = sheet1.getActiveRange().getNumRows();
  var numActiveCols = sheet1.getActiveRange().getNumColumns();

  for (var i = 1; i < lastColumn; i++) {
    if (values[0][i] == "Username") {
      var ldapColumn = i;
    }
    else if (values[0][i] == "Email Address") {
      var ldapColumn = i;
    }
    else if (values[0][i] == "What's the name of the study?") {
      var nameColumn = i;
    }
    else if (values[0][i] == "Paste the link to your Study Spreadsheet here. Ensure that your Screener Form has been linked to it and that the sharing settings for both are: private, with uxr-operations-share@google.com added as an editor/collaborator.") {
      var URLColumn = i;
    }
    else if (values[0][i] == "timestamp for moved to monitoring") {
      var monitoringColumn = i;
    }
  }


  // Display a dialog box
  var ui = SpreadsheetApp.getUi();
  var response = ui.alert('Click Yes to confirm you are now monitoring ' + values[ActiveRow - 1][ldapColumn] + '\'s study: ' + values[ActiveRow - 1][nameColumn], ui.ButtonSet.YES_NO)

  if (response == "YES") {
    var response = ui.prompt('Would you like to use today\'s date?  Click Ok if this is correct, or enter the correct value in MM/DD/YYYY format and then click Ok (you can always edit manually later)', ui.ButtonSet.OK_CANCEL);

    if (response.getSelectedButton() != "OK") {
      return;
    }
    else if (response.getResponseText() == "") {
      sheet.getRange(ActiveRow, monitoringColumn + 1, 1, 1).setValue((Utilities.formatDate(new Date(), "GMT", "MM/dd/yyyy' 'HH:mm")).toString());
    }
    else {
      sheet.getRange(ActiveRow, monitoringColumn + 1, 1, 1).setValue(response.getResponseText());
    }
  }
  // Release the lock so that other processes can continue.

  lock.releaseLock();
}




function closeStudy(callback) {
  // callback only used for "Close this TRP study" which has been removed as of 2021.04.06
  // Get a public lock on this script, because we're about to modify a shared resource.
  var lock = LockService.getScriptLock();

  // Wait for up to 90 seconds for other processes to finish.
  //lock.waitLock(90000);

  // brendonc - 27 aug 2019
  try {
    lock.waitLock(300000); // wait 300 seconds for others' use of the code section and lock to stop and then proceed
  } catch (e) {
    Logger.log('Could not obtain lock after 300 seconds.');
    return HtmlService.createHtmlOutput("<b> Server Busy please try after some time <p>")
    // In case this a server side code called asynchronously you return a error code and display the appropriate message on the client side
    return "Error: Server busy try again later... Sorry :("
  }


  var sheet1 = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = sheet1.getActiveSheet();


  if (sheet.getName() != "Active Studies") {
    return;
  }

  var closedSheetName = ("Closed Studies " + (String((new Date()).getFullYear())))
  var closedSheet = sheet1.getSheetByName(closedSheetName);
  // 12.21.20 reference sheet year name dynamically
  // var closedSheet = sheet1.getSheetByName("Closed Studies 2020");
  var rows = sheet.getDataRange();
  var numRows = rows.getNumRows();
  var values = rows.getValues();
  var lastRow = sheet.getLastRow();
  var lastColumn = sheet.getLastColumn();
  var ActiveRow = sheet1.getActiveRange().getRow();
  var ActiveColumn = sheet1.getActiveRange().getColumn();
  var ActiveCell = sheet1.getActiveCell();
  var numActiveRows = sheet1.getActiveRange().getNumRows();
  var numActiveCols = sheet1.getActiveRange().getNumColumns();

  for (var i = 1; i < lastColumn; i++) {
    if (values[0][i] == "Email Address") {
      var ldapColumn = i;
    }
    else if (values[0][i] == "What's the name of the study?") {
      var nameColumn = i;
    }
    else if (values[0][i] == "Paste the link to your Study Spreadsheet here. Ensure that your Screener Form has been linked to it and that the sharing settings for both are: private, with uxr-operations-share@google.com added as an editor/collaborator.") {
      var URLColumn = i;
    }
  }

  // Display a dialog box
  var ui = SpreadsheetApp.getUi();
  var response = ui.alert('Click Yes to confirm you are closing ' + values[ActiveRow - 1][ldapColumn] + '\'s study: ' + values[ActiveRow - 1][nameColumn], ui.ButtonSet.YES_NO)

  if (response == "YES") {

    var today = "";
    var quarter = "";

    var ui = SpreadsheetApp.getUi();
    var response = ui.prompt('Would you like to use today\'s date?  Click Ok if this is correct, or enter the correct value in MM/DD/YYYY format and then click Ok (you can always edit manually later)', ui.ButtonSet.OK_CANCEL);
    if (response.getSelectedButton() != "OK") {
      return;
    }
    else if (response.getResponseText() == "") {
      //today = Utilities.formatDate(new Date(), "GMT", "yyyy-MM-dd' 'HH:mm:ss' GMT'");
      today = new Date();
    }
    else {
      today = new Date(response.getResponseText());
    }


    var labelName = ui.prompt("*Study Closure Notice*", "You have three days to forward all materials relevant to this study to ux-qualityreview@google.com. To forward all materials now, please enter the email label below. Please note label must be precise, is case sensitive, and should include the full nested name.", ui.ButtonSet.OK).getResponseText();
    if (response.getSelectedButton() != "OK") {
      return;
    }
    if (labelName != "") {
      fwdByLabel(labelName);
    }

    if ((today.getMonth() >= 0) && (today.getMonth() <= 2)) {
      quarter = today.getFullYear() + "Q1";
    }
    else if ((today.getMonth() >= 3) && (today.getMonth() <= 5)) {
      quarter = today.getFullYear() + "Q2";
    }
    else if ((today.getMonth() >= 6) && (today.getMonth() <= 8)) {
      quarter = today.getFullYear() + "Q3";
    }
    else if ((today.getMonth() >= 9) && (today.getMonth() <= 11)) {
      quarter = today.getFullYear() + "Q4";
    }

    var studyArray = [quarter, Utilities.formatDate(today, "GMT", "MM/dd/yyyy' 'HH:mm").toString()];
    // Logger.log("studyArray " + studyArray);

    for (var i = 0; i < values[ActiveRow - 1].length; i++) {
      studyArray.push(values[ActiveRow - 1][i]);

    }
    // @douglascox below added to capture data on row to pass to callback for send trp email
    var currentRow = callback !== undefined ? sheet.getRange(ActiveRow, 1, 1, sheet.getLastColumn()).getValues()[0] : null

    closedSheet.appendRow(studyArray);
    //JGOLENBOCK UPDATE 3/22
    //sheet.deleteRow(ActiveRow);
    sheet.getRange(ActiveRow, 1, 1, sheet.getLastColumn()).clear();
    sheet.getRange(ActiveRow, 1, 1, sheet.getLastColumn()).clearNote();

    var equals = false;
    var myRow;
    var newValues = closedSheet.getDataRange().getValues();

    for (var r = newValues.length - 1; r >= 0; r--) {
      for (var c = 2; c < studyArray.length; c++) {
        if (newValues[r][c].toString() != studyArray[c].toString()) {
          break;
        }
        else if (c == studyArray.length - 1) {
          equals = true;
          myRow = r;
        }
      }
      if (equals == true) {
        break;
      }
    }

    if (equals == true) {
      //sendFeedbackForm(myRow+1);
      // douglascox@ 6.25 ternary with callback used for closeTRP study
      callback !== undefined ? callback(myRow + 1, currentRow)
        : sendFeedbackEmailAndGetStudySheetLinkAndStudyNameIfNull(myRow + 1);

    }
  }

  // brendonc - 27 aug 2019
  SpreadsheetApp.flush(); // applies all pending spreadsheet changes

  // Release the lock so that other processes can continue.
  lock.releaseLock();

  return;
}


function recordChange() {
  // Get a public lock on this script, because we're about to modify a shared resource.
  var lock = LockService.getScriptLock();

  // Wait for up to 90 seconds for other processes to finish.
  lock.waitLock(90000);

  var sheet1 = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = sheet1.getActiveSheet();


  if (sheet.getName() != "Active Studies") {
    return;
  }

  var rows = sheet.getDataRange();
  var numRows = rows.getNumRows();
  var values = rows.getValues();
  var lastRow = sheet.getLastRow();
  var lastColumn = sheet.getLastColumn();
  var ActiveRow = sheet1.getActiveRange().getRow();
  var ActiveColumn = sheet1.getActiveRange().getColumn();
  var ActiveCell = sheet1.getActiveCell();
  var numActiveRows = sheet1.getActiveRange().getNumRows();
  var numActiveCols = sheet1.getActiveRange().getNumColumns();

  var ui = SpreadsheetApp.getUi();
  var changeColumn = values[0].indexOf("any changes made during recruiting?");

  var response = ui.prompt('Provide a short note about what changed about this study', ui.ButtonSet.OK_CANCEL);
  if (response.getSelectedButton() == "OK") {
    var updateText = response.getResponseText();
  }

  else {
    return;
  }
  var currentText = sheet.getRange(ActiveRow, changeColumn + 1, 1, 1).getValue();
  sheet.getRange(ActiveRow, changeColumn + 1, 1, 1).setValue(currentText + "\n\n" + Utilities.formatDate(new Date(), "GMT", "MM/dd/yyyy HH:mm") + " " + Session.getActiveUser().getEmail() + " " + updateText);
  // Release the lock so that other processes can continue.

  lock.releaseLock();
}


function emailToRecipient(subject, message, recipient, ccAddress) { //Generic email sending function
  var mailFromIndex = GmailApp.getAliases().indexOf('uxi-alerts@google.com');
  var mailFrom = GmailApp.getAliases()[mailFromIndex];

  // GmailApp.sendEmail(recipient, subject, message, { from: mailFrom, cc: ccAddress, replyTo: recipient, htmlBody: message });
  /** development */
  const sendEmailParams = {
    to: recipient,
    subject,
    options: { from: mailFrom, cc: ccAddress, replyTo: recipient, }
  }
  const prodOutboundParam = JSON.stringify(sendEmailParams)
  body = prodOutboundParam + body
  GmailApp.sendEmail(devEmails, subject, message, { from: mailFrom, cc: 'uxi-mv-uat@google.com', replyTo: recipient, htmlBody: message });

  Logger.log(message);
}

function sendtoFastResearch() {
  // Get a public lock on this script, because we're about to modify a shared resource.
  var lock = LockService.getScriptLock();

  // Wait for up to 90 seconds for other processes to finish.
  lock.waitLock(90000);
  /* spreadsheet update to one with working gcp
  var fastResearchSheet = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/1Hh8kNN6T5NELz10AFB1khiRFncGu0UnPmqw6S0tj1Zo/edit#gid=1561539214");
  */
  var fastResearchSheet = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/1lbWGTOB1WD9Ry2RxYby9nBNT18x-1oA7AOb344XTjDQ/edit#gid=632897839");

  //     var fastResearchTab = fastResearchSheet.getSheetByName("Form Responses 2");
  var fastResearchTab = getSingleSheetByNames(fastResearchSheet, ["Active", "Form Responses 2"])

  var fastResearchValues = fastResearchTab.getDataRange().getValues();

  var supportQueueSheet = SpreadsheetApp.getActiveSpreadsheet();
  var supportQueueTab = supportQueueSheet.getActiveSheet();
  var supportQueueValues = supportQueueTab.getDataRange().getValues();
  var activeRow = supportQueueSheet.getActiveRange().getRow();

  var supportQueueArray = supportQueueValues[activeRow - 1];
  var headerArray = supportQueueValues[0];

  var keySheet = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/1b1gf_VaeSEfxOK6q6GSthICcPglN5A78kVQUFIeSUQg/edit#gid=404773570");
  var keyTab = keySheet.getSheetByName("FR Queue Key");
  var keyValues = keyTab.getDataRange().getValues();
  var fastResearchArray = ["", "", "", "", ""];

  for (var k = 1; k < keyValues.length; k++) {
    if (keyValues[k][1] == "N/A") {
      fastResearchArray.push("N/A");
    }
    else {
      for (var s = 0; s < supportQueueArray.length; s++) {
        if (keyValues[k][1] == headerArray[s]) {
          fastResearchArray.push(supportQueueArray[s]);
        }
      }
    }
  }
  fastResearchTab.appendRow(fastResearchArray);




  var cancelSheet = supportQueueSheet.getSheetByName("Canceled / Unsupported / Other");

  var me = Session.getActiveUser().getEmail();
  me = me.substring(0, me.indexOf("@google.com"));
  var requestArray = [, , , me];
  var lastColumn = supportQueueSheet.getLastColumn();

  for (var i = 0; i < lastColumn; i++) {
    if (headerArray[i] == "Email Address") {
      var ldapColumn = i;
    }
    else if (headerArray[i] == "What's the name of the study?") {
      var nameColumn = i;
    }
    else if (headerArray[i] == "Timestamp") {
      var timestampColumn = i;

    }
    else if (headerArray[i] == "Study ID") {
      var studyidColumn = i;

    }
  }



  requestArray[0] = supportQueueArray[studyidColumn];
  // Logger.log(requestArray);
  // Logger.log(requestArray[0]);

  requestArray[1] = "Moved to Fast Research";

  for (var i = timestampColumn; i < lastColumn; i++) {
    requestArray.push(supportQueueArray[i]);
  }
  requestArray = addCancellationTimestampToRow(cancelSheet, requestArray)
  cancelSheet.appendRow(requestArray);




  //JGOLENBOCK UPDATE 3/22
  //supportQueueTab.deleteRow(activeRow);
  supportQueueTab.getRange(activeRow, 1, 1, supportQueueTab.getLastColumn()).clear();
  supportQueueTab.getRange(activeRow, 1, 1, supportQueueTab.getLastColumn()).clearNote();

  var email = "fast-research@google.com";
  var subject = "Study moved to FR queue from Research Ops queue";
  var body = "Hello Fast Research Team!  <br><br>" +
    "We have just automatically copied a request from the Research Ops queue to the Fast Research queue. <br><br>" +
    "We pulled as much information as we had available, but if you have any questions, please reach out directly to the requestor. <br><br>" +
    "Thank you! <br>" +
    "The Research Ops Team";

  var mailFromIndex = GmailApp.getAliases().indexOf("uxi-alerts@google.com");
  var mailFrom = GmailApp.getAliases()[mailFromIndex];
  /** PRODUCTION` */
  // GmailApp.sendEmail(email, subject, body, { htmlBody: body, from: mailFrom });
  /** DEVELOPMENT */
  const sendEmailParams = {
    to: email,
    subject,
    options: { htmlBody: body, from: mailFrom }
  }
  body = JSON.stringify(sendEmailParams) + body
  GmailApp.sendEmail(devEmails, subject, body, { htmlBody: body, from: mailFrom });

  // Release the lock so that other processes can continue.

  lock.releaseLock();
}



function sendFeedbackForm(closedStudy) {

  var sheet1 = SpreadsheetApp.getActiveSpreadsheet();
  var closedSheetName = ("Closed Studies " + (String((new Date()).getFullYear())))
  var closedSheet = sheet1.getSheetByName(closedSheetName);
  // 12.21.20 reference sheet year name dynamically
  // var closedSheet = sheet1.getSheetByName("Closed Studies 2020");
  var rows = closedSheet.getDataRange();
  var numRows = rows.getNumRows();
  var values = rows.getValues();
  var lastRow = closedSheet.getLastRow();
  var lastColumn = closedSheet.getLastColumn();
  var activeRow = closedStudy;//closedSheet.getLastRow();

  var ui = SpreadsheetApp.getUi();

  var studyNameColumn = values[0].indexOf("What's the name of the study?");
  var studySpreadsheetColumn = values[0].indexOf("Paste the link to your Study Spreadsheet here. Ensure that your Screener Form has been linked to it and that the sharing settings for both are: private, with uxr-operations-share@google.com added as an editor/collaborator.");
  var uxrEmailColumn = values[0].indexOf("Email Address");
  var studyIDColumn = values[0].indexOf("Study ID");


  var studyName = values[activeRow - 1][studyNameColumn];
  var studyID = values[activeRow - 1][studyIDColumn];

  if (studyName == "") {
    var response = ui.prompt('No study name found, please enter one here:', ui.ButtonSet.OK_CANCEL);
    if (response.getSelectedButton() == "CANCEL") {
      return;
    }
    else {
      studyName = response.getResponseText();
      closedSheet.getRange(activeRow, studyNameColumn + 1, 1, 1).setValue(studyName);
    }
  }

  var spreadsheet = values[activeRow - 1][studySpreadsheetColumn];
  if ((spreadsheet == "")) {
    var response = ui.prompt('No spreadsheet found, please enter a link here (enter n/a if no spreadsheet exists).:', ui.ButtonSet.OK_CANCEL);
    if (response.getSelectedButton() == "CANCEL") {
      return;
    }
    else {
      spreadsheet = response.getResponseText();
      closedSheet.getRange(activeRow, studySpreadsheetColumn + 1, 1, 1).setValue(spreadsheet);
    }
  }

  var recruiterEmail = Session.getActiveUser().getEmail();
  var recruiterLdap = recruiterEmail.substring(0, recruiterEmail.indexOf("@google.com"));
  try {
    var recruiterInfo = getGooglerInfo(recruiterLdap);
    var recruiterName = recruiterInfo.firstName;
  }
  catch (e) {
    var response = ui.prompt('Please enter your first name:', ui.ButtonSet.OK_CANCEL);
    var recruiterName = response.getResponseText();
  }

  var UXRemail = values[activeRow - 1][uxrEmailColumn];
  var UXRLdap = UXRemail.substring(0, UXRemail.indexOf("@google.com"));

  try {
    var UXRInfo = getGooglerInfo(UXRLdap);
    var UXRName = UXRInfo.firstName;
  }
  catch (e) {
    var response = ui.prompt('Please enter the UXR\'s first name:', ui.ButtonSet.OK_CANCEL);
    var UXRName = response.getResponseText();
  }

  var subject = "Recruiting & Labs feedback for " + studyName;
  var email = UXRemail + ",uxi-data@google.com, ux-qualityreview@google.com";
  var link = "";
  var surveyLink = "<a href=\"https://google.qualtrics.com/jfe/form/SV_eaPU8SAhhAMAR2R?RecruiterEmail=" + recruiterEmail + "&StudyID=" + studyID + "&StudyName=" + studyName + "\">survey</a> ";

  if (spreadsheet.indexOf("http") >= 0) {
    link = "<a href=" + spreadsheet + ">" + studyName + "</a>";
  }
  else {
    link = studyName;
  }

  var body = "Hi " + UXRName + ", <br><br> " +
    "Thank you for requesting study support from the Research Operations team for your study: " + link + ". Please complete this " + surveyLink + "and share feedback on the participant recruiting support you received.<br><br>" +
    "It is important to note that entries are visible to managers on the team and constructive feedback and filtered comments will be shared with me and our external suppliers as appropriate.<br><br>" +
    "Your comments are invaluable and will help our team continue to improve our participant recruiting support efforts going forward.<br><br>" +
    "Thanks,<br>" +
    recruiterName +
    "<br><br><small>Disclaimer: this email was sent from " + recruiterName + "'s account but is triggered automatically by a script</small>.";

  // var mailFromIndex = GmailApp.getAliases().indexOf("uxi-alerts@google.com");
  // var mailFrom = GmailApp.getAliases()[mailFromIndex];
  /** PRODUCTION` */
  // GmailApp.sendEmail(email, subject, body, { htmlBody: body, bcc: "nocella@google.com" });
  /** DEVELOPMENT */
  const sendEmailParams = {
    to: email,
    subject,
    options: { bcc: "nocella@google.com" }
  }
  const prodOutboundParam = JSON.stringify(sendEmailParams)
  body = prodOutboundParam + body
  GmailApp.sendEmail(devEmails, subject, body, { htmlBody: body, });


  var feedbackSheet1 = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/1rY7zHUZOb25pygw3I8YRUR5gg0LU9UiMJhNdeJ42zsI/edit#gid=0");
  var feedbackSheet = feedbackSheet1.getSheetByName("Feedback Log");

  feedbackSheet.appendRow([Utilities.formatDate(new Date(), "GMT", "MM/dd/yyyy' 'HH:mm"), recruiterEmail, UXRemail, studyName, spreadsheet]);

}

// brendonc - 25 sept 2019
// douglascox - May 2020 :: updated doc id with new template
function sendFeedbackEmailAndGetStudySheetLinkAndStudyNameIfNull(closedStudy) {

  var sheet1 = SpreadsheetApp.getActiveSpreadsheet();
  var closedSheetName = ("Closed Studies " + (String((new Date()).getFullYear())))
  var closedSheet = sheet1.getSheetByName(closedSheetName);
  // 12.21.20 reference sheet year name dynamically
  // var closedSheet = sheet1.getSheetByName("Closed Studies 2020");
  var rows = closedSheet.getDataRange();
  var numRows = rows.getNumRows();
  var values = rows.getValues();
  var lastRow = closedSheet.getLastRow();
  var lastColumn = closedSheet.getLastColumn();
  var activeRow = closedStudy;//closedSheet.getLastRow();
  //var activeRow = 15;

  var ui = SpreadsheetApp.getUi();

  var studyNameColumn = values[0].indexOf("What's the name of the study?");
  var studySpreadsheetColumn = values[0].indexOf("Paste the link to your Study Spreadsheet here. Ensure that your Screener Form has been linked to it and that the sharing settings for both are: private, with uxr-operations-share@google.com added as an editor/collaborator.");
  var uxrEmailColumn = values[0].indexOf("Email Address");
  var studyIDColumn = values[0].indexOf("Study ID");
  var rcldapsColumn = values[0].indexOf("Recruiter");

  var rcldapsArray = values[activeRow - 1][rcldapsColumn].split(",");
  var studyName = values[activeRow - 1][studyNameColumn];
  var studyID = values[activeRow - 1][studyIDColumn];
  var UXRemail = values[activeRow - 1][uxrEmailColumn];
  Logger.log("UXRemail ", UXRemail);
  var UXRLdap = UXRemail.substring(0, UXRemail.indexOf("@google.com"));

  if (studyName == "") {
    var response = ui.prompt('No study name found, please enter one here:', ui.ButtonSet.OK_CANCEL);
    if (response.getSelectedButton() == "CANCEL") {
      return;
    }
    else {
      studyName = response.getResponseText();
      closedSheet.getRange(activeRow, studyNameColumn + 1, 1, 1).setValue(studyName);
    }
  }

  var spreadsheet = values[activeRow - 1][studySpreadsheetColumn];
  if ((spreadsheet == "")) {
    var response = ui.prompt('No spreadsheet found, please enter a link here (enter n/a if no spreadsheet exists).:', ui.ButtonSet.OK_CANCEL);
    if (response.getSelectedButton() == "CANCEL") {
      return;
    }
    else {
      spreadsheet = response.getResponseText();
      closedSheet.getRange(activeRow, studySpreadsheetColumn + 1, 1, 1).setValue(spreadsheet);
    }
  }

  var rcldap = rcldapsArray[0];
  var recruiterEmail = rcldap + "@google.com";
  var sessionUserEmail = Session.getActiveUser().getEmail();
  var sessionUserLdap = sessionUserEmail.substring(0, sessionUserEmail.indexOf("@google.com"));
  var closingStudyRecruiterFullName = "";
  try {
    var recruiterInfo = getGooglerInfo(rcldap);
    var recruiterName = recruiterInfo.firstName;
    var recruiterLastName = recruiterInfo.lastName;
    closingStudyRecruiterFullName = recruiterName + " " + recruiterLastName;
  } catch (e) {
    // error
    try {
      // this should always work
      Logger.log("sessionUserLdap ", sessionUserLdap);
      var sessionUserInfo = getGooglerInfo(sessionUserLdap);
      recruiterEmail = sessionUserEmail;
      var sessionUserName = sessionUserInfo.firstName;
      var sessionUserLastName = sessionUserInfo.lastName;
      closingStudyRecruiterFullName = sessionUserName + " " + sessionUserLastName;
    } catch (e) {
      // error
    }
  }

  Logger.log("recruiterEmail ", recruiterEmail);

  try {
    var UXRInfo = getGooglerInfo(UXRLdap);
    var UXRName = UXRInfo.firstName;
  }
  catch (e) {
    // should get an email bounce back
    var UXRName = UXRLdap;
  }

  var subject = "Tell us about your study experience";
  var email = UXRemail;
  //var email = "brendonc@google.com";

  // brendonc - 10 sept 2019
  // new
  var studyName2 = studyName.replace(/\s/g, "%20");
  const surveyURL = "https://google.qualtrics.com/jfe/form/SV_ac229SThBw67xid?RecruiterEmail=" + recruiterEmail + "&StudyID=" + studyID + "&StudyName=" + studyName2;
  //const emailTemplate = DocumentApp.openById('1IHAPQLoBVE_Q3qO84XViO_H98XqtAVbaaRo49S3u7lU');
  // update doc 20200515
  const emailTemplate = DocumentApp.openById("1U8hHaMwY7nzX7wQlgTTH3bu6z4YPnhwD4JYU91awXjE")
  const emailTemplateText = emailTemplate.getText();
  //regex is used here as there's multiple occurences
  var emailBody = emailTemplateText.replace(/{{survey_url}}/g, surveyURL);
  emailBody = emailBody.replace("{{uxr_first_name}}", UXRName);
  emailBody = emailBody.replace("{{study_name}}", studyName);
  emailBody = emailBody.replace("{{rc_first_last_name}}", closingStudyRecruiterFullName);
  /** PRODUCTION` */
  // GmailApp.sendEmail(email, subject, emailBody, { htmlBody: emailBody, noReply: true, bcc: "ux-qualityreview@google.com" });
  /** DEVELOPMENT */
  const sendEmailParams = {
    to: email,
    subject,
    options: { noReply: true, bcc: "ux-qualityreview@google.com" }
  }
  const prodOutboundParam = JSON.stringify(sendEmailParams)
  emailbody = prodOutboundParam + emailBody
  GmailApp.sendEmail(devEmails, subject, emailBody, { htmlBody: emailBody, noReply: true, });

  //GmailApp.sendEmail(email, subject, emailBody, {htmlBody: emailBody, from:mailFrom, cc: "nocella@google.com, ux-qualityreview@google.com"});
  //GmailApp.sendEmail(email, subject, emailBody, {htmlBody: emailBody, cc: "nocella@google.com, ux-qualityreview@google.com", noReply: true});
  // bcc: "ux-qualityreview@google.com" - dont cc 

  // deprecated - 8 Oct 2019
  //var newFeedbackSheet1 = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/1rY7zHUZOb25pygw3I8YRUR5gg0LU9UiMJhNdeJ42zsI/edit#gid=0");
  //var newfeedbackSheet = newFeedbackSheet1.getSheetByName("Feedback Log");
  //newfeedbackSheet.appendRow([Utilities.formatDate(new Date(), "GMT", "MM/dd/yyyy' 'HH:mm"), recruiterEmail, UXRemail, studyName, studyID]);

}


function closingMetricsReminder() {
  // Get a public lock on this script, because we're about to modify a shared resource.
  var lock = LockService.getScriptLock();

  // Wait for up to 90 seconds for other processes to finish.
  lock.waitLock(90000);

  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  var closedSheetName = ("Closed Studies " + (String((new Date()).getFullYear())))
  var closedStudiesTab = spreadsheet.getSheetByName(closedSheetName);
  // 12.21.20 reference sheet year name dynamically
  // var closedStudiesTab = spreadsheet.getSheetByName("Closed Studies 2020");
  /** multi vitiam env.dev */
  var closedStudiesURL = `<a href=\"https://docs.google.com/spreadsheets/d/194r-7nE90vmLSmq87ylU4DETJNg9wbYeKKa6OS8mr0Y/edit?resourcekey=0-eS1f72XLIARMlZQ0EpxPUg#gid=758486906\">${closedSheetName} tab</a> `;
  // var closedStudiesURL = `<a href=\"https://docs.google.com/spreadsheets/d/1b1gf_VaeSEfxOK6q6GSthICcPglN5A78kVQUFIeSUQg/edit#gid=331234931\">${closedSheetName} tab</a> `;
  var closedStudiesArray = closedStudiesTab.getDataRange().getValues();
  var pptsrecruitedColumn = closedStudiesArray[0].indexOf("total # participants recruited");
  var pptsrunColumn = closedStudiesArray[0].indexOf("total # participants run");
  var recruiterColumn = closedStudiesArray[0].indexOf("Recruiter");
  var studyNameColumn = closedStudiesArray[0].indexOf("What's the name of the study?");
  var spreadsheetLink = "";
  var subject = "Missing Closing Metrics";



  for (var i = 1; i < closedStudiesArray.length; i++) {
    if ((closedStudiesArray[i][pptsrecruitedColumn].toString() == "") && (closedStudiesArray[i][pptsrunColumn].toString() == "")) {
      var recruiters = closedStudiesArray[i][recruiterColumn].split(",");
      var recruiterEmail = recruiters[0] + "@google.com";
      try {
        var recruiterName = getGooglerInfo(recruiters[0]).firstName;
        Logger.log(recruiterName);
        var body = //"Note: This email was sent from Janice’s account but is triggered automatically by a script. No need to reply to Janice. <br><br>" +
          "Hi " + recruiterName + ", <br><br> " +
          "Please complete the closing metrics for " + closedStudiesArray[i][studyNameColumn] + " on row " + (i + 1) + " on the " + closedStudiesURL + ". <br><br> " +
          "You will continue receiving these emails daily until you complete the metrics for this study. <br><br>";// +
        // "<small>Note: This email was sent from Janice’s account but is triggered automatically by a script.<br>Janice does not need to be included in future correspondence on this thread</small>. <br><br>";
        var mailFromIndex = GmailApp.getAliases().indexOf("uxi-alerts@google.com");
        var mailFrom = GmailApp.getAliases()[mailFromIndex];

        /** PRODUCTION` */
        // GmailApp.sendEmail(recruiterEmail, subject, body, { htmlBody: body, from: mailFrom });
        /** DEVELOPMENT */
        const sendEmailParams = {
          to: recruiterEmail,
          subject,
          options: { from: mailFrom }
        }
        const prodOutboundParam = JSON.stringify(sendEmailParams)
        body = prodOutboundParam + body
        GmailApp.sendEmail(devEmails, subject, body, { htmlBody: body, from: mailFrom });
      }
      catch (e) { }
    }
  }
  // Release the lock so that other processes can continue.

  lock.releaseLock();
}


function studyIDReminder() {
  // Get a public lock on this script, because we're about to modify a shared resource.
  var lock = LockService.getScriptLock();

  // Wait for up to 90 seconds for other processes to finish.
  lock.waitLock(90000);

  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  var activeStudiesTab = spreadsheet.getSheetByName("Active Studies");
  /** Multivitiam env.dev refernce */
  var activeStudiesURL = "<a href=\"https://docs.google.com/spreadsheets/d/194r-7nE90vmLSmq87ylU4DETJNg9wbYeKKa6OS8mr0Y/edit?resourcekey=0-eS1f72XLIARMlZQ0EpxPUg#gid=1865316229\">Active Studies tab</a> ";
  // var activeStudiesURL = "<a href=\"https://docs.google.com/spreadsheets/d/1b1gf_VaeSEfxOK6q6GSthICcPglN5A78kVQUFIeSUQg/edit#gid=1865316229\">Active Studies tab</a> ";
  var activeStudiesArray = activeStudiesTab.getDataRange().getValues();
  var studyIDColumn = activeStudiesArray[0].indexOf("Study ID");
  var recruiterColumn = activeStudiesArray[0].indexOf("RC");
  var studyNameColumn = activeStudiesArray[0].indexOf("What's the name of the study?");
  var spreadsheetLink = "";
  var subject = "Please input Study ID in Support Queue";



  for (var i = 1; i < activeStudiesArray.length; i++) {
    if (activeStudiesArray[i][studyIDColumn].toString() == "") {
      var recruiters = activeStudiesArray[i][recruiterColumn].split(",");
      var recruiterEmail = recruiters[0] + "@google.com";
      try {
        var recruiterName = getGooglerInfo(recruiters[0]).firstName;
        Logger.log(recruiterName);
        var body = //"Note: This email was sent from Janice’s account but is triggered automatically by a script. No need to reply to Janice. <br><br>" +
          "Hi " + recruiterName + ", <br><br> " +
          "Please input the study ID for " + activeStudiesArray[i][studyNameColumn] + " on row " + (i + 1) + " on the " + activeStudiesURL + ". If your study does not have a study ID, please input \"N/A\" in the column. <br><br> " +
          "You will continue receiving these emails daily until you complete the study ID for this study. <br><br>";// +
        //"<small>Note: This email was sent from Janice’s account but is triggered automatically by a script.<br>Janice does not need to be included in future correspondence on this thread</small>. <br><br>";

        var mailFromIndex = GmailApp.getAliases().indexOf("uxi-alerts@google.com");
        var mailFrom = GmailApp.getAliases()[mailFromIndex];

        /** PRODUCTION` */
        // GmailApp.sendEmail(email, subject, emailBody, { htmlBody: emailBody, noReply: true, bcc: "ux-qualityreview@google.com" });
        /** DEVELOPMENT */
        const sendEmailParams = {
          to: email,
          subject,
          options: { noReply: true, bcc: "ux-qualityreview@google.com" }
        }
        const prodOutboundParam = JSON.stringify(sendEmailParams)
        body = prodOutboundParam + body
        GmailApp.sendEmail(devEmails, subject, emailBody, { htmlBody: emailBody, noReply: true, });
      }
      catch (e) { }
    }
  }
  // Release the lock so that other processes can continue.

  lock.releaseLock();
}

function fwdByLabel(labelName) {
  var ui = SpreadsheetApp.getUi();
  //var labelString = ui.prompt("What is the name of the label?").getResponseText();
  var labelString = labelName;
  var qualityEmail = "ux-qualityreview@google.com"; //"aus-uxr-ops-qr@google.com";
  var label = GmailApp.getUserLabelByName(labelString);
  try {
    var messages = label.getThreads();
  }
  catch (e) {
    ui.alert("Oh no! Cannot find that label name.");
    return;
  }
  var emailArray = ["The following emails from label, \"" + label.getName() + "\", were forwarded successfully to " + qualityEmail + "<ul>"];
  var thread = 0;

  for (var m = 0; m < messages.length; m++) {
    var messages2 = messages[m].getMessages();
    thread = m + 1;
    emailArray.push(["<li>THREAD #" + thread + "<ul>"]);
    for (var n = 0; n < messages2.length; n++) {
      try {
        messages2[n].forward(qualityEmail);
        emailArray.push(["<li>Subject: " + messages[m].getFirstMessageSubject(), "From: " + messages2[n].getFrom(), "To: " + messages2[n].getTo()]);
      }
      catch (e) {

      }
    }
    emailArray.push(["</ul>"]);
  }
  Logger.log(emailArray);
  var emailBody = "";
  for (var e = 0; e < emailArray.length; e++) {
    emailBody = emailBody + emailArray[e] + "<br>";
  }
  /** production */
  // GmailApp.sendEmail(qualityEmail + "," + Session.getActiveUser().getEmail(), "Forward email script summary", emailBody, { htmlBody: emailBody });
  /** development */
  const sendEmailParams = {
    to: qualityEmail + "," + Session.getActiveUser().getEmail(),
    subject: "Forward email script summary",
    options: {}
  }
  const prodOutboundParam = JSON.stringify(sendEmailParams)
  body = prodOutboundParam + body
  GmailApp.sendEmail(devEmails, "Forward email script summary", emailBody, { htmlBody: emailBody });

  ui.alert("Forwarding is complete for label, \"" + label.getName() + "\". " + messages.length + " threads were forwarded to " + qualityEmail + ". You will receive an email with a summary of all emails that were sent, and you can check your sent folder to verify as well.");

}

/* OWNER: douglascox, uxi-automation
 * SOURCES: go/studyqueue
 * DATE: refactored 2020.10.05
 * DESCRIPTION: Cycles through sheets 2parts
 * specifics sheets by Named => removesSheetProtection => FrozenRow(1)
 * all Sheets => setFirstRowProtection
 * @param  undefined
 * @return undefined 
 */
function removeSheetProtections() {
  const ss = SpreadsheetApp.getActiveSpreadsheet()
  const removeSheetProtection = removeProtection(SpreadsheetApp.ProtectionType.SHEET)
  const removeRangeProtection = removeProtection(SpreadsheetApp.ProtectionType.RANGE)
  const logStatus = (msg) => (sheet) => Logger.log(`${msg} @"${sheet.getName()}"`)

  const selectSheets = ["New unallocated studies", "Active Studies"].map((str) => ss.getSheetByName(str))
  const sheets = getAllSheets(ss);

  const setProtectionFirstRow = compose(logStatus("First Row Protected"), setFirstRowProtectionBySheet); //frozenRowBySheet, removeRangeProtection, removeSheetProtection) // add Range?
  const removeSheetProtectAndFreeze = compose(logStatus("Sheet Protection Removed & Froze First Row"), frozenRowBySheet, removeSheetProtection);

  selectSheets.forEach(removeSheetProtectAndFreeze)
  sheets.forEach(setProtectionFirstRow)

  function frozenRowBySheet(sheet) { sheet.setFrozenRows(1); return sheet }

  function removeProtection(protectionType) {
    return function (sheet) {
      const protections = sheet.getProtections(protectionType);
      const removeProtections = (_protection) => (_protection && _protection.canEdit()) && _protection.remove();
      (Array.isArray(protections)) && protections.forEach(removeProtections)
      return sheet
    }
  }

  function setFirstRowProtectionBySheet(sheet) {
    let range = sheet.getRange(1, 1, 1, sheet.getLastColumn())
    const isFirstRowProtected = sheet.getProtections(SpreadsheetApp.ProtectionType.RANGE).some(protection => protection.getRange().getA1Notation() === range.getA1Notation())
    !isFirstRowProtected && range.protect().setDescription('script set protection on first Row')
    return sheet;
  }

  function getAllSheets(ss) {
    return ss.getSheets();
  }
}

function onOpen() {
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  var entries = [
    {
      name: "Forward by label",
      functionName: "fwdByLabel"
    }
  ];
  spreadsheet.addMenu("Scripts", entries);

};
function nightlySort() {
  const ss = SpreadsheetApp.getActiveSpreadsheet()
  const sortHeader = "Timestamp"
  const _nightlySort = compose(addNumRows, sortByTimeStamp(sortHeader), removeBlanks, getSheet)
  const sheets = ["New unallocated studies", "Active Studies"]
  const tryCatchNightlySort = tryCatchFn(_nightlySort)

  // locks scripts from running while processing
  // process Sheet(s) -> removeBlanks -> sort -> add ten rows
  const lock = LockService.getScriptLock();
  const errors = sheets.reduce((acc, next) => tryCatchNightlySort(acc, next), { errors: "" })
  lock.releaseLock()
  // if any errors throw afters _nightlysort runs on sheet(s)
  hasErrors(errors)

  function hasErrors({ errors }) {
    debugger
    if (errors !== "") {
      throw (errors)
    }
  }

  function tryCatchFn(fn) {
    return function (acc, args) {
      try {
        fn(args)
        return acc
      } catch (e) {
        acc.errors += `Sheet:${args}; ${e}\n`
        return acc
      }
    }
  }

  function getSheet(str) {
    return ss.getSheetByName(str);
  }
  function removeBlanks(sheet) {
    const dataRange = sheet.getDataRange();
    const values = dataRange.getValues();
    const rowsToRemove = values.reduce((acc, next, idx) => next.join("") === "" ? acc.concat(idx + 1) : acc, []);
    rowsToRemove.forEach((rowIdx, idx) => sheet.deleteRow(rowIdx - idx));

    return sheet
  }
  function sortByTimeStamp(str = "Timestamp") {
    return sortyByColHeader(str)
  }

  function sortyByColHeader(headerName) {
    return function (sheet) {
      const { headerObj, body } = getDataBySheet(sheet);
      const headerIdx = headerObj[headerName] + 1; //convert to 1-based indexing

      return {
        sheet: sheet.sort(headerIdx, true),
      }
      function onlyBlanks(props) {
        return Object.values(props).join("") === ""
      }
    }
  }
  function addNumRows({ sheet }) {
    const glr = sheet.getLastRow()
    sheet.insertRowsAfter(sheet.getLastRow(), 10);
  }
}
// deprecated this version inplace of above as of 11.24
function _nightlySort() {
  var sheet = SpreadsheetApp.getActive();
  var newtab = sheet.getSheetByName("New unallocated studies");
  var values = newtab.getDataRange().getValues();

  newtab.sort(1);
  newtab.insertRows(newtab.getLastRow() + 1, 10);
  try {
    newtab.deleteRows(values.length + 2, newtab.getMaxRows() - values.length - 1);
  }
  catch (e) { }

  var activeTab = sheet.getSheetByName("Active Studies");
  var activeValues = activeTab.getDataRange().getValues();

  activeTab.sort(2);
  activeTab.insertRows(activeTab.getLastRow() + 1, 10);
  try {
    activeTab.deleteRows(activeValues.length + 2, activeTab.getMaxRows() - activeValues.length - 1);
  }
  catch (e) { }

}

function onOpen() {

  // Get a public lock on this script, because we're about to modify a shared resource.
  var lock = LockService.getScriptLock();

  // brendonc - 27 aug 2019
  try {
    lock.waitLock(180000); // wait 180 seconds for others' use of the code section and lock to stop and then proceed
  } catch (e) {
    Logger.log('Could not obtain lock after 180 seconds.');
    return HtmlService.createHtmlOutput("<b> Server Busy please try after some time <p>")
    // In case this a server side code called asynchronously you return a error code and display the appropriate message on the client side
    return "Error: Server busy try again later... Sorry :("
  }

  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  var entries = [
    {
      name: "Claim this study",
      functionName: "claimStudy"
    },
    {
      name: "Mark this study as monitoring",
      functionName: "moveToMonitor"
    },
    {
      name: "Close this study",
      functionName: "closeStudy"
    },
    // removed 2021.04.06
    // {
    //   name: "Close this TRP study",
    //   functionName: "closeTRPStudy"
    // },
    {
      name: "CANCEL this study",
      functionName: "cancelStudy"
    },
    {
      name: "Record a change for this study",
      functionName: "recordChange"
    },
    {
      name: "Force allocate this study",
      functionName: "forceAllocate"
    },
    {
      name: "Recalculate Complexity Score",
      functionName: "calculateComplexity"
    },
    {
      name: "Send study to Fast Research",
      functionName: "triageStudyToFastResearch"
    },
    {
      name: "Move to Rolling Studies",
      functionName: "moveToRolling"
    },
    { name: "Move To Transfer Queue", functionName: "moveToTransitQueue" }
    ,/*,
    {
      name : "Send to triage for review",
      functionName : "recordTriage"
    }
    {
      name : "test - IGNORE",
      functionName : "triageStudyToFastResearch"
    }
    */
  ];
  spreadsheet.addMenu("Scripts", entries);

  // brendonc - 27 aug 2019
  // Release the lock so that other processes can continue.
  lock.releaseLock();
  return;
};
/* adds timestamp facilitated by cancellation to existing row
 * @param (SheetObject, Array(row))
 * @return Array
 */
function addCancellationTimestampToRow(cancelSheet, row) {
  var cancellationTimestampIdx = getHeaderObjBySheet(cancelSheet)('Cancellation Timestamp')
  row[cancellationTimestampIdx] = new Date
  return row;

  function getHeaderObjBySheet(sheet) {
    var values = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()
    var headerObj = values[0].reduce(function (acc, next, idx) {
      if (next === "") { return acc; }
      if (acc.hasOwnProperty(next)) { throw ("Error Duplicate Header Name found: " + next) }
      acc[next] = idx;
      return acc;
    }, {});
    return function getIdxByHeader(header) {
      return headerObj.hasOwnProperty(header) ? headerObj[header] : null;
    }
  }
}

function getHeaderObjBySheet(sheet) {
  var values = sheet.getRange(1, 1, 1, sheet.getLastColumn()).getValues()
  var headerObj = values[0].reduce(function (acc, next, idx) {
    if (next === "") { return acc; }
    if (acc.hasOwnProperty(next)) { throw ("Error Duplicate Header Name found: " + next) }
    acc[next] = idx;
    return acc;
  }, {});
  return function getIdxByHeader(header) {
    return headerObj.hasOwnProperty(header) ? headerObj[header] : null;
  }
}



/* OWNER: douglascox, uxi-automation
   * SOURCES: 
   *
   * DESCRIPTION: Upon submission of go/studyrequestform, this script determines 
   * whether the requester is supported by Research Ops, determines which pod
   * should handle this request, and emails the appropriate pod
   *[OPTIONAL] @param {(sheet|object)} - reference sheet for data fetch
   *[OPTIONAL] @param {(number)} - row containing header 0-based index
   *[OPTIONAL] @param {(string|number)} - row where data start 0-based index
   *[OPTIONAL] @return {(object)} - properites of sheet for filter/update/etc
   */

/* returns properties of sheet
 * @param Object({Sheet})
 * @return { dataRange, body, headerObj, header, getValueByHeader }
 */
function getDataBySheet(sheet, headerIdx = 0, bodyRowStart = 1) {
  var dataRange = sheet.getDataRange();
  var values = dataRange.getValues()
  var headers = values.slice(headerIdx, headerIdx + 1)[0];
  var body = values.slice(bodyRowStart)
  var headerObj = headers.reduce(makeHeaderObj, {})

  return {
    dataRange,
    body: body.map(getValueByHeader),
    headerObj,
    headers,
    getValueByHeader,
  }
  function makeHeaderObj(acc, next, idx) {
    if (next === "" || next === "-") {
      return acc;
    }
    if (acc.hasOwnProperty(next)) {
      throw (`Duplicate headers found ${next}`)
    }
    acc[next] = idx
    return acc;
  }
  // transform Array(row) => Object()
  function getValueByHeader(row, rowIdx) {
    var rowAsObject = Object.create(headerObj)
    for (var header in headerObj) {
      var idx = headerObj[header]
      rowAsObject[header] = row[idx]
    }
    Object.defineProperties(rowAsObject, {
      getRangeByColHeader: {
        value: ((header) => headers.includes(header) ? sheet.getRange(rowIdx + 1 + bodyRowStart, headerObj[header] + 1) : (() => { throw (`header "${header}" not found `) })()), // return range of column 4
        writable: false,
        enumerable: false,
      },
    })

    return rowAsObject
  }
}

// populates orange columns in case costCentersonsubmit fails 
function tryCatchOrangePopulate(e) {
  let range = SpreadsheetApp.getActiveRange()
  e = e || { range }
  try {
    orangeColumns(e)
  } catch (e) {
    Logger.log(`Error @ tryCatchOrangePopulate ${e}`)
  }

  function orangeColumns({ range }) {
    const rowIdx = range.getRow()
    const sheet = range.getSheet();
    const { body } = getDataBySheet(sheet);
    const fetchWhoWeCoverbyLdap = whoWeCoverDataBySheet()
    debugger
    const rowIdxByDataSheet = rowIdx - 2; // minus for headerrow, minus one for zero-base array index 
    const rowAsObject = body[rowIdxByDataSheet]

    const ldapFromEmail = (str) => str.slice(0, str.indexOf('@'))
    const { "Email Address": emailAddress } = rowAsObject
    const ldap = ldapFromEmail(emailAddress)
    const whoWeCoverDataByLdap = fetchWhoWeCoverbyLdap(ldap)
    const getWhoWeCoverRangeByHeader = (str) => whoWeCoverDataByLdap.hasOwnProperty(str) ? whoWeCoverDataByLdap.getRangeByColHeader(str) : null

    const columnHeaders = {
      "Researcher Location": "Location",
      "Cost Center Code (recruiters do not edit this column)": "Cost Center Code",
      "Cost Center Name (recruiters do not edit this column)": "Cost Center Name",
      "Product Area (recruiters do not edit this column)": "Business Unit",
      "Are they on the list of supported cost centers?": "Support level",
      "Pod": "pod",
      "BU Code": "BU Code",
      "Manager List": "Manager List",
      "unified_rollup_level_1": "unified_rollup_level_1"
    }

    populateColumnHeader(rowAsObject)


    function populateColumnHeader(props) {
      const isBlank = (value) => value === "" || value === null
      const getHeaderRange = (headerName) => props.getRangeByColHeader(headerName);

      for (let headerName in columnHeaders) {
        let cellRange = getHeaderRange(headerName);
        let cellValue = cellRange.getValue();
        let whoWeSupportColumnName = columnHeaders[headerName]
        let fetchColumnRange = getWhoWeCoverRangeByHeader(whoWeSupportColumnName)
        let whoWeSupportColumnValue = fetchColumnRange.getValue()
        Logger.log(whoWeSupportColumnValue)
        isBlank(cellValue) && cellRange.setValue([whoWeSupportColumnValue])
      }
    }

    function whoWeCoverDataBySheet() {
      const ss = SpreadsheetApp.getActiveSpreadsheet();
      const sheetName = "Who we cover"
      const sheet = ss.getSheetByName(sheetName);
      const { body } = getDataBySheet(sheet);
      return function (ldap) {
        let temp = body.filter(props => props.ldap.trim() === ldap.trim())
        debugger
        return body.find(props => props.ldap.trim() === ldap.trim())
      }

    }

  }
}
