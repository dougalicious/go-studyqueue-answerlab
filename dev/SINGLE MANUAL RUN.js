function manualCostCentersonSubmit(){

  var e = artificalEvent(42)
  debugger
  //e.values[userNameColumn].substring(0, e.values[userNameColumn].indexOf("@google.com"));
  try{
    costCentersonSubmit(e)
  } catch(e){
    Logger.log(`failed: ${e}`)
  }
  






function artificalEvent(rowIdx){
  const ss = SpreadsheetApp.getActiveSpreadsheet()
  const sheet = ss.getActiveSheet()
  const sheetName = sheet.getName()
  const temprow = SpreadsheetApp.getActiveRange().getRow()
  const { body, headerObj } = getDataBySheet(sheet)
  const activeRowIdx = (!!rowIdx ? rowIdx : SpreadsheetApp.getActiveRange().getRow()) -2
  debugger
  const activeRowAsObj = body[activeRowIdx]
  const headers = Object.keys(activeRowAsObj)
  const values = Object.values(activeRowAsObj)
  for ( let header in activeRowAsObj){
    if(header !== undefined){
      activeRowAsObj[header] = [activeRowAsObj[header]]
    }
  }
  
  const range = {
    getValues: () => Object.keys(activeRowAsObj).map((header) => activeRowAsObj[header][0]),
    getRow: () => activeRowIdx === -1 ? null : activeRowIdx,
    columnStart: 0 ,
    columnEnd: headers.length,
    getSheet: () => sheet,
    
  }
  const namedValues = {namedValues: activeRowAsObj, values}
  const v = range.getValues()
  
  debugger
  return Object.assign(namedValues, {range} );
}



}





function costCentersonSubmit_SINGLE_SUBMISSION_MANUAL_RUN() {
  // Get a public lock on this script, because we're about to modify a shared resource.
  //var lock = LockService.getScriptLock();
  
  // Wait for up to 900 seconds for other processes to finish.
  //lock.waitLock(900000);
  
  //var sheet = e.range.getSheet();//SpreadsheetApp.getActiveSheet();
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName('New unallocated studies');
  var rows = sheet.getDataRange();
  var numRows = rows.getNumRows();
  var values = rows.getValues();
  var lastRow = sheet.getLastRow();
  var lastColumn = sheet.getLastColumn();
  
  
  var activeRow = 42; //******* UPDATE THIS BEFORE RUNNING *********//
  //var activeRow = sheet.getActiveRange().getRow(); //use for onOpen script menu
  
  for (var m = 0; m < lastColumn; m++)
  {
    if(values[0][m] == "Username"){ 
      var userNameColumn = m;  
    }
    else if (values[0][m] == "Email Address"){
      var userNameColumn = m;
    }
    else if(values[0][m].indexOf("Cost Center Code") >=0) { 
      var codeColumn = m;  
    }
    else if(values[0][m].indexOf("Cost Center Name") >=0) { 
      var nameColumn = m;  
    }
    else if (values[0][m].indexOf("supported") >=0) {
      var supportColumn = m;
    }
    else if (values[0][m].indexOf("Product Area") >=0) {
      var PAColumn = m;
    }
    else if (values[0][m].indexOf("Pod") >=0) {
      var PodColumn = m;
    }
    else if (values[0][m].indexOf("BU Code") >=0) {
      var BUColumn = m;
    }
    else if (values[0][m].indexOf("Where in the world would the participants in your study be located?") >=0) {
      var RegionColumn = m;
    }
    else if (values[0][m].indexOf("Paste the link to your Study Spreadsheet here. Ensure that your Screener Form has been linked to it and that the sharing settings for both are: private, with uer-recruiters@google.com added as an editor/collaborator.") >= 0){
      var urlColumn = m;
    }
    else if (values[0][m].indexOf("What's the name of the study?")>=0){
      var studyNameColumn = m;
    }
    else if (values[0][m].indexOf("Researcher Location")>=0){
      var researcherLocationColumn = m;
    }
    else if (values[0][m].indexOf("BU code for Cases") >=0) {
      var BUCasesColumn = m;
    }
     else if(values[0][m].indexOf("Do your participants belong to any of the following categories?") >= 0){
      var categoryColumn = m;
    }
  }
  
  
  
  var researcherID = 0;
  //hardcoded
  //var tempResearcher = 'jesyka'; //.values[userNameColumn].substring(0, e.values[userNameColumn].indexOf("@google.com"));
  //var myUrl = 'https://docs.google.com/spreadsheets/d/1eiKQueP_phdvfdYyRFPv8BqEhHvSnNtWcpGCL6MFU9A/edit#gid=3'; //e.values[urlColumn];
  //var myStudyName = 'YTV Family v1 Diary Study [Dec 2019]'; //e.values[studyNameColumn];
  
  var tempResearcher = sheet.getRange(activeRow, 2, 1, 1).getValue();
    Logger.log(tempResearcher);
  var myUrl = sheet.getRange(activeRow, urlColumn+1, 1, 1).getValue();
  var myStudyName = sheet.getRange(activeRow, studyNameColumn+1, 1, 1).getValue();
  
debugger
  
  try{
    researcherID = getGooglerInfo(tempResearcher);
  }
  catch(e){
    
  } 
  
  
  emailStudyReceipt(sheet, researcherID, myUrl, myStudyName, activeRow);
  
  if(researcherID == 0){
    return;
  }
  
  
  //var costCenter = researcherID.costCenter;
  var ccString = researcherID.costCenter;
  var ccCode = ccString.substring(0, ccString.indexOf(":")); //Person.costCenterNumber;
  var ccName = ccString.substring(ccString.indexOf(" ")+1); //Person.costCenter;
  
  //var ccCode = researcherID.costCenterNumber;
  
  if(ccCode == "71T"){
    var manager = getGooglerInfo(researcherID.manager);
    var name = manager.fullName;
    
    var ccString = manager.costCenter;
    var ccCode = ccString.substring(0, ccString.indexOf(":")); //Person.costCenterNumber;
    var ccName = ccString.substring(ccString.indexOf(" ")+1); //Person.costCenter;
    
    //costCenter = manager.costCenter;
    //ccCode = manager.costCenterCode;
  }
  
  
  //var ccName = costCenter;
  debugger
  var supported = checkSupport(tempResearcher);
  
  
  sheet.getRange(activeRow, codeColumn+1, 1, 1).setValue(ccCode);
  sheet.getRange(activeRow, nameColumn+1, 1, 1).setValue(ccName);
  sheet.getRange(activeRow, supportColumn+1, 1, 1).setValue(supported[0]);
  sheet.getRange(activeRow, PAColumn+1, 1, 1).setValue(supported[1]);
  sheet.getRange(activeRow, researcherLocationColumn+1, 1, 1).setValue(supported[2]);
  sheet.getRange(activeRow, PodColumn+1, 1, 1).setValue(supported[3]);
  sheet.getRange(activeRow, BUColumn+1, 1, 1).setValue(supported[4]);
  // sheet.getRange(activeRow, BUCasesColumn+1, 1, 1).setValue(supported[5]);
  
  if(supported[0]!="Not on list"){
    if(sheet.getRange(activeRow, RegionColumn+1, 1, 1).getValue() == "Europe; Middle East; Africa"){
      sheet.getRange(activeRow, PodColumn+1, 1, 1).setValue("Pod EMEA");
    }
    else if(sheet.getRange(activeRow, RegionColumn+1, 1, 1).getValue() == "Asia (excluding India); Pacific Islands"){
      sheet.getRange(activeRow, PodColumn+1, 1, 1).setValue("Pod APAC");
    }
    else if(sheet.getRange(activeRow, RegionColumn+1, 1, 1).getValue() == "Australia; New Zealand"){
      sheet.getRange(activeRow, PodColumn+1, 1, 1).setValue("Pod APAC");
    }
    else if(sheet.getRange(activeRow, RegionColumn+1, 1, 1).getValue() == "India"){
      sheet.getRange(activeRow, PodColumn+1, 1, 1).setValue("Pod APAC");
    }
   // else if((sheet.getRange(activeRow, categoryColumn+1, 1, 1).getValue()).indexOf("Education")>=0){
   //   sheet.getRange(activeRow, PodColumn+1, 1, 1).setValue("Pod 2");
   // }
  }
  debugger
  var activeValues = sheet.getRange(activeRow, 1, 1, sheet.getLastColumn()).getValues();
  allocation_TEST(values[0], activeValues[0], activeRow);
  // Release the lock so that other processes can continue.
  
  //lock.releaseLock();
}



