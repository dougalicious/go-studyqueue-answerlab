
function recordTriage() { //identify Active Study studies to Triage team to investigate
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getActiveSheet();
  var values = sheet.getDataRange().getValues();
  var lastRow = sheet.getLastRow();
  var lastColumn = sheet.getLastColumn();
  var activeRow = ss.getActiveRange().getRow();
  Logger.log(activeRow);
  var activeColumn = ss.getActiveRange().getColumn();
  var activeCell = ss.getActiveCell();
  var activeValues = sheet.getRange(activeRow, 1, 1, lastColumn).getValues();

  if (sheet.getName() != "Active Studies") {
    return;
  }

  for (var m = 0; m < lastColumn; m++) {
    if (values[0][m] == "What type of support do you need?") {
      var supportColumn = m;
    }
    else if (values[0][m] == "How many participants do you need for this study?") {
      var pptColumn = m;
    }
    else if (values[0][m] == "What is the targeted first date of the study?") {
      var startDateColumn = m;
    }
    else if (values[0][m] == "Timestamp") {
      var requestDateColumn = m;
    }
    else if (values[0][m] == "Which type of study are you planning to do?") {
      var inPersonColumn = m;
    }
    else if (values[0][m] == "Where in the world would the participants in your study be located?") {
      var nonNApptColumn = m;
    }
    else if (values[0][m] == "How difficult is this study to recruit for based on the locations requested?") {
      var locsColumn = m;
    }
    else if (values[0][m] == "How difficult is it to recruit for the requested ppts?") {
      var profilesColumn = m;
    }
    else if (values[0][m] == "Phone screens required?") {
      var phoneScreenColumn = m;
    }
    else if (values[0][m] == "Does your study require you to get approval(s) from Legal/E&C/PWG?") {
      var approvalColumn = m;
    }
    else if (values[0][m] == "In which city/region/country will study sessions take place?") {
      var cityColumn = m;
    }
    else if (values[0][m] == "Complexity Score") {
      var complexityScoreColumn = m;
    }
    else if (values[0][m] == "What's the name of the study?") {
      var titleColumn = m;
    }
    else if (values[0][m].indexOf("Paste the link to your Study Spreadsheet here. Ensure that your Screener Form has been linked to it and that the sharing settings for both are: private, with uer-recruiters@google.com added as an editor/collaborator.") >= 0) {
      var urlColumn = m;
    }
  }

  var colL = values[0].indexOf('Email Address') + 1;
  var emailAddress = sheet.getRange(activeRow, colL, 1, 1).getValue();
  var ldap = emailAddress.substr(0, emailAddress.indexOf("@google.com"));
  var title = activeValues[titleColumn];
  var myUrl = activeValues[urlColumn];
  myUrl = '<a href="' + myUrl + '">study request</a>';
  var researcherID = getGooglerInfo(ldap);

  var claimURL = SpreadsheetApp.getActiveSpreadsheet().getUrl()
  // var claimURL = "https://docs.google.com/spreadsheets/d/1b1gf_VaeSEfxOK6q6GSthICcPglN5A78kVQUFIeSUQg/edit#gid=404773570"
  claimURL = "<a href=\"" + claimURL + "\">moved to the active tab</a>";

  var studydetails = "<ul>";
  for (var m = 0; m < lastColumn; m++) {
    if (values[0][m] == "Researcher Location") {
      studydetails = studydetails + "<li>Researcher location: <b>" + activeValues[0][m] + "</b><br>";
    }
    else {
      studydetails = studydetails + "<li>" + values[0][m] + ': <b>' + activeValues[0][m] + "</b><br>";
    }
  }
  studydetails = studydetails + "</ul>"


  //email that study info uxi-triage@.
  var subject = 'Recruitment Ops Outsourced Study Request Review';
  var message = //"Note: This email was sent from Janice’s account but is triggered automatically by a script. No need to reply to Janice. <br><br>" +
    "Hello,<br><br>" +
    "A new " + myUrl + " has come in from " + ldap + ".<br>" +
    "This request has been escalated to the Triage team. " +
    //"You and your manager will both be emailed again if the study is not " + claimURL + " within 2 business days.<br>" +
    "Here are the full request details:<br><br>" + studydetails +
    "<br><br>" +
    "Thank you!<br>";
  //var recipient = 'bonaro@google.com';
  var recipient = 'uxi-triage@google.com';
  var ccAddress = 'bonaro@google.com';

  //emailStudyReceipt(sheet, researcherID, myUrl, title, activeRow);
  //emailToRecipient(subject, message, recipient, ccAddress)

  //append a new note to the "any changes made during recruiting?" field to append a timestamp, ldap, and the string, "Sent to triage for review"
  var colJ = values[0].indexOf('any changes made during recruiting?') + 1;
  sheet.getRange(activeRow, colJ, 1, 1).setValue(ldap + ' ' + new Date() + ' Sent to triage for review');

  var colAJ = values[0].indexOf('# of RCs working on study') + 1;
  var formResponse = sheet.getRange(activeRow, 1, 1, colAJ).getValues();
  Logger.log(Session.getActiveUser().getEmail());
  /** multivitiam env.dev comment out bottom 3 rows */
  // var triageSs = SpreadsheetApp.openByUrl('https://docs.google.com/spreadsheets/d/13bwntMgiEWM0tJvo1GOQ-nXi6OXLyR2mnPa9dCM467M/edit#gid=1263635405');
  // var triageSheet = triageSs.getSheetByName('Recruiting Ops');
  // triageSheet.getRange(triageSheet.getLastRow() + 1, triageSheet.getDataRange().getValues()[0].indexOf('Study ID') + 1, 1, colAJ).setValues(formResponse);
}



function triageStudyToFastResearch() { //Sends study to FR
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getActiveSheet();
  var isValidSheet = ["New unallocated studies", "Active Studies"].includes(sheet.getName())

  if (!isValidSheet) {
    Logger.log(sheet.getName())
    return;
  }
  var values = sheet.getDataRange().getValues();
  var lastRow = sheet.getLastRow();
  var lastCol = sheet.getLastColumn();
  var activeRow = ss.getActiveRange().getRow();
  // var activeRow = ss.getActiveRange().hasOwnProperty('getRow') ? ss.getActiveRange().getRow(); : 67;

  // 10.29 below to implement validation prompt on study to be sent to go/frtrix 
  const getCellValueByHeader = (header) => values[0].indexOf(header) !== -1 ? values[activeRow - 1][values[0].indexOf(header)] : "N/A"
  const validationCheck = { studyId: getCellValueByHeader("Study ID"), studyName: getCellValueByHeader("What's the name of the study?"), requesterEmail: getCellValueByHeader("Email Address") }
  if (promptConfirmation(validationCheck)) { return }


  //script executer info
  var activeEmail = Session.getActiveUser().getEmail();
  var userInfo = AdminDirectory.Users.get(activeEmail, { viewType: 'domain_public', projection: 'basic' });
  var firstName = userInfo.name.givenName; //submitter first name

  //date formatting
  var startDateIndex = values[activeRow - 1][values[0].indexOf("What is the targeted first date of the study?")];
  var timezone = 'CalendarApp.getDefaultCalendar().getTimeZone()';
  var startDate = Utilities.formatDate(new Date(startDateIndex), timezone, "yyyy-MM-dd");
  Logger.log(startDate);
  var today = Utilities.formatDate(new Date(), timezone, "MM/dd/yyyy");

  //create the submission form with pre-populated fields
  var form = triangeLinkGenerator(values[0], values[activeRow - 1])
  /* the below has been replace with the above 
     the above form generates a prefilled link with more values pre-populated
  var form = 'https://docs.google.com/forms/d/e/1FAIpQLSctli2431s3Ir2e-7NDIpk8Vh9AZV2vQ5VOCIOAcH0Oh9te0A/viewform' + //'&entry.639503438=' +link+to+proposal +
    '?entry.601760742=' + values[activeRow-1][values[0].indexOf("What's the name of the study?")] +
    '&entry.2124795296=' + startDate +
    '&entry.486338788=' + values[activeRow-1][values[0].indexOf("Email Address")] +
    '&entry.138690839=' + values[activeRow-1][values[0].indexOf("In which city/region/country will study sessions take place?")] +
    '&entry.1899355706=' + values[activeRow-1][values[0].indexOf("We've captured your username already. Any other researchers working with you on this study?")];
    //'&entry.1566771330=' + values[activeRow-1][values[0].indexOf("Cost Center Name (recruiters do not edit this column)")]
    */
  var formUrl = '<div style="margin:4px 4px 20px 0px; width: 70px;">' +
    '<a style="align-content: center; text-align: center; width:220px; text-decoration: none; vertical-align: center" href="' + form + '" target="_blank">' +
    '<div style="width: 220px; height: 30px; background-color: rgb(66,131,244); color: white; vertical-align: middle; padding-top: 8px; border-radius:5px" >' +
    'Triage Form Link</div></a></div>';
  //https://docs.google.com/forms/d/e/1FAIpQLSctli2431s3Ir2e-7NDIpk8Vh9AZV2vQ5VOCIOAcH0Oh9te0A/viewform?entry.486338788=email+address

  //build the array of values
  var array = [];
  for (var i = 0; i <= values[0].indexOf('BU Code'); i++) {
    if (values[0][i].indexOf('targeted first date') < 0) {
      array.push({ header: values[0][i], value: values[activeRow - 1][i], idx: i });
    }
    else {
      array.push({ header: values[0][i], value: Utilities.formatDate(new Date(startDateIndex), timezone, "MM/dd/yyyy"), idx: i });
    }
  }
  Logger.log(array);
  /*
  var studyLiInfo = array.map( getAsBulletsLi )
  var triageStudyToFastResearchHTMLTemplate = getTriageStudyToFastResearchHTML({studyLiInfo, firstName, url: form
  */

  //build the body of the email with the values as bullets
  var htmlBody = "<ul>";
  for (var j = 0; j < array.length; j++) {
    var htmlBody = htmlBody + getAsBulletsLi(array[j]);
    function getAsBulletsLi({ header, value }) {
      let innerHtml = "<li>" + header + ": " + makeStrong(value) + "</li>";
      return innerHtml;
    }
    function makeStrong(str) {
      return "<strong>" + str + "</strong>"
    }
  }

  htmlBody = htmlBody + '<br>' + formUrl + '<br>'; //attach the form link at the bottom
  htmlBody = "Hi " + firstName + ",<br>You have flagged the following rOps study to be Triaged for FR.  This was flagged on " + today + ".<br><br>" + htmlBody + "</ul>";
  Logger.log('htmlBodyFinal: ' + htmlBody); //final creation of the data being sent in the email

  //email information
  var subject = '[Multivitamin Test] R-Ops request transfer (Fast Research request)';

  // added bottom 3 lines to update htmlBody of Email, emphasize action trigger on top
  var studyLiInfo = array.map(getAsBulletsLi).join("")
  var options = { url: form, today, firstName: firstName, studyData: studyLiInfo }
  var htmlBody = triageEmailTemplate(options)

  var message = htmlBody;
  var recipient = activeEmail;
  var ccAddress = 'uxi-request-mgmt@google.com';
  //var ccAddress = 'uxi-triage@google.com';
  emailToRecipientTriage(subject, message, recipient, ccAddress);


  //remove the line from the queue
  var locationTab = ss.getSheetByName('Studies Allocated to Fast Research');


  var isActiveStudiesSheet = sheet.getName() === "Active Studies";
  var copyRow = isActiveStudiesSheet ? [getValuesFromActiveStudiesAsFRFormat()] : sheet.getRange(activeRow, 1, 1, lastCol).getValues();


  // 20200515 - update to include "Active Studies"
  //  var copyRow = sheet.getRange(activeRow, 1, 1, lastCol).getValues();
  Logger.log('copyRow: ' + copyRow);
  Logger.log('copyRow.length: ' + copyRow.length);
  locationTab.getRange(locationTab.getLastRow() + 1, 1, 1, locationTab.getLastColumn()).setValues(copyRow);

  //move this line to the Triage sheet - Not needed 01.22.2019
  //var triageSs = SpreadsheetApp.openByUrl('https://docs.google.com/spreadsheets/d/13bwntMgiEWM0tJvo1GOQ-nXi6OXLyR2mnPa9dCM467M/edit#gid=1263635405');
  //var triageSheet = triageSs.getSheetByName('Recruiting Ops');

  //remove row from New Unallocated Studies
  sheet.deleteRow(activeRow);


  /* OWNER: douglascox, uxi-automation
 * SOURCES: go/frtrix, go/studyqueue
 *
 * DATE: 20200909
 * ie: triangeLinkGenerator(values[0], values[activeRow - 1])
 * DESCRIPTION: Generates a pre-fill form url link, with the passed in values. combines header ColIdx with activerow RowIdx and populates pre-filled links where applicable
 * @param {(Array| headerRowValues)} - an array of the header row values
 * @param {(Array| activeRowValues)} - an array of the active row values
 * @return {(string)} - return pre-fill Form Url link
 */
  function triangeLinkGenerator(headerRowValues, activeRowValues) {

    const replaceSpaceWithPlus = (str) => str.replace(/\s+/g, '+')
    const generateEntrySegment = (entryNum, value) => `entry.${entryNum}=${value}`
    const rowByHeaderObj = getRowByHeaderObj(headerRowValues, activeRowValues)

    const timezone = 'CalendarApp.getDefaultCalendar().getTimeZone()';
    const convertDateEntryFormate = (date) => Utilities.formatDate(new Date(date), timezone, "yyyy-MM-dd");
    const getHeaderByProps = (props) => (value, defaultResponse) => props.hasOwnProperty(value) ? props[value] : defaultResponse
    const getValueByHeader = getHeaderByProps(rowByHeaderObj);

    const url = getURL(getValueByHeader)
    Logger.log(url)
    return url;
    debugger
    function getURL(getValueByHeader) {
      const addOtherResearchers = getValueByHeader("We've captured your username already. Any other researchers working with you on this study?")
      let baseUrl = "https://docs.google.com/forms/d/e/1FAIpQLSctli2431s3Ir2e-7NDIpk8Vh9AZV2vQ5VOCIOAcH0Oh9te0A/viewform?"
      const entryMap = {
        emailAddress: {  //Q#01
          entryNum: "486338788",
          value: getValueByHeader("Email Address")
        },
        researchProjectName: {  //Q#02
          entryNum: "601760742",
          value: getValueByHeader("What's the name of the study?")
        },
        productArea: {  //Q#03
          entryNum: "791550648=__other_option__&entry.791550648.other_option_response",
          value: getValueByHeader("Product Area (recruiters do not edit this column)")
        },
        withinPA: {  //Q#04
          entryNum: "773095477",
          value: getValueByHeader("Cost Center Name (recruiters do not edit this column)")
        },
        services: {  //Q#05
          entryNum: "1702010196",
          value: replaceSpaceWithPlus("Participant recruitment and incentives")
        },
        linkProposal: {  //Q#06
          entryNum: "639503438",
          value: getValueByHeader(null, "tbd link")
        },
        startDate: { //Q#08
          entryNum: "2124795296",
          value: convertDateEntryFormate(getValueByHeader("What is the targeted first date of the study?"))
        },
        endDate: {  //Q#09 //startDateChange reference mapping sheet with no corresponding mapping
          entryNum: "178233570",
          value: getValueByHeader(null, "")
        },
        startDateChange: {
          entryNum: "1888707070",
          value: "3"
        },
        country: { //Q#11
          entryNum: "269300057",
          value: getValueByHeader("Where in the world would the participants in your study be located?")
        },
        city: { //Q#12
          entryNum: "138690839",
          value: getValueByHeader("In which city/region/country will study sessions take place?")
        },
        costCenterFunding: { //Q#13
          entryNum: "269300057=__other_option__&entry.269300057.other_option_response",
          value: getValueByHeader(null, "")
        },
        costCenterCode: {  //Q#14
          entryNum: "547277552",
          value: getValueByHeader(null, "")
        },
        preferredVendor: {  //Q#14
          entryNum: "763918773",
          value: getValueByHeader(null, "TBC")
        },
        teamMembers: { //Q#15
          entryNum: "1899355706",
          value: addOtherResearchers
        },
      }
      // fields below are from spreadsheet and not from entryMap obj
      const researchQuestion = getValueByHeader("Please include a link to your research plan or any other relevant documentation that would be helpful for this recruitment effort.", "")
      const numberParticipant = getValueByHeader("How many participants do you need for this study?", "")
      const studyType = getValueByHeader("Which type of study are you planning to do?", "")
      let templateLink
      try {
        templateLink = makeTemplateCopy({ emailAddress: entryMap.emailAddress.value, researchProjectName: entryMap.researchProjectName.value, productArea: entryMap.productArea.value, teamMembers: entryMap.teamMembers.value, country: entryMap.country.value, city: entryMap.city.value, startDate: entryMap.startDate.value, researchQuestion, numberParticipant, studyType, addOtherResearchers })
      } catch (e) {
        templateLink = "unable to pre-populate"
      }

      // //  // Logger.log(templateLink)
      entryMap.linkProposal.value = templateLink || "unable to pre-populate"

      const linksAsString = Object.keys(entryMap).reduce((acc, next) => {
        let qType = next;
        let { entryNum, value } = entryMap[qType]
        let isAddToLink = (entryNum === "" || value === "") ? false : true
        return isAddToLink ? [...acc, generateEntrySegment(entryNum, replaceSpaceWithPlus(value))] : acc
      }, []).join("&")
      return baseUrl + linksAsString;
    }


    function getRowByHeaderObj(headerRow, activeRowValues) {
      return headerRow.reduce((acc, next, idx) => {
        let value = activeRowValues[idx]
        let question = next
        let questionObject = new Object()
        questionObject[question] = value;
        return acc.hasOwnProperty(question) ? acc : Object.assign(acc, questionObject)
      }, {})
    }
  }

  /* OWNER: douglascox, uxi-automation
   * SOURCES:  go/studyqueue  formLink: go/frtrix
   *
   * DATE: 2020-09-15
   * DESCRIPTION: makes a copy of template spreadsheet, and takes inputs to populates spreadsheet. then returns spreadsheet URL
   * @param {(Objects| values)} - an array of the header row values
   @return {(string|copySSUrl)} - return spreadsheetUrl
   */
  function makeTemplateCopy({ emailAddress, researchProjectName, productArea, teamMembers, country, city, startDate, numberParticipant, researchQuestion, studyType, addOtherResearchers }) {
    debugger
    const templateId = "1aH054ZOkvZb14EMGibpj1oc4CN5PMV24QLyoenAqNxU"
    // const templateId = "18UBCcZhtboVO1iwq4WCj5iJcenc_k0qHPeGSFKZ_oBo"
    const ss = SpreadsheetApp.openById(templateId)
    const ssName = ss.getName()
    //const driveFile = DriveApp.getFileById(templateId).makeCopy()//`[test]Copy of ${ssName}`)
    const copySS = ss.copy("Copy of " + ss.getName())
    const copySSid = copySS.getId();
    const copySheet = copySS.getSheetByName("Project overview");
    const copySSUrl = copySS.getUrl()
    debugger
    // Overview
    const getRangeByA1Notation = (str) => copySS.getRange(str)
    const rangeStudyName = getRangeByA1Notation("B5")
    const rangeProduct = getRangeByA1Notation("B6")
    const rangeProjectTeam = getRangeByA1Notation("B7");
    // Quant Studies
    const rangeLocations = getRangeByA1Notation("B17")
    // Qual Studies
    const rangeStudyLocation = getRangeByA1Notation("B22")
    // Project Timelines
    const rangeStartDate = getRangeByA1Notation("B31");
    // Service Deliverables
    const rangeParticpantRecruitment = getRangeByA1Notation('B35');
    const rangeStudyType = getRangeByA1Notation("B11")
    const rangeResearchQuestion = getRangeByA1Notation("B10")
    const rangeQuantNumberParticipant = getRangeByA1Notation("B14")
    const rangeQualNumberParticipant = getRangeByA1Notation("B25")

    debugger
    const combineLocations = [country, city].join(', ');
    const projectTeam = [emailAddress, teamMembers].join(', ')
    rangeStudyName.setValue(researchProjectName)
    rangeProduct.setValue(productArea)
    rangeProjectTeam.setValue(projectTeam)
    rangeLocations.setValue(combineLocations)
    rangeStudyLocation.setValue(combineLocations)
    rangeStartDate.setValue(startDate)
    rangeParticpantRecruitment.setValue("Yes")
    rangeStudyType.setValue(studyType)
    rangeResearchQuestion.setValue(researchQuestion)
    rangeQualNumberParticipant.setValue(numberParticipant)
    rangeQuantNumberParticipant.setValue(numberParticipant)

    copySS.addEditor('fast-research-share@google.com')
    try {
      copySS.addEditor(emailAddress)
      copySS.addEditors(getLdapsFromStr(addOtherResearchers))
    } catch (e) {
      Logger.log(`error to add other researchers ${addOtherResearchers}`)
    }
    return copySSUrl
  }

  function promptConfirmation({ studyId, studyName, requesterEmail }) {
    const response = Browser.msgBox(`The following study will be forwarded to Fast Research.\\nPlease check and click 'Yes' to confirm:\\n\\n Study Id: ${studyId},\\n Study Name: "${studyName}"\\n Requester Email: ${requesterEmail}`, Browser.Buttons.YES_NO);
    return (response !== "yes" ? true : false)
  }


}



function emailToRecipientTriage(subject, message, recipient, ccAddress) { //Generic email sending function
  var mailFromIndex = GmailApp.getAliases().indexOf('uxi-alerts@google.com'); //change to triage alias?? if needed
  var mailFrom = GmailApp.getAliases()[mailFromIndex];

  /** productrion */
  // GmailApp.sendEmail(recipient, subject, message, { from: mailFrom, cc: ccAddress, replyTo: recipient, htmlBody: message });
  /** development */
   const sendEmailParams = {
    to: recipient,
    subject,
    options: { from: mailFrom, cc: ccAddress, replyTo: recipient, }
  }
  const prodOutboundParam = JSON.stringify(sendEmailParams)
        body = prodOutboundParam + body
  GmailApp.sendEmail(devEmails, subject, message, { from: mailFrom, replyTo: recipient, htmlBody: message });

  Logger.log(message);
}



function email_test_michael_1234() {
  /*
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName("New unallocated Studies");
  var range = sheet.getRange(7, 1, 1, sheet.getLastColumn());
  var form = 'https://docs.google.com/forms/d/e/1FAIpQLSctli2431s3Ir2e-7NDIpk8Vh9AZV2vQ5VOCIOAcH0Oh9te0A/viewform';
    //'&entry.1566771330=' + values[activeRow-1][values[0].indexOf("Cost Center Name (recruiters do not edit this column)")]
  var formUrl = '<a href="' + form + '">Triage Form Link</a>';
  var htmlBody = '<ul><li><div style="margin:4px 4px 20px 0px; width: 70px;">' + 
    '<a style="align-content: center; text-align: center; width:220px; text-decoration: none; vertical-align: center" href="' + form + '" target="_blank">' + 
    '<div style="width: 220px; height: 30px; background-color: rgb(66,131,244); color: white; vertical-align: middle; padding-top: 8px; border-radius:5px" >' + 
    'Triage Form Link</div></a></div></li></ul>';
    */

  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName("New unallocated studies")
  var values = sheet.getDataRange().getValues();
  var lastRow = sheet.getLastRow();
  var lastCol = sheet.getLastColumn();
  var activeRow = 45 //ss.getActiveRange().getRow();

  //script executer info
  var activeEmail = Session.getActiveUser().getEmail();
  var userInfo = AdminDirectory.Users.get(activeEmail, { viewType: 'domain_public', projection: 'basic' });
  var firstName = userInfo.name.givenName; //submitter first name

  //date formatting
  var startDateIndex = values[activeRow - 1][values[0].indexOf("What is the targeted first date of the study?")];
  var timezone = 'CalendarApp.getDefaultCalendar().getTimeZone()';
  var startDate = Utilities.formatDate(new Date(startDateIndex), timezone, "yyyy-MM-dd");
  Logger.log(startDate);
  var today = Utilities.formatDate(new Date(), timezone, "MM/dd/yyyy");

  //create the submission form with pre-populated fields
  var form = 'https://docs.google.com/forms/d/e/1FAIpQLSctli2431s3Ir2e-7NDIpk8Vh9AZV2vQ5VOCIOAcH0Oh9te0A/viewform' + //'&entry.639503438=' +link+to+proposal +
    '?entry.601760742=' + values[activeRow - 1][values[0].indexOf("What's the name of the study?")] +
    '&entry.2124795296=' + startDate +
    '&entry.486338788=' + values[activeRow - 1][values[0].indexOf("Email Address")] +
    '&entry.138690839=' + values[activeRow - 1][values[0].indexOf("In which city/region/country will study sessions take place?")] +
    '&entry.1899355706=' + values[activeRow - 1][values[0].indexOf("We've captured your username already. Any other researchers working with you on this study?")];
  //'&entry.1566771330=' + values[activeRow-1][values[0].indexOf("Cost Center Name (recruiters do not edit this column)")]
  var formUrl = '<div style="margin:4px 4px 20px 0px; width: 70px;">' +
    '<a style="align-content: center; text-align: center; width:220px; text-decoration: none; vertical-align: center" href="' + form + '" target="_blank">' +
    '<div style="width: 220px; height: 30px; background-color: rgb(66,131,244); color: white; vertical-align: middle; padding-top: 8px; border-radius:5px" >' +
    'Triage Form Link</div></a></div>';
  //https://docs.google.com/forms/d/e/1FAIpQLSctli2431s3Ir2e-7NDIpk8Vh9AZV2vQ5VOCIOAcH0Oh9te0A/viewform?entry.486338788=email+address

  //build the array of values
  var array = [];
  for (var i = 0; i <= values[0].indexOf('BU Code'); i++) {
    if (values[0][i].indexOf('targeted first date') < 0) {
      array.push({ header: values[0][i], value: values[activeRow - 1][i], idx: i });
    }
    else {
      array.push({ header: values[0][i], value: Utilities.formatDate(new Date(startDateIndex), timezone, "MM/dd/yyyy"), idx: i });
    }
  }
  Logger.log(array);

  var studyLiInfo = array.map(getAsBulletsLi).join("")
  var options = { url: form, today, firstName: firstName, studyData: studyLiInfo }
  var htmlBody = triageEmailTemplate(options)
  var subject = 'TESTING EMAIL Triage to Fast Research ';
  var message = htmlBody;
  var recipient = 'douglascox@google.com';
  var ccAddress = '';
  debugger
  emailToRecipientTriage(subject, message, recipient, ccAddress);


  //helper functions
  function getAsBulletsLi({ header, value }) {
    let innerHtml = "<li>" + header + ": " + makeStrong(value) + "</li>";
    return innerHtml;
  }
  function makeStrong(str) {
    return "<strong>" + str + "</strong>"
  }
}

function getValuesFromActiveStudiesAsFRFormat() {
  //debugger
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName("Active Studies")

  var values = sheet.getDataRange().getValues();
  var lastRow = sheet.getLastRow();
  var lastCol = sheet.getLastColumn();
  var activeRow = ss.getActiveRange().getRow();
  var locationTab = ss.getSheetByName('Studies Allocated to Fast Research');
  //debugger
  var getHeaderAsObj = (sheet, row) => {
    return sheet.getRange(1, 1, 1, sheet.getLastColumn())
      .getValues()[0]
      .reduce((acc, next, idx) => {
        if (next === "") { return acc; }
        if (acc.hasOwnProperty(next)) { throw ("Duplicate Header Error, in 'Studies Allocated to Fast Research'") }
        row === undefined ? acc[next] = idx : acc[next] = row[idx];
        return acc
      }, {})
  }
  var headerRowAsObj = getHeaderAsObj(locationTab)
  var isActiveStudiesSheet = sheet.getName() === "Active Studies"
  var activeRow = values[activeRow - 1];
  //    debugger
  var getRowValuesAsObj = getHeaderAsObj(sheet, activeRow);

  //  var v =getRowValuesAsObj()
  debugger
  let valuesFromActiveStudiesAsFRFormat = getAsRowFromHeaderObj(getRowValuesAsObj, headerRowAsObj)
  return valuesFromActiveStudiesAsFRFormat
  // valuesFromActiveStudiesAsFRFormat
  var copyRow = isActiveStudiesSheet ? getValuesFromActiveStudiesAsFRFormat() : sheet.getRange(activeRow, 1, 1, lastCol).getValues();
  debugger

  function getAsRowFromHeaderObj(curr, next) {
    for (let header in next) {
      let colIdx = next[header]
      let currValue = curr.hasOwnProperty(header) ? curr[header] : ""
      //  debugger
      next[header] = currValue
    }
    return Object.values(next)
  }
}

function triageEmailTemplate({ url, studyData, firstName, today }) {

  return `
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=4.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="">
    <title>HTML Email Template</title>
</head>
<style>
  body {
    font-family: Roboto, Arial, Helvetica, sans-serif;
    font-size: 15px;
    color: #000000;
    -webkit-text-size-adjust: none !important;
    -webkit-font-smoothing: antialiased !important;
    -ms-text-size-adjust: none !important;
}

table, tr, td {
    mso-table-lspace: 0pt;
    mso-table-rspace: 0pt;
}

a:link, a:visited, a:hover, a:active {
    color: #4285f4;
    text-decoration: none;
}

.appleLinks a {
    color: #000000 !important;
    text-decoration: none !important;
}

strong {
    font-weight: bold !important;
}

em {
    font-style: italic !important;
}

.yshortcuts a span {
    color: inherit !important;
    border-bottom: none !important;
}

html {
    -webkit-text-size-adjust: none;
    -ms-text-size-adjust: 100%;
}

.ReadMsgbody1 {
    width: 100%;
}

.ExternalClass {
    width: 100%;
}

.ExternalClass * {
    line-height: 100%
}

td {
    -webkit-text-size-adjust: none;
}

a[href^=tel] {
    color: inherit;
    text-decoration: none;
}

.mob-hide {
    display: none !important;
}

div, p, a, li, td {
    -webkit-text-size-adjust: none;
}

td {
    text-decoration: none !important;
}

a[x-apple-data-detectors] {
    color: inherit !important;
    text-decoration: none !important;
    font-size: inherit !important;
    font-family: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
}

@media screen and (max-width:480px) {
    .pd {
        padding-left: 20px !important;
        padding-right: 20px !important;
    }
}

h3 {
    margin-top: 0;
    margin-bottom: 1;
}

#table {
    border-collapse: collapse;
}

#th, #td {
    padding: 10px;
    text-align: left;
    border: 1px solid #efefef;
}

/*                  tr:nth-child(even) {
       background-color: #eee;
     }*/

/*                  tr:nth-child(odd) {
       background-color: #fff;
     }*/
 </style>

<body style="margin:10 !important; padding:0px 0 0 0px !important; background-color:#FFFFFF;">
    <table width="480" style="max-width: 600px; width: 100%; margin: 0 auto; text-align: center;" cellpadding="0"
        cellspacing="0" border="0">
        <tr>
            <td valign="top" align="center" style="padding-top: 10">
                <table border="0" align="center" cellpadding="0" cellspacing="10" width="100%">
                    <tr>
                        <td class="leftColumnContent">
                            <img src="http://services.google.com/fh/files/emails/ux_infra_logo_long.png"
                                class="columnImage" width="230" alt="logoImageUrl"
                                style="-ms-interpolation-mode:bicubic; height:auto; outline:none; text-decoration:none"
                                height="auto"> 
                        </td>
                        <td
                            style="font-family:Google Sans,Arial,sans-serif;font-size:16px;font-weight:700;color:#9aa0a6;letter-spacing:-0.31px;text-align:right">
                            </td>
                    </tr>
                </table>
                <!-- Main card start -->
        <tr>
            <td align="center" valign="top" bgcolor="#ffffff"
                style="-webkit-border-radius:0px; -moz-border-radius:0px; border-radius: 0px; border-left: 1px solid #efefef; border-top: 1px solid #efefef; border-right: 1px solid #efefef; border-bottom: 1px solid #efefef;">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>         
                        <tr>
                            <td valign="top" align="left"
                                style="font-family:Roboto, Helvetica, Arial sans-serif; font-size: 14px; line-height:24px; color: #414347; padding:30px 40px 30px 40px;">

                                <table>
<tr>
                                                                 <td valign="top" align="center" style="font-family:Google Sans, Roboto, Helvetica, Arial sans-serif; font-size: 18px; font-weight: 500; line-height:36px; color: #202124; padding:0px 40px 0px 40px; letter-spacing: -0.31px">TL;DR: Complete this
<a href="${url}"  target="_blank">form</a> to complete your study transfer to Fast Research</td> 
                                                            </tr>

</table><br>
Hi ${firstName},
                                </a>
                                <br>
                                <p>
                                    You have flagged the following rOps study to be transfered to Fast Research.  This was flagged on ${today}.<br>
<table align="center" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                  <tr>
                                                                        <td>
                                                                          <table align="center" border="0" cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                  <td align="center" style="border-radius: 3px;" bgcolor="#4285f4"><a href="${url}" target="_blank" style="font-size: 16px; font-family: Google Sans, Roboto, Helvetica, Arial, sans-serif; font-weight: 500; letter-spacing: -0.31px; color: #ffffff; text-decoration: none; text-decoration: none;border-radius: 3px; padding: 11px 19px 11px 19px; border: 1px solid #4285f4; display: inline-block;border-radius:5px"><!--[if gte mso 15]>&nbsp;&nbsp;&nbsp;<![endif]-->Transfer Form Link<!--[if gte mso 15]>&nbsp;&nbsp;&nbsp;<![endif]--></a></td>
                                                                                </tr>
                                                                          </table>
                                                                        </td>
                                                                  </tr>
                                                                </table>
<br> ${studyData} 
                                </p>
                                <p>
                                    
                                    <!-- Sign off starts -->
                                                                <tr>
                                                                  <td valign="top" align="left" style="font-family:Roboto, Helvetica, Arial sans-serif; font-size: 14px; line-height:24px; color: #414347; padding:0px 20px 40px 40px;">Thank you,<br> 
UX Infrastructure<br>

</td>
                                                            </tr>
                                </p>
 
                        <!-- Sign off ends -->
                    </tbody>
                </table>
            </td>
        </tr>
        <!-- Main card end -->

</body>

</html>
`
}

