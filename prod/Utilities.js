/* OWNER: dougecox, uxi-automation
 * SOURCES: go/studyqueue
 *
 * DESCRIPTION: use for sheet name transition. the script checks left to right searching for a valid sheet name when/if it finds it then returns the sheet
 * @date 07/17/2020 https://b.corp.google.com/issues/158816791 | to capture any edge cases while in transition
 * @param {(Object|Spreadsheet)} -description ie takes a number
 * @param {(Array| Strings})
 * @return {(Object:Sheet)} - description ie Date object 
 */

function getSingleSheetByNames(ss = SpreadsheetApp.getActiveSpreadsheet(), arr = ["Form Responses 2"]) {
  return ss.getSheetByName(arr.find(isValidSheet))

  function isValidSheet(name) {
    return ss.getSheetByName(name) === null ? false : true
  }
}

function promptConfirmationClaimStudy(msg, { ...args}){
  const notes = Object.entries(args).map( (arr) =>  `\\n${arr[0]}: ${arr[1]}`).join("");
  const response = Browser.msgBox(`${msg} ${notes}`, Browser.Buttons.YES_NO);
  return ( response === "yes" ? true : false) 
}

function getLdapsFromStr(str) {
  return str.split(/[\s,]+/g)
    .map((str) => {
      if (typeof str !== 'string') { return (() => { throw ('${next} is not a string') }) }
      if (/@google.com/gi.test(str)) {
        return str
      } else if (["NA", "N/A", ""].some(value => value === str)) {
        return null
      } else if (str.indexOf('@') === 0) {
        return str.slice(1).concat('@google.com')
      } else if (/@/g.test(str)) {
        return str.concat('google.com')
      } else {
        return str.concat('@google.com')
      }
    }, [])
    .filter(str => str !== null || str === "")
}
function artificalEvent(rowIdx) {
  const ss = SpreadsheetApp.getActiveSpreadsheet()
  const sheet = ss.getActiveSheet()
  const sheetName = sheet.getName()
  rowIdx = rowIdx || SpreadsheetApp.getActiveRange().getRow()
  const { body, headerObj } = getDataBySheet(sheet)
  const activeRowIdx = (!!rowIdx ? rowIdx : SpreadsheetApp.getActiveRange().getRow()) - 2
  const activeRowAsObj = body[activeRowIdx]
  const headers = Object.keys(activeRowAsObj)
  const values = Object.values(activeRowAsObj)
  for (let header in activeRowAsObj) {
    if (header !== undefined) {
      activeRowAsObj[header] = [activeRowAsObj[header]]
    }
  }

  const range = {
    getValues: () => Object.keys(activeRowAsObj).map((header) => activeRowAsObj[header][0]),
    getRow: () => activeRowIdx === -1 ? null : activeRowIdx,
    columnStart: 0,
    columnEnd: headers.length,
    getSheet: () => sheet,

  }
  const namedValues = { namedValues: activeRowAsObj, values }
  const v = range.getValues()

  
  return Object.assign(namedValues, { range });

  function getDataBySheet(sheet, headerIdx = 0, bodyRowStart = 1) {
    var dataRange = sheet.getDataRange();
    var values = dataRange.getValues()
    var headers = values.slice(headerIdx, headerIdx + 1)[0];
    var body = values.slice(bodyRowStart)
    var headerObj = headers.reduce(makeHeaderObj, {})
    var a = body.map(getValueByHeader)
    return {
      dataRange,
      body: body.map(getValueByHeader),
      headerObj,
      headers,
      getValueByHeader,
    }
    function makeHeaderObj(acc, next, idx) {
      if (next === "" || next === "-") {
        return acc;
      }
      if (acc.hasOwnProperty(next)) {
        throw ("Duplicate headers found")
      }
      acc[next] = idx
      return acc;
    }
    // transform Array(row) => Object()
    function getValueByHeader(row, rowIdx) {
      var rowAsObject = Object.create(headerObj)
      for (var header in headerObj) {
        var idx = headerObj[header]
        rowAsObject[header] = row[idx]
      }
      Object.defineProperties(rowAsObject, {
        getRangeByColHeader: {
          value: ((header) => sheet.getRange(rowIdx + bodyRowStart + 1, headerObj[header] + 1)), // return range of column 4
          writable: false,
          enumerable: false,
        },
      })

      return rowAsObject
    }
  }
}
