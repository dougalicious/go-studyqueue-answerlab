

function closedStudiesEmail(){
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var closedSheetName = ("Closed Studies "+(String((new Date()).getFullYear())) )
  var sheet = ss.getSheetByName(closedSheetName);
  // 12.21.20 reference sheet year name dynamically
  // var sheet = ss.getSheetByName("Closed Studies 2020")
  var range = sheet.getRange(965, 1, 128, sheet.getLastColumn())
  var values = range.getValues()
  var idxs = values.map((_,idx) => 965+idx).slice(0,-3)
  
  idxs.forEach(MANUALsendFeedbackEmailAndGetStudySheetLinkAndStudyNameIfNull)
  //var sli = MANUALsendFeedbackEmailAndGetStudySheetLinkAndStudyNameIfNull(idxs[0]);
  debugger
}
function MANUALsendFeedbackEmailAndGetStudySheetLinkAndStudyNameIfNull(closedStudy) {
  
  var sheet1 = SpreadsheetApp.getActiveSpreadsheet();
  var closedSheetName = ("Closed Studies "+(String((new Date()).getFullYear())) )
  var closedSheet = sheet1.getSheetByName(closedSheetName);
  // 12.21.20 reference sheet year name dynamically
  // var closedSheet = sheet1.getSheetByName("Closed Studies 2020");
  var rows = closedSheet.getDataRange();
  var numRows = rows.getNumRows();
  var values = rows.getValues();
  var lastRow = closedSheet.getLastRow();
  var lastColumn = closedSheet.getLastColumn();
  var activeRow = closedStudy;//closedSheet.getLastRow();
  //var activeRow = 15;
  
  var ui = SpreadsheetApp.getUi();
  
  var studyNameColumn = values[0].indexOf("What's the name of the study?");
  var studySpreadsheetColumn = values[0].indexOf("Paste the link to your Study Spreadsheet here. Ensure that your Screener Form has been linked to it and that the sharing settings for both are: private, with uer-recruiters@google.com added as an editor/collaborator.");
  var uxrEmailColumn = values[0].indexOf("Email Address");
  var studyIDColumn = values[0].indexOf("Study ID");
  var rcldapsColumn = values[0].indexOf("Recruiter");
  
  var rcldapsArray = values[activeRow-1][rcldapsColumn].split(",");
  var studyName = values[activeRow-1][studyNameColumn];
  var studyID = values[activeRow-1][studyIDColumn];
  var UXRemail = values[activeRow-1][uxrEmailColumn];
  Logger.log("UXRemail ", UXRemail);
  var UXRLdap = UXRemail.substring(0, UXRemail.indexOf("@google.com"));
  
//  if(studyName == ""){
//    var response = ui.prompt('No study name found, please enter one here:' , ui.ButtonSet.OK_CANCEL);
//    if(response.getSelectedButton() == "CANCEL")
//    {
//      return;
//    }
//    else{
//      studyName = response.getResponseText();
//  //    closedSheet.getRange(activeRow, studyNameColumn+1, 1, 1).setValue(studyName);
//    }
//  }
  
  var spreadsheet = values[activeRow-1][studySpreadsheetColumn];
 
  
  var rcldap = rcldapsArray[0];
  var recruiterEmail = rcldap + "@google.com";
  var sessionUserEmail = Session.getActiveUser().getEmail();
  var sessionUserLdap = sessionUserEmail.substring(0, sessionUserEmail.indexOf("@google.com"));
  var closingStudyRecruiterFullName = "";
  try{
    var recruiterInfo = getGooglerInfo(rcldap);
    var recruiterName = recruiterInfo.firstName;
    var recruiterLastName = recruiterInfo.lastName;
    closingStudyRecruiterFullName = recruiterName + " " + recruiterLastName;
  } catch(e){
    // error
    try {
      // this should always work
      Logger.log("sessionUserLdap ", sessionUserLdap);
      var sessionUserInfo = getGooglerInfo(sessionUserLdap);
      recruiterEmail = sessionUserEmail;
      var sessionUserName = sessionUserInfo.firstName;
      var sessionUserLastName = sessionUserInfo.lastName;
      closingStudyRecruiterFullName = sessionUserName + " " + sessionUserLastName;
    } catch(e) {
      // error
    }
  }
  
  Logger.log("recruiterEmail ", recruiterEmail);
  
  try{
    var UXRInfo = getGooglerInfo(UXRLdap);
    var UXRName = UXRInfo.firstName;
  }
  catch(e){
    // should get an email bounce back
    var UXRName = UXRLdap;
  }
  
  var subject = "Tell us about your study experience";
  var email = UXRemail;
  //var email = "brendonc@google.com";
  debugger
  // brendonc - 10 sept 2019
  // new
  var studyName2 = studyName.replace(/\s/g, "%20");
  const surveyURL = "https://google.qualtrics.com/jfe/form/SV_ac229SThBw67xid?RecruiterEmail="+ recruiterEmail + "&StudyID=" + studyID + "&StudyName=" + studyName2;
  //const emailTemplate = DocumentApp.openById('1IHAPQLoBVE_Q3qO84XViO_H98XqtAVbaaRo49S3u7lU');
  // update doc 20200515
  const emailTemplate = DocumentApp.openById("1U8hHaMwY7nzX7wQlgTTH3bu6z4YPnhwD4JYU91awXjE")
  const emailTemplateText = emailTemplate.getText();
  //regex is used here as there's multiple occurences
  var emailBody = emailTemplateText.replace(/{{survey_url}}/g, surveyURL);
  emailBody = emailBody.replace("{{uxr_first_name}}", UXRName);
  emailBody = emailBody.replace("{{study_name}}", studyName);
  emailBody = emailBody.replace("{{rc_first_last_name}}", closingStudyRecruiterFullName);
//  GmailApp.sendEmail("douglascox@google.com", "[TEST]"+email+"||"+subject, emailBody, {htmlBody: emailBody, noReply: true, });
  // original 07042020
    GmailApp.sendEmail(email, subject, emailBody, {htmlBody: emailBody, noReply: true, bcc: "ux-qualityreview@google.com"});
  Logger.log('email sent for')
  Logger.log(closedStudy)
  
  //GmailApp.sendEmail(email, subject, emailBody, {htmlBody: emailBody, from:mailFrom, cc: "nocella@google.com, ux-qualityreview@google.com"});
  //GmailApp.sendEmail(email, subject, emailBody, {htmlBody: emailBody, cc: "nocella@google.com, ux-qualityreview@google.com", noReply: true});
  // bcc: "ux-qualityreview@google.com" - dont cc 
  
  // deprecated - 8 Oct 2019
  //var newFeedbackSheet1 = SpreadsheetApp.openByUrl("https://docs.google.com/spreadsheets/d/1rY7zHUZOb25pygw3I8YRUR5gg0LU9UiMJhNdeJ42zsI/edit#gid=0");
  //var newfeedbackSheet = newFeedbackSheet1.getSheetByName("Feedback Log");
  //newfeedbackSheet.appendRow([Utilities.formatDate(new Date(), "GMT", "MM/dd/yyyy' 'HH:mm"), recruiterEmail, UXRemail, studyName, studyID]);
  
}


function allocationTestEmailGenerator() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName('New unallocated studies');
  var rows = sheet.getDataRange();
  var numRows = rows.getNumRows();
  var values = rows.getValues();
  var lastRow = sheet.getLastRow();
  var lastColumn = sheet.getLastColumn();
   var activeRow = 11;
  var activeValues = sheet.getRange(activeRow, 1, 1, sheet.getLastColumn()).getValues();
  allocation_TESTEMAILTTYPEMAUPDTE(values[0], activeValues[0]);
}

function allocationEmail({supportedByString, ldap, studydetails, claimURL, myURL, iPaTS, notIPaTS,ldapFound}){
  Logger.log(JSON.stringify({supportedByString, ldap, studydetails, claimURL, myURL, iPaTS, notIPaTS,ldapFound}));
  let intro = `Hello ${supportedByString},` 
  let body = iPaTS ? 
            notIPaTS 
            ? `A new ${myURL} has come in from ${ldap}.  This submission may include an iPaTS request as well.<br>
               Please pick up this study or escalate to your manager if needed. " +
               You and your manager will both be emailed again if the study is not ${claimURL} within 2 business days.<br>" +
               Here are the full request details:<br><br> ${studydetails}`
            : `A new ${myURL} for iPaTS access has come in from ${ldap}.<br>
               Please grant iPaTS access or escalate to your manager if needed.
               You and your manager will both be emailed again if the study is not ${claimURL} within 2 business days.<br>
               Here are the full request details:<br><br> ${studydetails}`
                : ldapFound
                ? `A new ${myURL} has come in from ${ldap}.<br>
                  Please pick up this study or escalate to your manager if needed. 
                  You and your manager will both be emailed again if the study is not ${claimURL} within 2 business days.<br>
                  Here are the full request details:<br><br> ${studydetails}`
                : `A new study request has come in from ${ldap}.<br> 
                  This is not a person we support. Please reply with information about our support model.<br>
                  Here are the full request details:<br><br> ${studydetails}`;
   let signature = `Thank you!<br> 
                    Super duper allocator script Ninja <br><br>`
  debugger
  return {intro:intro, body: body, signature:signature};
 } 
  
  function getHtmlTemplate({intro, body, signature}){
  
  return `
  <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=4.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="">
    <title>HTML Email Template</title>
</head>
<style>
  body {
    font-family: Roboto, Arial, Helvetica, sans-serif;
    font-size: 15px;
    color: #000000;
    -webkit-text-size-adjust: none !important;
    -webkit-font-smoothing: antialiased !important;
    -ms-text-size-adjust: none !important;
}

table, tr, td {
    mso-table-lspace: 0pt;
    mso-table-rspace: 0pt;
}

a:link, a:visited, a:hover, a:active {
    color: #4285f4;
    text-decoration: none;
}

.appleLinks a {
    color: #000000 !important;
    text-decoration: none !important;
}

strong {
    font-weight: bold !important;
}

em {
    font-style: italic !important;
}

.yshortcuts a span {
    color: inherit !important;
    border-bottom: none !important;
}

html {
    -webkit-text-size-adjust: none;
    -ms-text-size-adjust: 100%;
}

.ReadMsgbody1 {
    width: 100%;
}

.ExternalClass {
    width: 100%;
}

.ExternalClass * {
    line-height: 100%
}

td {
    -webkit-text-size-adjust: none;
}

a[href^=tel] {
    color: inherit;
    text-decoration: none;
}

.mob-hide {
    display: none !important;
}

div, p, a, li, td {
    -webkit-text-size-adjust: none;
}

td {
    text-decoration: none !important;
}

a[x-apple-data-detectors] {
    color: inherit !important;
    text-decoration: none !important;
    font-size: inherit !important;
    font-family: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
}

@media screen and (max-width:480px) {
    .pd {
        padding-left: 20px !important;
        padding-right: 20px !important;
    }
}

h3 {
    margin-top: 0;
    margin-bottom: 1;
}

#table {
    border-collapse: collapse;
}

#th, #td {
    padding: 10px;
    text-align: left;
    border: 1px solid #efefef;
}

/*                  tr:nth-child(even) {
       background-color: #eee;
     }*/

/*                  tr:nth-child(odd) {
       background-color: #fff;
     }*/
 </style>

<body style="margin:10 !important; padding:0px 0 0 0px !important; background-color:#FFFFFF;">
    <table width="480" style="max-width: 600px; width: 100%; margin: 0 auto; text-align: center;" cellpadding="0"
        cellspacing="0" border="0">
        <tr>
            <td valign="top" align="center" style="padding-top: 10">
                <table border="0" align="center" cellpadding="0" cellspacing="10" width="100%">
                    <tr>
                        <td class="leftColumnContent">
                            <img src="http://services.google.com/fh/files/emails/ux_infra_logo_long.png"
                                class="columnImage" width="230" alt="logoImageUrl"
                                style="-ms-interpolation-mode:bicubic; height:auto; outline:none; text-decoration:none"
                                height="auto"> 
                        </td>
                     <!--  removed  <td
                            style="font-family:Google Sans,Arial,sans-serif;font-size:16px;font-weight:700;color:#9aa0a6;letter-spacing:-0.31px;text-align:right">
                            UXI Automation</td>  -->
                    </tr>
                </table>
                <!-- Main card start -->
        <tr>
            <td align="center" valign="top" bgcolor="#ffffff"
                style="-webkit-border-radius:0px; -moz-border-radius:0px; border-radius: 0px; border-left: 1px solid #efefef; border-top: 1px solid #efefef; border-right: 1px solid #efefef; border-bottom: 1px solid #efefef;">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>         
                        <tr>
                            <td valign="top" align="left"
                                style="font-family:Roboto, Helvetica, Arial sans-serif; font-size: 14px; line-height:24px; color: #414347; padding:30px 40px 30px 40px;">

                                ${intro}
                                </a>
                                <br>
                                <p>${body}</p>
                                <p>${signature}</p>
 
                        <!-- Sign off ends -->
                    </tbody>
                </table>
            </td>
        </tr>
        <!-- Main card end -->

</body>

</html>
  `
}
// utilities
function compose(...fnArgs) {
  const [first, ...funcs] = fnArgs.reverse();
  return function(...args) {
    return funcs.reduce((res, fn) => fn(res), first(...args));
  };
}



function resendSurveyClosedStudies2020(){
  
  var uid = [
    "7b665bd4-52a7-441e-8279-941e69370944",
    "e4e38402-15d7-4124-b363-4a35c5e12cec",
    "8f7d3155-03eb-4e50-93f8-618e9e0ca7d3",
    "cbb83449-c709-4b51-9f7f-41924aa30222"];
  var ss = SpreadsheetApp.getActiveSpreadsheet()
  var closedSheetName = ("Closed Studies "+(String((new Date()).getFullYear())) )
  var sheet = ss.getSheetByName(closedSheetName);
  // 12.21.20 reference sheet year name dynamically
  // var sheet = ss.getSheetByName("CLosed Studies 2020")
  var dataBySheet = getDataBySheet(sheet);
  
  var body = dataBySheet.body.filter((rowAsObj) => uid.includes(rowAsObj["Study ID"]));
  
//  body = body.slice(0,1)
  body.forEach(resendEmailSurvey)
   
   
 function resendEmailSurvey(rowAsObj){
   var subject = "[RESENT with clickable links]Tell us about your study experience";
 // var email = UXRemail;
  //var email = "brendonc@google.com";
  
  // brendonc - 10 sept 2019
  // new
  var studyName = rowAsObj["What's the name of the study?"]
  var studyName2 = studyName.slice().replace(/\s/g, "%20");
  debugger
  var studyID = rowAsObj["Study ID"]
  var UXRemail = rowAsObj["Email Address"]
  var userName = UXRemail.substring(0, UXRemail.indexOf("@google.com"));
  var UXRName = getGooglerInfo(userName).firstName;
  var rc = rowAsObj["Recruiter"]
  var rcFullName = getGooglerInfo(rc).fullName
 
  var closingStudyRecruiterFullName = rcFullName
  
  const surveyURL = "https://google.qualtrics.com/jfe/form/SV_ac229SThBw67xid?RecruiterEmail="+ UXRemail + "&StudyID=" + studyID + "&StudyName=" + studyName2;
  //const emailTemplate = DocumentApp.openById('1IHAPQLoBVE_Q3qO84XViO_H98XqtAVbaaRo49S3u7lU');
  // update doc 20200515
  const emailTemplate = DocumentApp.openById("1U8hHaMwY7nzX7wQlgTTH3bu6z4YPnhwD4JYU91awXjE")
  const emailTemplateText = emailTemplate.getText();
  //regex is used here as there's multiple occurences
  var emailBody = emailTemplateText.replace(/{{survey_url}}/g, surveyURL);
  emailBody = emailBody.replace("{{uxr_first_name}}", UXRName);
  emailBody = emailBody.replace("{{study_name}}", studyName2);
  emailBody = emailBody.replace("{{rc_first_last_name}}", closingStudyRecruiterFullName);
 // GmailApp.sendEmail('douglascox@google.com', subject, emailBody, {htmlBody: emailBody, noReply: true, });
  GmailApp.sendEmail(UXRemail, subject, emailBody, {htmlBody: emailBody, noReply: true, bcc: "ux-qualityreview@google.com"});
 }
  
  
    /* returns properties of sheet
 * @param Object({Sheet})
 * @return { dataRange, body, headerObj, header, getValueByHeader }
 */
function getDataBySheet(sheet, headerIdx=0, bodyRowStart=1){
  var dataRange = sheet.getDataRange();
  var values = dataRange.getValues()
  var headers = values.slice(headerIdx,headerIdx+1)[0];
  var body = values.slice(bodyRowStart)
  var headerObj = headers.reduce(makeHeaderObj,{})
  var a = body.map(getValueByHeader)
  return {
    dataRange,
    body: body.map(getValueByHeader),
    headerObj,
    headers,
    getValueByHeader,
  }
  function makeHeaderObj(acc, next, idx){
    if (next === "" || next === "-"){
      return acc;
    }
    if ( acc.hasOwnProperty(next) ){
      throw ("Duplicate headers found")
    }
    acc[next] = idx
    return acc;
  }
  // transform Array(row) => Object()
  function getValueByHeader(row, rowIdx){
    var rowAsObject =  Object.create(headerObj)
    for (var header in headerObj){
    var idx = headerObj[header]
    rowAsObject[header] = row[idx]
  }   
  Object.defineProperties(rowAsObject, {
      getRangeByColHeader: {
        value: ((header) => sheet.getRange(rowIdx + bodyRowStart+1, headerObj[header]+1)), // return range of column 4
        writable: false,
        enumerable: false,
      },
      })
      
    return rowAsObject
  }
}
}